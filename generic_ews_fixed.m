function res1 = generic_ews_fixed(data, varargin)
    %generic_ews_fixed get the confidence limits of resilience indicators
    %assuming fixed conditions in a time series.
    %It uses the sieve bootstrap (Bühlmann, 1997). First an AR(n) model is fitted
    %(n=1..10, optimum based on AIC) and the residuals determined. Using this fitted 
    %AR model, we generate nboot new data series using the fitted AR model.
    %The noise term is sampled with replacement from the residuals.
    %
    %We use these bootstrapped time series to fit nboot AR models and 
    %determine the confidence limits of AR(1), std etc. as quantiles.
    %
    %Usage
    %res=generic_ews_fixed(data) the bootstrapped confidence limits are determined
    %    using default alpha(=0.05) and default nboot(=1000)
    %    the function returns a struct with the following fields:
    %              bootstrap - includes all information about the bootstrap, it
    %                contains the AR coefficients (ARcoefs), the number of
    %                bootstraps (nboot), the original data set (data) and the random
    %              seed (seed). This information can be used to recreate the
    %                 bootstrapped data.
    %              bootstrapsets - for each of the indicators it contains the
    %                 bootstrapped values.
    %              alpha - the applied alpha for the confidence limits
    %              CL - table with the estimated two-sided confidence limits of each
    %                 indicator determined as quantiles: [alpha/2 1-alpha/2]
    %
    %
    % res=generic_ews_fixed(data,'variable',setting...) specify other options
    % alpha        - specify the alpha for the confidence limits
    % nboot        - number of bootstrap samples
    % detrending   - the timeseries can be detrended/filtered prior to analysis (can be a cell for each data column).
    %                There are 3 options: 'no'= no detrending, 'gaussian' =
    %                gaussian filtering, 'linear' = linear detrending. Default is 'no' detrending. 
    % detrending   - the timeseries can be detrended/filtered prior to analysis (can be a cell for each data column).
    %                There are six options: 'no'= no detrending, 'gaussian' =
    %                gaussian filtering, 'movmean' = moving average (statistics
    %                toolbox), 'poly' = polynomical (default degree=4, see
    %                polydegree), 'linear' = linear detrending, or 'first-diff' = first-differencing. Default is 'gaussian' detrending.
    % slopekind    - 'linreg','kendall','TS','none','all' kind of slope indicator if 
    %                 groups have been selected (default TS)
    % groups       - optional grouping variable (integer number for
    %                instance). This variable is used for the slopes. 
    % indicators   - cell of strings with the names of the indicators.
    %                <a href="matlab:indicator_fun.supported">full list</a>
    %                Examples:
    %                'AR' - autocorrelation of residuals
    %                'acf' - autocorrelation function of residuals
    %                'std' - standard deviation of residuals
    %                'DFA' - detrended fluctuation analysis
    %                'skewness' - skewness of residuals
    %                'cv' - coefficient of variation of original data
    %                'abscorr' - the average of the absolute correlation with all other columns
    %                default: {'AR', 'acf', 'std', 'skewness'}
    %
    %                You can also specify custom functions as a function handle
    %                or a struct with the type of function (see register_EWS_functions in the code). 
    %                The function handle should take 2 arguments: x and trend and return the value.
    %                example: 
    %                >> generic_ews(data, 'indicators',{'AR','std',@(x,trend)autoregression(x-trend,2)}
    % bandwidth	   - is the bandwidth used for the Gaussian kernel when gaussian filtering is applied.
    %                It is expressed as percentage of the timeseries/burst length (must be numeric between 0 and 100)
    %                Alternatively it can be given by the optimal bandwidth suggested by Bowman and Azzalini (1997) Default).
    % time         - time used for automatic grouping: the "bursts" should have the
    %                same resolution, and period between bursts should be > twice the resolution
    %                average time of burst is used for slopes.
    % timeseries   - a numeric vector of the observed univariate timeseries values or a numeric
    %
    % datacolumn   - column number(s) with the data []=all (default is 1). In case of
    %                multivariate indicators you need to adapt this!
    % bandwidth	   - is the bandwidth used for the Gaussian kernel when gaussian filtering is applied.
    %                It is expressed as percentage of the timeseries length (must be numeric between 0 and 100)
    %                Alternatively it can be given by the optimal bandwidth suggested by Bowman and Azzalini (1997) Default).
    %
    %
    % logtransform - if TRUE data are logtransformed prior to analysis as log(X+1). Default is FALSE.
    %
    % interpolate  - If TRUE linear interpolation is applied to produce a timeseries of equal length as the original. Default is FALSE (assumes there are no gaps in the timeseries).
    %
    % polydegree   - degree of polynomial for detrending (default = 4)      
    %
    % corrcolumns  - column number with the variable for cross correlation 
    %                ([] is all other columns than the data column), default = []
    % bandwidth_unit - unit of bandwidth (see units of winsize)
    % nanflag        - how to deal with nan values in the data set 'includenan' or 'omitnan'
    %
    % Specific parameters for different indicators:
    % acf_lag       - The lag for acf indicator. Default = 1
    % ar_lag        - The lag for autoregression. Default = 1
    % compress_nclass - number of classes for digitizing data for compress indicator, default = 20
    % dfa_order     - the order of the detrended fluctuation analysis. Default = 1
    % dfa_win       - the maximum window size for detrended fluctuation analysis. Default = 100
    % kbdm_overlap  - overlap in KBDM indicator
    % klempelziv_nclass - number of classes for digitizing data for KLempelZiv indicator, default = 20
    % mse_dim      - embedding dimension for multi-scale entropy. Default=3
    % mse_mtau     - number of scales (tau) for multi-scale entropy. Default=5
    % mse_r        - The radius for multi-scale sample entropy. Default = 0.1
    % sampen_dim   - embedding dimension for sample entropy. Default=3
    % sampen_r     - The radius for sample entropy. Default = 0.1
    %   
    %
    %
    %To recreate the bootstrapped data sets use 
    %>> sets=sievebootstrap(res)
    %
    %reference: Bühlmann, P. (1997). Sieve bootstrap for time series. Bernoulli, 123-148.
    ewsdata = ewsdata_fixed(data, varargin{:});
    if ~isempty(ewsdata.groups)
        res1 = ewsdata.run_bursts;
    else
        res1 = ewsdata.run_fixed;
    end
end

