%TAGS:generic_ews
%"winsize", "detrending", "bandwidth", "datacolumn", "corrcolumns", "silent", "logtransform", "arlag",
%"indicators", "figures", "cv", "interpolate", "AR_n", "powerspectrum", "ebisuzaki", "title",
%get all indicators:


try
    ar = indicator_fun('ar', 'nanflag', 'omitnan');
    data = rand(1000, 1);
    data(rand(size(data)) < 0.1) = NaN;
    x = data(1:end - 1) - mean(data(~isnan(data)));
    y = data(2:end) - mean(data(~isnan(data)));
    ndx = ~(isnan(x) | isnan(y));
    slope = x(ndx) \ y(ndx);
    assert(abs(ar.calc(data, data) - slope) < 1E-4, 'nanflag of ar does not remove pairs');
catch err
    if ~strcmp(err.identifier, 'ews:nanflag')
        rethrow(err);
    end
end
%Test whether all supported indicator functions are vectorised and the nanflags

indicator_fun.supported;
tab = indicator_fun.allproperties;
ind = indicator_fun(@(x)autoregression(x,2));
tab1 = [tab; ind.props];
ind = indicator_fun(struct('name', 'AR2', 'DIOR', false, 'function', @(x)autoregression(x,2)));
ind = ind.set('vectorized', true);
assert(ind.props.DIOR == false && ind.props.vectorized, 'properties user function not set')
tab1 = [tab1; ind.props];
x = rand(100, 10);
x(rand(size(x)) < 0.01) = NaN;
for i = 1:size(tab, 1)
   % if ~any(strcmp(tab.Name{i}, {'accel',  'd2xdt2', 'slope'}))
        try
            ind = indicator_fun(tab.Name{i}, 'nanflag', 'omitnan');
            res = ind.funct(x);
            if ~isempty(ind.outfunct)
                res = ind.outfunct(res);
            end
            assert(~any(any(isnan(res))), 'omitnan does not work for "%s"', tab.Name{i});
            ind = indicator_fun(tab.Name{i}, 'nanflag', 'includenan');
            res = ind.funct(x);
            if ~isempty(ind.outfunct)
                res = ind.outfunct(res);
            end
            assert(any(any(isnan(res))), 'includenan does not work for "%s"', tab.Name{i});
        catch err
            if ~strcmp(err.identifier, 'ews:nanflag')
                rethrow(err);
            end
        end
%    end
end



%*****Test generic_ews_fixed
%sievebootstrap vectorized?
if verLessThan('matlab', '8.4.0')
    %autocorrelation column 1 = 0.5 and column2 =0.99
    g_T = dataset('xls', 'testdata.xlsx', 'sheet', 4); %#ok<DTSET>
else
    g_T = readtable('testdata.xlsx', 'sheet', 4);
end
generic_ews(g_T{:, 2}, 'indicators', {'compress', 'kbdm',  ...
    indicator_fun('sampen', 'r', 0.2 * std(g_T{:, 2})),  ...
    struct('name', 'mse', 'dim', 4, 'r', 0.3 * std(g_T{:, 2}))})

if exist('sievebootstrap', 'file') %in old versions private functions are not visible
    aa = sievebootstrap(g_T(:, 2:end));
    %are the autocorrelations preserved?
    aut1 = autoregression(aa(:, 1, 1));
    assert(all(size(aa) == [1001 2 1000]) && aut1 < 0.6 && aut1 > 0.4, 'sievebootstrap not working')
    aut1 = autoregression(aa(:, 2, 1));
    assert(aut1 > 0.95, 'sievebootstrap not working')
end


%no grouping
res = generic_ews_fixed(ones(100, 1), 'indicators', {'ar'});
assert(isnan(res.indicators(1, 1)), 'too few data not well handled')
t = (1:1000)';
data = t * 0.01 + 5 + rand(size(t)) * 3;
data(rand(size(data)) < 0.02) = NaN;
t=datetime('now')+t;
try

    tic
    res = generic_ews_fixed(data,'time',t ,'detrending', 'linear', 'nanflag', 'omitnan');
    toc
    assert(~any(isnan(res.indicators)), 'detrending nan values wrong')
    tic
    res = generic_ews_fixed(data, 'time',t ,'detrending', 'gaussian', 'nanflag', 'omitnan');
    toc;
    assert(~any(isnan(res.indicators)), 'detrending nan values wrong')
    res = generic_ews_fixed(data, 'time',t ,'detrending', 'movmean', 'nanflag', 'omitnan');
    assert(~any(isnan(res.indicators)), 'detrending nan values wrong')


    res = generic_ews_fixed(data, 'time',t ,'detrending', 'first-diff', 'nanflag', 'omitnan');
    assert(~any(isnan(res.indicators)), 'detrending nan values wrong')
    res = generic_ews_fixed(data,'time',t , 'detrending', 'polynomial', 'polydegree', 3, 'nanflag', 'omitnan');
    assert(~any(isnan(res.indicators)), 'detrending nan values wrong')

    %multivariate
    if false
        res = generic_ews_fixed(rand(100, 10),'time',t , 'groups', [ones(50, 1); ones(50, 1) + 1], 'indicators', {'AR', 'var', 'max_AR', 'mafeig', 'max_var', 'crosscorr'});
        assert(max(res.groups(1).CL.Estimated{1}) == res.groups(1).CL.Estimated{3}, 'maximum AR does not work in fixed mode')
        if ~verLessThan('matlab', '8.0') %in old versions private functions are not accessible this way
            assert(TS_slope([res.groups.groupvalue]', [res.groups(1).CL.Estimated{1}(1) res.groups(2).CL.Estimated{1}(1)]') == res.CL.tsslope{1, 1}, 'TS slope not calculated correctly')
        end

        res = generic_ews_fixed(rand(100, 10),'time',t , 'groups', [ones(50, 1); ones(50, 1) + 1], 'indicators', {'min_AR', 'max_var', 'max_AR', 'mafeig'});
        assert(corr([res.groups.groupvalue]', [res.groups(1).CL.Estimated(3) res.groups(2).CL.Estimated(3)]', 'type', 'kendall') == res.CL.kendall{3, 1}, 'Kendall corr not calculated correctly')

    end
    figure(1)
    plot(res.timeseries, 'k')
    hold on
    plot(res.trend, 'r')

    res = generic_ews(data, 'winsize', 5, 'title', 'dataset 1', 'indicators', {'ar', 'std', 'skewness'}, 'nanflag', 'omitnan');
    assert(~any(isnan(res.indicators(end, :))), 'detrending nan values wrong')

    %with grouping
    data = days(t-t(1)) * 0.01 + 5 + rand(size(t)) * 3;
    groups = ones(size(data));
    groups(200:400) = 2;
    groups(401:700) = 3;
    groups(701:1000) = 4;
    res = generic_ews_fixed(data, 'grouping', groups, 'detrending', 'linear', 'nanflag', 'omitnan');
    assert(numel(res.groups) == 4, 'grouping not working');
    if isa(res.CL.tsslope, 'table')
        assert(strncmp(res.CL.tsslope.Properties.RowNames(1), 'slope_', 6), 'slope/grouping not working')
    else
        assert(strncmp(res.CL.tsslope.Properties.ObsNames(1), 'slope_', 6), 'slope/grouping not working')
    end
catch err
    if ~strcmp(err.identifier, 'ews:nanflag')
        rethrow(err);
    end
end

%res = generic_ews_fixed(rand(1000,10), 'grouping', groups,'indicators',{'AR','max_ar'}, 'detrending', 'linear');

%overwring 'arlag'
res = generic_ews(rand(100, 1), 'indicators', {'ar', struct('name', 'ar', 'arlag', 2)}, 'arlag', 1);
assert(~isequal(res.indicator_funs{1}.parameters, res.indicator_funs{2}.parameters) && res.taus(2) ~= res.taus(1), 'cannot set parameters separately')

%test for defaults of datacolumn (multivariate or not)
g_s = generic_ews(rand(100, 10), 'winsize', 5, 'title', 'dataset 1', 'indicators', {'ar', 'std', 'skewness'});
assert(isequal(g_s.EWSopt.datacolumn, {1}), 'default datacolumn not working');
try
    g_s = generic_ews(rand(100, 10), 'winsize', 5, 'title', 'dataset 1', 'indicators', {'ar', 'mafeig', 'skewness'});
catch err
    if ~strcmp(err.identifier, 'indicator_fun:maf:too_few_rows')
        rethrow(err)
    end
end
g_s = generic_ews(rand(100, 10), 'winsize', 11, 'title', 'dataset 1', 'indicators', {'ar', 'mafeig', 'skewness'});
assert(isequal(g_s.EWSopt.datacolumn, {[]}), 'default datacolumn not working');

if verLessThan('matlab', '8.4.0')
    g_T = dataset('xls', 'testdata.xlsx', 'sheet', 2); %#ok<DTSET>
else
    g_T = readtable('testdata.xlsx', 'sheet', 2);
end

%expanding window instead of moving window
g_s = generic_ews(g_T, 'winsize', 5, 'expanding_window', true, 'title', 'dataset 1', 'indicators', {'ar', 'std', 'skewness'}, 'datacolumn', [], 'ebisuzaki', 100);

%multivariate indicators (random data)
g_s = generic_ews(rand(100, 10), 'winsize', 10, 'title', 'dataset 1', 'indicators', {'min_std', 'mean_var', 'max_ar'}, 'datacolumn', [], 'ebisuzaki', 100);
assert(~iscell(g_s.nullmodel.taus) & size(g_s.nullmodel.taus, 2) == 100, 'multivariate ebisuzaki is not working');

g_s = generic_ews(rand(100, 10), 'winsize', 10, 'title', 'dataset 1', 'indicators', {'std', 'mean_var', 'max_ar'}, 'datacolumn', [], 'ebisuzaki', 100);
assert(iscell(g_s.nullmodel.taus) & size(g_s.nullmodel.taus, 2) == 100, 'multivariate ebisuzaki is not working');


g_s = generic_ews(rand(100, 10), 'winsize', 10, 'expanding_window', true, 'title', 'dataset 1', 'indicators', {'std', 'mean_var', 'max_ar'}, 'datacolumn', [], 'ebisuzaki', 0);

g_s1 = generic_ews(rand(100, 10), 'indicators', tab.Name, 'datacolumn', [], 'corrcolumns', 1);
assert(iscell(g_s1.indicators), 'multivariate is not working');

g_s1 = generic_ews(rand(100, 10), 'indicators', {'max_ar', 'degfinger', 'mafac', 'explvar', 'pcavar', 'ar', 'max_abscorr'}, 'datacolumn', []);
assert(iscell(g_s1.indicators) && size(g_s1.indicators{6}, 2) == 10, 'multivariate is not working');


%dataset instead of table and user defined function
g_D = dataset('xls', 'testdata.xlsx', 'sheet', 1); %#ok<DTSET>
if ~verLessThan('matlab', '8.0') %in old versions private functions are not accessible this way
    g_s1 = generic_ews(g_D, 'indicators', {'AR', 'DFA', @(x)autoregression(x,2,'omitnan'), struct('name', 'AR2', 'type', 2, 'function', @(x)autoregression(x,2,'omitnan')), 'mean', 'crosscorr', 'abscorr', 'skewness', 'cv', 'std'}, 'arlag', 2);
    g_ndx = ~isnan(g_s1.indicators(:, 1));
    assert(sum(g_s1.indicators(g_ndx, 1) - g_s1.indicators(g_ndx, 3)) <1E-4 && sum(g_s1.indicators(g_ndx, 1) - g_s1.indicators(g_ndx, 4)) < 1E-4, 'generic_ews:test1', 'the user functions did not work well')
end

%table/matrix with 2 data sets, but not multivariate
if verLessThan('matlab', '8.4.0')
    g_T = dataset('xls', 'testdata.xlsx', 'sheet', 1); %#ok<DTSET>
else
    g_T = readtable('testdata.xlsx', 'sheet', 1);
end
g_T.X2 = rand(size(g_T, 1), 1);
g_T.t = datetime('now')+sort(rand(size(g_T.t)) * 100);
generic_ews(g_T, 'detrending', {'movmean', 'gaussian'}, 'figures', {'original data', 'residuals'; 'original data 1', 'original data 2'})
g_s = generic_ews(g_T, 'indicators', {'AR', 'mean', 'crosscorr', 'abscorr', 'skewness', 'cv', 'std'});
g_s1 = generic_ews(g_s.timeseries, g_s.EWSopt); %should give the same result
assert(sum(abs(g_s1.taus - g_s.taus)) < 0.00001, 'generic_ews:test1', 'the saved data is different')

%interpolation
g_s = generic_ews(g_T, 'interpolate', false, 'figures', {'1', '2', '3', '4', '5'; '5', '6', 'residuals', 'original data', '7'; '8', '9', '10', '11', '12'}, 'logtransform', true, 'indicators', {'AR', 'mean', 'crosscorr', 'abscorr', 'skewness', 'cv', 'std'});
g_s1 = generic_ews(g_T, 'interpolate', true, 'indicators', {'AR', 'mean', 'crosscorr', 'abscorr', 'skewness', 'cv', 'std'});
assert(std(diff(g_s.EWSopt.time)) > 0.0001 && std(diff(g_s1.EWSopt.time)) < 0.0001, 'generic_ews:test1', 'interpolation not correct')
%not ascending time:
g_T.t(end)=g_T.t(1);
errorid='';
try
    generic_ews(g_T, 'indicators', {'AR', 'mean', 'crosscorr', 'abscorr', 'skewness', 'cv', 'std'});
catch err
    errorid=err.identifier;
end
assert(strcmp(errorid,'ewsdata:timemustincrease'),'No error for non-ascending times');

if verLessThan('matlab', '8.4.0')
    g_T = dataset('xls', 'testdata.xlsx', 'sheet', 3); %#ok<DTSET>
else
    g_T = readtable('testdata.xlsx', 'sheet', 3);
end

%other kinds of detrending
g_T.X2 = rand(size(g_T, 1), 1);
g_s1 = generic_ews(g_T, 'detrending', 'linear', 'indicators', {'AR', 'mean', 'crosscorr', 'abscorr', 'skewness', 'cv', 'std'});

g_s1 = generic_ews(g_T, 'detrending', 'no', 'indicators', {'AR', 'mean', 'crosscorr', 'abscorr', 'skewness', 'cv', 'std'});
g_ndx = ~isnan(g_s1.indicators(:, strcmp(g_s1.colnames, 'cv')));
assert(sum(g_s1.indicators(g_ndx, strcmp(g_s1.colnames, 'std')) ./ g_s1.indicators(g_ndx, strcmp(g_s1.colnames, 'mean')) - g_s1.indicators(g_ndx, strcmp(g_s1.colnames, 'cv'))) == 0, 'generic_ews:test1', 'cv not correct')
g_s1 = generic_ews(g_T, 'detrending', 'first-diff', 'indicators', {'AR', 'acf', 'mean', 'crosscorr', 'abscorr', 'skewness', 'cv', 'std'});

%interpolation in random order
g_T.t = sort(rand(size(g_T.t)) * 100);
g_s = generic_ews(g_T, 'interpolate', false, 'indicators', {'AR', 'mean', 'crosscorr', 'abscorr', 'skewness', 'cv', 'std'});
g_s1 = generic_ews(g_T, 'interpolate', true, 'indicators', {'AR', 'mean', 'crosscorr', 'abscorr', 'skewness', 'cv', 'std'});

%datacolumn
unique(g_s.taus)
g_s = generic_ews(rand(100, 10), 'datacolumn', [1 2 3 4 5 6 7], 'indicators', {'AR', 'AR', 'AR', 'AR', 'AR', 'AR', 'AR'});
assert(numel(unique(g_s.taus)) == 7, 'datacolumn not working correctly');
g_s = generic_ews(g_T, 'datacolumn', [1 2 1 1 1 1 1], 'indicators', {'AR', 'AR', 'AR', 'mean', 'abscorr', 'cv', 'std'})
g_ndx = ~isnan(g_s.indicators(:, strcmp(g_s.colnames, 'AR')));
assert(sum(abs(g_s.indicators(g_ndx, strcmp(g_s.colnames, 'AR')) - g_s.indicators(g_ndx, strcmp(g_s.colnames, 'AR_2')))) < 0.0001  ...
    && sum(g_s.indicators(g_ndx, strcmp(g_s.colnames, 'AR')) - g_s.indicators(g_ndx, strcmp(g_s.colnames, 'AR_1'))) > 0.01, 'generic_ews:test1', 'double AR different columns not correct')
if verLessThan('matlab', '8.4.0')
    g_T = dataset('xls', 'testdata.xlsx', 'sheet', 1); %#ok<DTSET>
else
    g_T = readtable('testdata.xlsx', 'sheet', 1);
end
%new bandwidth correction detrending
g_s = generic_ews(g_T.x, 'winsize', 10, 'title', 'dataset 1','nanflag','includenan');

assert(sum(abs(g_s.taus - [-0.0929 -0.0889 -0.1155 -0.0397])) < 0.001, 'generic_ews:test1', 'taus not correct')

%no removal of tails
g_s = generic_ews(g_T.x, 'winsize', 10, 'title', 'dataset 1', 'removetails', false);
assert(sum(abs(g_s.taus - [-0.0792 -0.0756 -0.0695 -0.0761])) < 0.001, 'generic_ews:test1', 'taus not correct')

%old bandwidth correction (for compatibility)
g_s = generic_ews(g_T.x, 'winsize', 10, 'title', 'dataset 1', 'detrending', 'oldgaussian','nanflag','includenan');

assert(sum(isnan(g_s.indicators(:, strcmp(g_s.colnames, 'AR')))) == 100 - 1 + sum(isnan(g_s.trend(1:end, 1))), 'generic_ews:test1', 'window size not correct')
%assert(sum(abs(s.taus - [-0.0892 -0.0846 -0.1093 -0.0444])) < 0.001, 'generic_ews:test1', 'taus not correct')
%is this correct?
assert(sum(abs(g_s.taus - [-0.0931 -0.0891 -0.1155 -0.0405])) < 0.001, 'generic_ews:test1', 'taus not correct')

if verLessThan('matlab', '8.4.0')
    g_T = dataset('xls', 'testdata.xlsx', 'sheet', 1); %#ok<DTSET>
else
    g_T = readtable('testdata.xlsx', 'sheet', 1);
end

%array instead of table
g_s = generic_ews(g_T.x, 'indicators', {'AR', 'mean', 'skewness', 'cv', 'std'});
g_s1 = generic_ews(g_T, 'indicators', {'AR', 'mean', 'skewness', 'cv', 'std'});
assert(sum(abs(g_s1.taus - g_s.taus)) < 0.00001, 'generic_ews:test1', 'taus not correct')
if verLessThan('matlab', '8.4.0')
    g_T = dataset('xls', 'testdata.xlsx', 'sheet', 2); %#ok<DTSET>
else
    g_T = readtable('testdata.xlsx', 'sheet', 2);
end

%add ebisuzaki to existing results
g_s = generic_ews(g_T.x, 'silent', true);
g_s.EWSopt.ebisuzaki = 100;
g_s1 = generic_ews(g_s);


%sensitivity
%g_s1 = generic_ews_sens(g_D, 'indicators', {struct('name', 'AR2', 'type', 2, 'function', @(x)autoregression(x,2)), 'mean', 'crosscorr', 'abscorr', 'skewness', 'cv', 'std'}, 'winsize', (5:10:90), 'bandwidth', (1:4:20));
%generic_ews_sens('-p', g_s1, 'indicators', {struct('name', 'AR2', 'type', 2, 'function', @(x)autoregression(x,2)), 'mean', 'crosscorr'}, 'units', 'yr');
if verLessThan('matlab', '8.4.0')
    %tree
    tree = dataset('xls', 'testdata.xlsx', 'sheet', 5); %#ok<DTSET>
else
    tree = readtable('testdata.xlsx', 'sheet', 5);
end
addpath('C:\d\alg\MAT LAB\stats\generic_ews\data')
g_res = run_regime(tree, 'BM III')
disp('generic_ews is ok')

