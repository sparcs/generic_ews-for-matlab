# generic_ews for MATLAB

generic_ews for MATLAB is a translation of the early warning toolbox from R to MATLAB. It also includes multivariate indicators (Weinans et al. 2021). It includes also functions for sensitivity analysis and pvalues.

The code is used in Scheffer et al. 2021, and the code used in this paper is included.



# Dependency:
The software uses:<ul>
   <li>Statistics and Machine Learning Toolbox - Mathworks
   <li>Parallel Computing Toolbox (optional) - Mathworks
</ul>


# Functions:
<ol><li>generic_ews.m          - the rolling window method for early warning signals (Dakos et al. 2008)
<li>generic_ews_sens.m     - sensitivity analysis of generic_ews
<li>generic_ews_fixed.m     - the bursts method (beta version)
<li>run_regime.m           - (only in master branch) the main figures of Scheffer et al. 2021 are created with this code, usage:
                         run_regime(data,regime), where data should be a MATLAB table with 
                         (at least) two fields:<br>
                             trees<br>
                             tot_hab_cell<br>
                        The regime should be one of these strings:<br>
                            'BM III','P I','P II','Early P III','Late P III','P III'
</ol>

# References:
<ul><li>Ebisuzaki W. (1997) "A method to estimate the statistical significance of a correlation when the data are serially correlated." J Climate, 10, 2147-2153).
<li>Dakos, V., et al. (2008). "Slowing down as an early warning signal for abrupt climate change." Proceedings of the National Academy of Sciences 105(38): 14308-14312
<li>Dakos, V., et al. (2012)."Methods for Detecting Early Warnings of Critical Transitions in Time Series Illustrated Using Simulated Ecological Data." PLoS ONE 7(7): e41010. doi:10.1371/journal.pone.0041010
<li>Scheffer, M., et al. (2021). "Loss of resilience preceded transformations of pre-Hispanic Pueblo societies". Proceedings of the National Academy of Sciences 118:e2024397118.
<LI>Weinans, E., R. Quax, E. H. van Nes, and I. A. v. d. Leemput. 2021. Evaluating the performance of multivariate indicators of resilience loss. Scientific reports 11:9148.

</ul>



# Contact:
Egbert van Nes, Wageningen University, The Netherlands (Egbert.vannes@wur.nl) 
 
