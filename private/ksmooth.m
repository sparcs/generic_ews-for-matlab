
function res = ksmooth(x, y, bandwidth, nanflag, method, removetails)
    if nargin < 3 || isempty(bandwidth)
        bandwidth = [];
    end
    if nargin < 4 || isempty(nanflag)
        if matlabLessThan([8 5])
            nanflag = '';
        else
            nanflag = 'omitnan';
        end
    end
    if nargin < 5 || isempty(method)
        method = 'g';
    end
   
    % oldquantfact = 0.36055512754640; 2.72% lower (use oldg for that)
    %MAD = median absolute deviation = median(abs(X(i)-median(X)))
    %sigma=k*MAD 
    %for normally distributed data:
    %k=1/norminv(3/4)=1.4826
    %a quarter of that is 0.370650
    quantfact = 0.3706506; %=1/norminv(0.75)/4

    if nargin < 6 || isempty(removetails)
        removetails = true;
    end
    if isa(x,'datetime')
        x=days(x-x(1));
    end
    if isa(bandwidth,'duration');
        bandwidth=days(bandwidth);
    end
    if isempty(x)
        res = x;
        return
    end
    n = length(x);
    if isempty(bandwidth)
        % optimal bandwidth suggested by Bowman and Azzalini (1997) p.31
        hx = median(abs(x - median(x))) / 0.6745 * (4 / 3 / n)^0.2;
        hy = median(abs(y - median(y, nanflag)), nanflag) / 0.6745 * (4 / 3 / n)^0.2;
        h = sqrt(hy * hx);
        fprintf('bandwidth smoothing set to %g\n', h);
        if h < sqrt(eps) * n
            error('GRIND:outf:LittleVar', 'There is not enough variation in the data. Regression is meaningless.')
        end
    else
        h = bandwidth;
    end
    omitnan = strcmpi(nanflag, 'omitnan');
    switch lower(method(1))
        case 'g' %"Normal" = Gaussian kernel function
            res = x;
            %/* bandwidth is in units of half inter-quartile range. */
   
            h = quantfact * h;
            cutoff = 4 * h;
            imin = 1;
            %imin=find(x<xp(1)-cutoff,1);
            while(x(imin) < x(1) - cutoff && imin <= size(x, 1))
                imin = imin + 1;
            end
            for j = 1:size(x, 1)
                num = 0.0;
                den = 0.0;
                x0 = x(j);
                for i = imin:size(x, 1)
                    if ~(isnan(y(i)) && omitnan)
                        if (x(i) < x0 - cutoff)
                            imin = i;
                        else
                            if(x(i) > x0 + cutoff)
                                break;
                            end
                            x1 = (abs(x(i) - x0)) / h;
                            w = exp(-0.5 * x1 * x1);
                            %w = dokern(abs(x(i) - x0) / h, kern);

                            num = num + w * y(i);

                            den = den + w;
                        end
                    end
                end
                if(den > 0)
                    res(j) = num / den;
                else
                    res(j) = nan;
                end
              
            end
           
        case 'o' %"Normal" = Gaussian kernel function
            quantfact = 0.36055512754640;
            h = h * quantfact; %variance of 0.13 to get quartiles at + /  -  0.25 * h (comparable with ksmooth (R))
            res = ones(size(y, 1), 1);
            for k = 1:n
                xx = abs(x(k) - x) / h;
                z = exp(-xx(xx < 4).^2 / 2); %x < 4 is more efficient for long time series (negligible effect)
                if isempty(nanflag)
                    res(k) = sum(z .* y(xx < 4), 1) ./ sum(z, 1);
                else
                    res(k) = sum(z .* y(xx < 4), 1, nanflag) ./ sum(z, 1, nanflag);
                end
            end
           
        case 'b' %"box" = moving average
            d = h / 2; % 0.25 quartiles
            res = cell(1, n);
            for k = 1:n
                xx = abs(x(k) - x) / h;
                z = xx < d;
                if isempty(nanflag)
                    res(k) = sum(z .* y, 1) ./ sum(z, 1);
                else
                    res(k) = sum(z .* y, 1, nanflag) ./ sum(z, 1, nanflag);
                end
            end
    end
    if removetails
        %remove tails
        x1 = (x(end) - x) > h / 2 & (x - x(1)) > h / 2;
        res(~x1) = NaN;
    end

end
