function indices = unique_indicat(indicat)
    %unique indicators for groupwise evaluation (so the names, parameters and cols
    %should be unique, not the outfuncs)
    funnames = cell(numel(indicat), 1);
    colss = {indicat{1}.cols};
    parss = {indicat{1}.parameters};
    nodetrendings = indicat{1}.nodetrending;
    colndx = zeros(numel(indicat), 1);
    for i1 = 1:length(indicat)
        funnames{i1} = indicat{i1}.name;
        for j1 = 1:numel(colss)
            if isequal(indicat{i1}.cols, colss{j1}) && isequal(indicat{i1}.parameters, parss{j1}) && (indicat{i1}.nodetrending == nodetrendings(j1))
                colndx(i1) = j1;
                break
            end
        end
        if colndx(i1) == 0
            colss(end + 1) = {indicat{i1}.cols};
            parss(end + 1) = {indicat{i1}.parameters};
            nodetrendings(end + 1) = indicat{1}.nodetrending;
            colndx(i1) = numel(colss);
        end
    end
    [~, ~, indices] = unique(funnames);
    if numel(colss) > 1
        [~, ~, indices] = unique([indices, colndx], 'rows');
    end
end
