%violinplot - create a violinplot
%
%Usage: 
%  violinplot(x,cat) make violinplot of x, where cat are the values of the
%        catagories
%  violinplot(x,cat,'propertery',...)
%       You can use all properties of the function ksdensity (like 'bandwidth') and all
%       properties of patches (like 'facecolor')
%       additionally:
%       'percentiles' list of percentiles to indicate in the violins for
%             instance: [0.05 0.5 0.95]
%       'violinwidth' relative width of the violins relative to the smallest gap
%             in the categories, default 2/3
%       'categorynames' optionnally you can put strings for categories
%

% (c) Egbert van Nes
function violinplot(x, cat, varargin)
    if nargin > 2 && ischar(cat)
        varargin = [{cat} varargin];
        cat = [];
    end
    if iscell(x)
        if isempty(cat)
            cat = 1:numel(x);
        end
        x1 = [];
        cat1 = [];
        if iscell(cat)
            for i = 1:numel(x)
                x1 = [x1; x{i}]; %#ok<AGROW>
                cat1 = [cat1; cat{i}]; %#ok<AGROW>
            end
        else
            for i = 1:numel(x)
                x1 = [x1; x{i}]; %#ok<AGROW>
                cat1 = [cat1; cat(i) + zeros(size(x{i}))]; %#ok<AGROW>
            end
        end
        x = x1;
        cat = cat1;
    end
    ksdensityopts = {'Kernel' , 'Support' , 'Weights', 'Bandwidth', 'Function', 'BoundaryCorrection', 'Censoring' , 'NumPoints'};
    if nargin == 1 || isempty(cat)
        cat = repmat(1:size(x, 2), size(x, 1), 1);
    end
    if size(x, 1) > 1 && numel(cat) == size(x, 2)
        cat = repmat(cat, size(x, 1), 1);
    end
    percentiles = 0.5;
    violinwidth = 2 / 3;
    categorynames = {};
    used = false(size(varargin));
    ksdensityndx = false(size(varargin));
    for i = 2:2:length(varargin)
        if strcmpi(varargin{i - 1}, 'percentiles')
            percentiles = varargin{i};
            used(i - 1:i) = true;
        elseif strcmpi(varargin{i - 1}, 'violinwidth')
            violinwidth = varargin{i};
            used(i - 1:i) = true;
        elseif strcmpi(varargin{i - 1}, 'categorynames')
            categorynames = varargin{i};
            used(i - 1:i) = true;
        elseif any(strcmpi(varargin{i - 1}, ksdensityopts))
            ksdensityndx(i - 1:i) = true;
        end
    end
    ksdensityargs = varargin(ksdensityndx);
    varargin = varargin(~used & ~ksdensityndx);
    x = x(:);
    cat = cat(:);
    ucat = unique(cat);
    diffcat = min(diff(ucat));
    if isempty(diffcat)
        diffcat = 1;
    end
    oldhold = ishold;
    hold on;
    for i = 1:length(ucat)
        x1 = x(cat == ucat(i));
        try
            [dens, xdens] = ksdensity(x1, ksdensityargs{:});
            dens = dens / max(dens) * diffcat * violinwidth / 2;
            xperc = quantile(x1', percentiles);
            yperc = interp1(xdens, dens, xperc);
            fill([ucat(i) - dens fliplr(dens + ucat(i))], [xdens fliplr(xdens)], [0.7, 0.7, 1], 'tag', sprintf('violin(%g)', ucat(i)), varargin{:})
            for j = 1:length(percentiles)
                if percentiles(j) == 0.5
                    linestyle = 'k-';
                else
                    linestyle = 'k:';
                end
                plot([ucat(i) - yperc(j), ucat(i) + yperc(j)], [xperc(j), xperc(j)], linestyle);
            end
        catch err
            if ~strcmp(err.identifier, 'stats:mvksdensity:NanX')
                rethrow(err);
            end
        end
    end
    if ~isempty(categorynames)
        set(gca, 'xtick', 1:length(categorynames), 'xticklabel', categorynames)
    end
    if oldhold
        hold on
    else
        hold off
    end
end
