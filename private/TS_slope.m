function b = TS_slope(x,y)
%TS_slope - Theil–Sen estimator of the slope
%res=TS_slope(x,y)
%Calculates the Theil–Sen estimator is a method for robustly 
%fitting a line to sample points in the plane (simple linear regression) by 
%choosing the median of the slopes of all lines through pairs of points. 
%
%Based on function b = Theil_Sen_Regress(x,y)
%https://nl.mathworks.com/matlabcentral/fileexchange/34308-theil-sen-estimator
if size(x,1)==1
    b=NaN;
elseif size(x,1)==2
   Comb=nchoosek(1:size(x,1),2);
   b=median(diff(y(Comb),1,1)./diff(x(Comb),1,1));
else
   Comb=nchoosek(1:size(x,1),2);
   b=median(diff(y(Comb),1,2)./diff(x(Comb),1,2));
end
