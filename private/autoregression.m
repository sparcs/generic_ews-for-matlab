function rc = autoregression(Ys, alag, nanflag)
    %vectorized alag should be a scalar
    if nargin < 2
        alag = 1;
    end
    if nargin < 3
        nanflag = 'includenan';
    end
    if strcmp(nanflag, 'omitnan')
        if size(Ys, 2) > 1
            rc = zeros(1, size(Ys, 2));
            for i = 1:size(Ys, 2)
                rc(i) = autoregression(Ys(:, i), alag, nanflag);
            end
            return
        else
            Ys = Ys(~isnan(Ys));
        end
    end
    Ys = bsxfun(@minus, Ys, mean(Ys(2:end, :))); %subtracting the mean 
    %                       (important if you do regression without intercept!)
    rc = linregres(Ys(1:end - alag, :), Ys(1 + alag:end, :));

end

function b = linregres(x, y) %without intercept
    %b=(sum(x.*y)-1/length(x)*sum(x)*sum(y))/(sum(x.^2)-1/length(x)*sum(x).^2);
    b = sum(x .* y) ./ (sum(x.^2)); %without intercept
    %intercept
    %a=mean(y)-b*mean(x);
end


