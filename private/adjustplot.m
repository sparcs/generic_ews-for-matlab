function adjustplot(hfig, comm, varargin)
    %handy to adjust subplot positions or default appearance
    %adjustplot(gcf,'-d'): set defaults
    %adjustplot(gcf,'-1',orientation): make axes the same of subplots in a orientation
    %('x','y','b' (both)
    %adjustplot(gcf,'-t','tr') add text right (tr), text left (tl), bottom
    %left (bl), bottom right (bl)
    %adjustplot(gcf,'-r',orientation): remove axes of a subplot in a
    %orientation 't'=remove titles only, 'tx'=remove x titles, 'ty' remove
    %y titles
    %adjustplot(gcf,'-p',spacing): shift subplots closer together (spacing = [spacingx
    %spacingy])
    %adjustplot(gcf,'-b') remove ticks from the axes (does not
    %automatically update with scaling of the figure)
    if nargin < 1
        hfig = gcf;
    elseif ~ishandle(hfig)
        if nargin == 2
            varargin = [{comm} varargin];
            comm = hfig;
            hfig = gcf;
        elseif nargin == 1
            varargin{1} = [];
            comm = hfig;
            hfig = gcf;
        end
    end
    %     if nargin < 3
    %         varargin{1} = [];
    %     end
    %if nargin < 2
    %    comm = '-d';
    %end
    switch comm
        case '-b' %box without ticks, does not work if Inf is in xlim or ylim
            %not updated (could be done with listener)
            %ax=get(hfig,'currentaxes');
            axs = get(hfig, 'children');
            drawnow()
            for i = 1:length(axs)
                if strcmp(get(axs(i), 'Type'), 'axes')
                    yylim = get(axs(i), 'ylim');
                    xxlim = get(axs(i), 'xlim');
 
                    if strcmp(get(axs(i), 'YDir'), 'reverse')
                        iy = [2 1];
                    else
                        iy = [1 2];
                    end
                    if strcmp(get(axs(i), 'XDir'), 'reverse')
                        ix = [2 1];
                    else
                        ix = [1 2];
                    end
                    h = findobj(axs(i), 'tag', 'plot-box');
                    if isempty(h)
                        %first use only!! (else listener causes problem)
                        set(axs(i), 'box', 'off')
                        set(axs(i), 'ylim', yylim);
                        set(axs(i), 'xlim', xxlim);
                        oldhold = ~strcmp(get(axs(i), 'nextplot'), 'add');
                        hold(axs(i), 'on');
                        h = plot(axs(i), [xxlim(ix(1)), xxlim(ix(2)), xxlim(ix(2))], [yylim(iy(2)), yylim(iy(2)), yylim(iy(1))], 'k-', 'tag', 'plot-box', 'LineWidth', get(axs(i), 'LineWidth'));
                        set(get(get(h, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
                        if ~oldhold
                            hold(axs(i), 'off');
                        end
                        addlistener(axs(i), 'YLim', 'PostSet', @(src,evnt)adjustplot(hfig,'-b'));
                        addlistener(axs(i), 'XLim', 'PostSet', @(src,evnt)adjustplot(hfig,'-b'));
                    else
                        set(h, 'xdata', [xxlim(ix(1)), xxlim(ix(2)), xxlim(ix(2))], 'ydata', [yylim(iy(2)), yylim(iy(2)), yylim(iy(1))]);
                    end
                end
            end
            return;
        case '-d'
            if nargin < 3
                apen = struct('pen', '-', 'pen2', '-', 'markersize', 5, 'markerfacecolor', 'none', 'cycle', 0, ...
                    'i', 1, 'color', [0 0 1], 'color2', [0 0 1], 'drawcolor', [0.7500 0 0], 'colormap', [0 1 1; 1 0 1], ... 
                    'linewidth', 1, 'fontsize', 16, 'tickdir', 'out', 'box', 'on');
            else
                apen = varargin{1};
            end

            try
                hax = get(hfig, 'CurrentAxes');
            catch
                hax = findobj(hfig, 'type', 'axes');
            end
            if isempty(hax)
                hax = gca;
            end

            Cbar = findobj(hfig, 'tag', 'Colorbar');
            if ~isempty(Cbar)
                set(Cbar, 'FontSize', apen.fontsize);
            end
            set(hax, 'FontSize', apen.fontsize);
            set(hax, 'LineWidth', apen.linewidth);
            set(hax, 'TickDir', 'out');
            set(hax, 'TickLength', [0.015 0.025])
            if isempty(findobj(hfig, 'tag', 'plot-box'))
                set(hax, 'Box', apen.box);
            else
                adjustplot(hfig, '-b');
            end
            %set(H, 'TickDir', apen.tickdir);
            %            set(hax, 'Box', apen.box);
            t = findobj(hax, 'type', 'text');
            if ~isempty(t)
                if ~isfield(apen, 'fontsize')
                    apen.fontsize = 14;
                end
            
                set(t, 'fontsize', apen.fontsize);
            end

            t1 = [findobj(hfig, 'tag', 'conteq-text'); findobj(hfig, 'tag', 'contbif-text'); findobj(hfig, 'tag', 'label-text')];
            set(t1, 'fontsize', 10);
            if isempty(Cbar) || ~any(hax == Cbar)
                View = get(hax, 'view');
                if (apen.fontsize >= 14) && min(View == [0 90])
                    set(hax, 'units', 'normalized');
                    if ~isempty(Cbar)
                        P = [0.1500 0.1900 0.6455 0.7350];
                    else
                        P = [0.1300 0.1100 0.7750 0.8150];
                    end
                
                    pos1 = get(hax, 'position');
                    if (pos1(3) > 0.6) && (pos1(4) > 0.6)
                        set(hax, 'position', transform(P, apen.fontsize));
                        if ~isempty(Cbar)
                            P = [0.831375 0.13 0.058125 0.795];
                            set(Cbar, 'position', transform(P, apen.fontsize))
                        end
                    
                    end
                
                end
            end
        case '-t'
            if length(varargin) == 2
                atext = varargin{2};
                pos = varargin{1};
            else
                atext = varargin{1};
                pos = 'tl';
            end
            hax = get(hfig, 'CurrentAxes');
            if ischar(pos)
                switch pos
                    case 'tl'
                        pos = [0.05 0.95];
                    case 'tr'
                        pos = [0.95 0.95];
                    case 'bl'
                        pos = [0.05 0.05];
                    case 'br'
                        pos = [0.95 0.05];
                end
            end
            xlims = get(hax, 'xlim');
            ylims = get(hax, 'ylim');
            text(hax, xlims(1) + pos(1) * (xlims(2) - xlims(1)), ylims(1) + pos(2) * (ylims(2) - ylims(1)), atext);
        case '-l'
            hs = getsubplotgrid(hfig);
            if isempty(varargin{1})
                orientation = 'b';
            else
                orientation = varargin{1};
            end
            [rows, cols] = size(hs);
            if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'x', 1)
                for i = 1:rows - 1
                    for j = 1:cols
                        if ishandle(hs(i, j)) && (hs(i, j) ~= 0)
                            adjustticklabels(hs(i, j), 'X')
                        end
                    end
                end
            end
            if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'y', 1)
                for i = 1:rows
                    for j = 1:cols
                        if ishandle(hs(i, j)) && (hs(i, j) ~= 0)
                            adjustticklabels(hs(i, j), 'Y')
                        end
                    end
                end
            end
        case '-r'
            if isempty(varargin{1})
                orientation = 'b';
            else
                orientation = varargin{1};
            end
            hs = getsubplotgrid(hfig);
            if numel(hs) < 2
                disp('Too few axes');
                return;
            end
            removeticklabel = true;
            if strncmpi(orientation, 't', 1)
                removeticklabel = false;
                if contains(orientation, 'x')
                    orientation = 'x';
                elseif contains(orientation, 'y')
                    orientation = 'y';
                else
                    orientation = 'b';
                end
            end
            [rows, cols] = size(hs);
            if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'x', 1)
                for i = 1:rows - 1
                    for j = 1:cols
                        if ishandle(hs(i, j)) && (hs(i, j) ~= 0)
                            set(get(hs(i, j), 'xlabel'), 'string', '')
                            if removeticklabel
                                set(hs(i, j), 'XTickLabel', []);
                            end
                        end
                    end
                end
            end
            if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'y', 1)
                for i = 1:rows
                    for j = 2:cols
                        if ishandle(hs(i, j)) && (hs(i, j) ~= 0)
                            set(get(hs(i, j), 'ylabel'), 'string', '')
                            if removeticklabel
                                set(hs(i, j), 'YTickLabel', []);
                            end
                        end
                    end
                end
            end
            ndx = hs ~= 0;
            set(hs(ndx), 'fontsize', 12)
            aa = get(hs(ndx), 'xlabel');
            if iscell(aa)
                aa = [aa{:}];
            end
            set(aa, 'fontsize', 12);
            aa = get(hs(ndx), 'ylabel');
            if iscell(aa)
                aa = [aa{:}];
            end
            set(aa, 'fontsize', 12);
        case '-p'
            if isempty(varargin{1})
                spacing = 0.02;
            else
                spacing = varargin{1};
                if ischar(spacing)
                    spacing = str2double(spacing);
                end
            end
            set(hfig, 'PaperPositionMode', 'auto')
            set(hfig, 'units', 'normalized');
            figpos = get(hfig, 'position');
            vert = 0.5469; %normal size of figure
            hor = 0.41;
            [hs, colwidths] = getsubplotgrid(hfig);
            if isempty(hs)
                disp('No axes to adjust');
                return;
            elseif length(hs) == 1
                return;
            end

            if length(spacing) == 1
                spacing = spacing + zeros(2, 1);
            end
            [rows, cols] = size(hs);
            if cols <= 4 && rows <= 4
                newfigpos = [figpos(1:2) hor * cols / 2 vert * rows / 2];
            else
                rat = rows / cols;
                if rat < 1
                    newfigpos = [figpos(1:2) hor vert * rat];
                else
                    newfigpos = [figpos(1:2) hor / rat vert];
                end
            end
            if newfigpos(4) > 1
                newfigpos(2) = - 0.2;
            end
            set(hfig, 'position', newfigpos);
            for i = 1:rows
                for j = 1:cols
                    h = findobj(hfig, 'tag', sprintf('subplot(%d,%d)', i, j));
                    if any(ishandle(h)) && any(h ~= 0)
                        set(h, 'position', subplotpos(rows, cols, j, i, spacing, colwidths(i, j)));
                    end
                end
            end
            movegui(gcf, 'center')
        otherwise
            error('adjustsubplots:unknown', 'Unknown command');
    end
end

function [hs, colwidths] = getsubplotgrid(h)
    ch = get(h, 'children');
    hs = [];
    colwidths = [];
    if ~isempty(ch)
        tags = get(ch, 'tag');
        ch = ch(~strcmp(tags, 'legend'));
        types = get(ch, 'type');
        if ~iscell(types)
            types = {types};
        end
        poss = get(ch, 'position');
        if ~iscell(poss)
            poss = {poss};
        end
        ipos = zeros(length(ch), 4);
        for i = length(ch): - 1:1
            if strcmp(types{i}, 'axes')
                ipos(i, :) = poss{i};
            else
                ipos(i, :) = [];
                ch(i) = [];
                types(i) = [];
            end
        end
        %we actually need to account for the spacing between the columns, a
        %bit complex?
        colwidth = floor(ipos(:, 3) ./ min(ipos(:, 3)) + 0.01); %the columns each figure spans
        colpos = sort(unique(sort(ipos(:, 1))), 'ascend');
        rowpos = sort(unique(sort(ipos(:, 2))), 'descend');
        hs = zeros(length(rowpos), length(colpos));
        colwidth(colwidth > length(colpos)) = length(colpos);
        colwidths = zeros(length(rowpos), length(colpos));
        for i = 1:length(ch)
            if strcmp(types{i}, 'axes')
                arow = find(rowpos == ipos(i, 2));
                acol = find(colpos == ipos(i, 1));
                hs(arow, acol) = ch(i);
                colwidths(arow, acol) = colwidth(i);
                set(ch(i), 'tag', sprintf('subplot(%d,%d)', arow, acol));
            end
        end
    end
end

function adjustticklabels(hax, orient)
    axis(hax, 'tight')
    newtick = get(hax, [orient 'tick']);
    tickdiff = (newtick(2) - newtick(1));
    newlim = [newtick(1) - tickdiff newtick(end) + tickdiff];
    axis(hax, 'manual')
    set(hax, [orient 'lim'], newlim);
    set(hax, [orient 'tick'], [newtick(1) - tickdiff newtick]);
end
function P = transform(P, fontsize)
    %P = [left, bottom, width, height]
    %where left and bottom define the distance from the lower-left corner of the screen
    if fontsize <= 18
        P(1) = P(1) + 0.01;
        P(2) = P(2) + 0.03;
        P(3) = P(3) - 0.02;
        P(4) = P(4) - 0.04;
    elseif fontsize <= 22
        P(2) = P(2) + 0.04;
        P(4) = P(4) - 0.04;
    elseif fontsize <= 28
        P(1) = P(1) + 0.02;
        P(3) = P(3) - 0.02;
        P(2) = P(2) + 0.08;
        P(4) = P(4) - 0.08;
    else
        P(1) = P(1) + 0.08;
        P(3) = P(3) - 0.08;
        P(2) = P(2) + 0.16;
        P(4) = P(4) - 0.16;
    end

    return
end
