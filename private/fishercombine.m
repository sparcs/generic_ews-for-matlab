function [res]=fishercombine(ps)
% In statistics, Fisher's method,[1][2] also known as Fisher's combined probability test,
% is a technique for data fusion or "meta-analysis" (analysis of analyses).
% It was developed by and named for Ronald Fisher. In its basic form, it is used
% to combine the results from several independent tests bearing upon the same overall hypothesis (H0).
if nargin==0
    disp('usage: p=fishercombine(ps)   %add vector with ps of the independent tests');
end
chi2=-2*sum(log(ps));
df=2*length(ps);
p=1- chi2cdf(chi2,df);
if nargout==0
    fprintf('Chi2 = %g  df=%d  p = %g\n',chi2,df,p);
else
    res.p=p;
    res.chi=chi2;
    res.df=df;
end
end

