classdef indicator_fun
    % object that implements an indicator function
    % ind = indicator_fun('ar1','ar_lag',2)
    % ind.calc(ewsdata(rand(100,10)))
    % ind = indicator_fun('max_ar1')
    %
    % Supported indicators:
    % 
    % Univariate indicators:
    % 'accel'         acceleration (beta) 
    % 'acf'           autocorrelation function (parameters: acf_lag) (ar-based)
    % 'ar'            autoregression (parameters: ar_lag) (ar-based)
    % 'cv'            coefficient of variation (var-based)
    % 'd2xdt2'        2nd deriviative (beta) 
    % 'dfa'           detrended fluctuation analysis (parameters: dfa_win, dfa_order) (ar-based)
    % 'mean'          average (not a resilience indicator) 
    % 'skewness'      skewness 
    % 'slope'         slope deriviative (beta) 
    % 'std'           standard deviation (var-based)
    % 'var'           variance (var-based)
    % 
    % Cross-correlation (can be between 2 variables):
    % 'abscorr'       absolute value of cross correlation (corr-based)
    % 'cov'           covariance matrix (var-based, corr-based)
    % 'crosscorr'     cross correlation (corr-based)
    % 
    % Complexity measures:
    % 'KBDM'          Kolmogorov complexity using the block decomposition method (parameters: kbdm_overlap) 
    % 'KLempelZiv'    Kolmogorov complexity using the Lempel Ziv method (parameters: klempelziv_nclass) 
    % 'MSE'           Multi-scale sample entropy (parameters: mse_dim, mse_r, mse_mtau) 
    % 'SampEn'        Sample entropy (parameters: sampen_dim, sampen_r) 
    % 'compress'      zip compression ratio (parameters: compress_nclass) 
    % 'dfa'           detrended fluctuation analysis (parameters: dfa_win, dfa_order) (ar-based)
    % 
    % Multivariate indicators (dimension reduction):
    % 'degfinger'     degenerate fingerprinting (ar-based)
    % 'explvar'       explained variance (var-based)
    % 'mafac'         maximum autocorrelation MAF axis (ar-based)
    % 'mafeig'        eigenvalue of the first MAF axis (ar-based)
    % 'mafvar'        variance of the first MAF axis (var-based)
    % 'max_abscorr'   mean of absolute correlations (corr-based)
    % 'max_ar'        maximum autoregression  (parameters: ar_lag) (ar-based)
    % 'max_cov'       max covariance (corr-based)
    % 'max_crosscorr' max of cross correlations (corr-based)
    % 'max_std'       max standard deviation (var-based)
    % 'max_var'       max variance (var-based)
    % 'mean_ar'       average autoregression (parameters: ar_lag) (ar-based)
    % 'mean_std'      average standard deviation (var-based)
    % 'mean_var'      average variance (var-based)
    % 'min_crosscorr' min of cross correlations (corr-based)
    % 'pcavar'        variance of the first PCA axis (var-based)
    % 'sum_abscorr'   sum of absolute correlations (corr-based)
    % 
    % 
    % Note: you can turn univariate indicators to multivariate ones by adding:
    % 'max_', 'min_' 'mean_', 'sum_' before their name to get the max., min. etc of
    % univariate indicators. For instance 'max_AR' gives the maximum AR of all variables
    %
    % The functions support the nanflags 'omitnan' or 'includenan'
   
    properties % (Access = private)
        parameters = {} % you should adapt parameters with the set function (not case sensitive)
    end
    properties
        dispname %displayed name, may include mean_ max_ av_ etc.
        cols = {{}}; %the column(s) to be used in the data set. This can be 
        name %not case sensitive name of the basic indicator
        parameter_names = {} %additional parameters, for instance AR needs the time lag
        funct % the basic function used to calculate the indicator, should return the indicator(s) @(data)fun(data)
        outfunct = []; %for multivariate indicators, you can simply define outfunction min_ max_ av_ (col1,col2)
        nodetrending = false; %set to "true" if the indicator should never use detrending (even if it is available) 
    end
    methods (Static)

        function res = calcgroup(inds, indices, varargin)
            %inds objects for indicators, indices of indicators with the same Name
            %and different outfunct, 
            %the rest is the same as in calc  
            res = cell(size(inds));
            maxindex = max(indices);
            if maxindex == numel(inds)
                for i = 1:length(inds)
                    res{i} = inds{i}.calc(varargin{:});
                end
            else
                for i = 1:maxindex
                    res1 = [];
                    for j = 1:length(inds)
                        if indices(j) == i
                            if isempty(res1)
                                res1 = inds{j}.calcfunct(varargin{:});
                            end
                            if ~isempty(inds{j}.outfunct)
                                res{j} = inds{j}.outfunct(res1);
                            else
                                res{j} = res1;
                            end
                        end
                    end
                end
            end
        end
        function [res, pars] = allproperties(defaults)
            if nargin == 0
                defaults = false;
            end

            varnames = {'Name', 'DIOR', 'nodetrending', 'vectorized', 'AR_based', 'var_based', 'corr_based', 'complexity', 'pars', 'default_pars'};

            %new indicators? --> add the name and properties here:
            % (add function to update_funct)
            %  name = do not use underscore in the name of the indicator
            %  DIOR = is the indicator a dynamic indicator of resilience?
            %  vectorized = can you run the null model vectorized?
            %  AR_based = based on autocorrelation
            %  var_based = based on variabce
            %  corr_based = based on cross correlation
            %  complexity = complexity measure
            %  pars = additional parameters
            %  default_pars = default parameters
            % description
            % numcols = 1 if only the data culumn is used, 2 if also the
            % correlation columne is used
            
            Ctable = {  ...
                'abscorr' , true , true, false, false, false, true, false, '', []; 
                'acf' , true , false, true, true, false, false, false, 'acf_lag', 1; 
                'accel' , true , true, true , false, false , false, false, '', []; 
                'slope' , true , true, true , false, false , false, false, '', []; 
                'd2xdt2' , true , true, true , false, false , false, false, '', []; 
                'ar' , true , false, true , true, false , false, false, 'ar_lag', 1; 
                'cov' , true , true, false , false, true, true, false, '', []; 
                'crosscorr' , true , true, false , false, false, true, false, '', []; 
                'compress' , false , true, false , false, false, false, true, 'compress_nclass', 20; 
                'KBDM' , false , true, false , false, false, false, true, 'kbdm_overlap', 0; 
                'SampEn' , false , true, false , false, false, false, true, 'sampen_dim,sampen_r', [3, 0.1]; 
                'MSE' , false , true, false , false, false, false, true, 'mse_dim,mse_r,mse_mtau', [3, 0.1, 5]; 
                'KLempelZiv', false , true, false , false, false, false, true, 'klempelziv_nclass', 20; 
                'cv' , true , true , true, false, true, false, false, '', []; 
                'degfinger' , true , false, false , true, false, false, false, '', []; 
                'dfa' , true , false, true , true, false, false, true, 'dfa_win,dfa_order', [100, 1]; 
                'explvar' , true , false, false , false, true, false, false, '', []; 
                'mafac' , true , false, false , true, false, false, false, '', []; 
                'mafeig' , true , false, false , true, false, false, false, '', []; 
                'mafvar' , true , false, false , false, true, false, false, '', []; 
                'max_ar' , true , false, false , true, false, false, false, 'ar_lag', 1; 
                'max_abscorr' , true , true, false , false, false, true, false, '', []; 
                'max_cov' , true , true, false, false, false , true, false, '', []; 
                'max_crosscorr', true , true, false , false, false, true, false, '', []; 
                'max_std' , true , false, false, false, true , false, false, '', []; 
                'max_var' , true , false, false , false, true, false, false, '', []; 
                'mean' , false , true, true , false, false, false, false, '', []; 
                'mean_ar' , true , false, false , true, false, false, false, 'ar_lag', 1; 
                'mean_std' , true , false, false , false, true, false, false, '', []; 
                'mean_var' , true , false, false , false, true, false, false, '', []; 
                'min_crosscorr', true , true, false, false, false, true, false, '', [] ; 
                'pcavar' , true , false, false, false, true , false, false, '', []; 
                'std' , true , false, true , false, true, false, false, '', []; 
                'skewness' , true , false, true, false, false, false, false, '', [] ; 
                'sum_abscorr' , true , true, false , false, false, true, false, '', []; 
                'var' , true , false, true , false, true, false, false, '', []};
            %varnames2
            varnames2 = {'description', 'numcols'};
            Ctable2 = {  ...%name + descriptions must contain the same names as above
                'ar', 'autoregression', 1;  ...
                'accel', 'acceleration (beta)', 1;  ...
                'slope', 'slope deriviative (beta)', 1;  ...
                'd2xdt2', '2nd deriviative (beta)', 1;  ...
                'acf' , 'autocorrelation function', 1;  ...
                'cv' , 'coefficient of variation', 1;  ...
                'dfa' , 'detrended fluctuation analysis', 1;  ...
                'mean' , 'average (not a resilience indicator)', 1; 
                'skewness' , 'skewness', 1; 
                'std' , 'standard deviation', 1; 
                'var', 'variance', 1; 
                'abscorr' , 'absolute value of cross correlation', 2; 
                'crosscorr' , 'cross correlation', 2; 
                'cov' , 'covariance matrix', 2; 
                'compress', 'zip compression ratio', 1; 
                'KBDM', 'Kolmogorov complexity using the block decomposition method', 1; 
                'KLempelZiv', 'Kolmogorov complexity using the Lempel Ziv method', 1; 
                'SampEn', 'Sample entropy', 1; 
                'MSE', 'Multi-scale sample entropy', 1; 
                'degfinger' , 'degenerate fingerprinting', 1; 
                'explvar' , 'explained variance', 1; 
                'mafac' , 'maximum autocorrelation MAF axis', 1; 
                'mafeig' , 'eigenvalue of the first MAF axis', 1; 
                'mafvar' , 'variance of the first MAF axis', 1; 
                'max_ar' , 'maximum autoregression ', 1; 
                'max_crosscorr' , 'max of cross correlations', 2; 
                'max_cov' , 'max covariance', 2; 
                'max_std' , 'max standard deviation', 1; 
                'max_var' , 'max variance', 1; 
                'mean_ar' , 'average autoregression', 1; 
                'mean_std' , 'average standard deviation', 1; 
                'mean_var' , 'average variance', 1; 
                'max_abscorr' , 'mean of absolute correlations', 2; 
                'min_crosscorr' , 'min of cross correlations', 2; 
                'pcavar' , 'variance of the first PCA axis', 1; 
                'sum_abscorr' , 'sum of absolute correlations', 2};
            Ctable = sortrows(Ctable, 1);
            Ctable2 = sortrows(Ctable2, 1);
            if any(~strcmp(Ctable(:, 1), Ctable2(:, 1)))
                error('lists do not match');
            end
            Ctable = [Ctable Ctable2(:, 2:end)];
            varnames = [varnames varnames2]';
            try
                res = cell2table(Ctable);
                res.Properties.VariableNames = varnames;
            catch err
                if strcmp(err.identifier, 'MATLAB:UndefinedFunction')
                    %old MATLAB version: create dataset instead of table
                    res = dataset; %#ok<DTSET>
                    for i = 1:numel(varnames)
                        if any(cellfun(@(x)numel(x)~=1, Ctable(:, i)))
                            res.(varnames{i}) = vertcat(Ctable(:, i));
                        else
                            res.(varnames{i}) = vertcat(Ctable{:, i});
                        end
                    end
                else
                    rethrow(err);
                end
            end
            for i = 1:size(res, 1)
                if ~isempty(res.pars{i})
                    % pars=res.pars{i};
                    % s = sprintf('%s,', pars{:});
                    res.description{i} = sprintf('%s (parameters: %s)', res.description{i}, regexprep(res.pars{i}, '[ ]*,[ ]*', ', '));
                end
            end
            derived = ~cellfun(@isempty, strfind(res.Name, '_'));
            %dimension reduction
            res.dim_reduct = ~res.vectorized & ~((res.complexity | res.corr_based) & ~derived);
   
            if defaults
                res = res(1, :);
                res.Name = {''};
                res.DIOR = true;
                res.vectorized = false;
                res.AR_based = false;
                res.var_based = false;
                res.corr_based = false;
                res.description = {'user-defined function'};
                res.pars = {''};
                res.numcols = 1;
                res.default_pars = {[]};
            end
            if ~defaults && nargout > 1
                pars = regexp(sprintf('%s,', res.pars{:}), ',*', 'split');
                pars = pars(cellfun(@(x)~isempty(x), pars));
                default_pars = horzcat(indicator_fun.allproperties.default_pars{:});
                [~, ndx] = unique(pars);
                pars = pars(ndx);
                default_pars = default_pars(ndx);
                pars = [pars(:), num2cell(default_pars(:))]';
                pars = pars(:)';
            end
      
        end
 


        function res1 = supported
            %returns a table with supported indices with some properties
            function maketab(tab, title1)
                fprintf('\n%s\n', title1)
                based_on = {'ar-based, ', 'var-based, ', 'corr-based, '};
                for i = 1:size(tab, 1)
                    base = based_on([tab.AR_based(i), tab.var_based(i), tab.corr_based(i)]);
                    if ~isempty(base)
                        s = [base{:}];
                        s = ['(' s(1:end - 2) ')'];
                    else
                        s = [];
                    end
                    fprintf('%-15s %s %s\n', ['''', tab.Name{i}, ''''], tab.description{i}, s);
                end
            end
            res = indicator_fun.allproperties;

            if nargout == 1
                res1 = res;
            else
                disp('Supported indicators:')
                ndx = ~res.corr_based & ~res.dim_reduct & (~(res.complexity & ~res.DIOR));
                maketab(res(ndx, :), 'Univariate indicators:')
                ndx = res.corr_based & ~res.dim_reduct;
                maketab(res(ndx, :), 'Cross-correlation (can be between 2 variables):')
                ndx = res.complexity & ~res.dim_reduct;
                maketab(res(ndx, :), 'Complexity measures:')
                ndx = res.dim_reduct;
                maketab(res(ndx, :), 'Multivariate indicators (dimension reduction):')
                fprintf('\n\nNote: you can turn univariate indicators to multivariate ones by adding:\n''max_'', ''min_'' ''mean_'', ''sum_'' before their name to get the max., min. etc of\n');
                fprintf('univariate indicators. For instance ''max_AR'' gives the maximum AR of all variables\n');
            end
        end
        function res = cell2ind(names, opts)
            %create a cell array with indicator functions
            %opts a struct with options
            if ~iscell(names)
                names = {names};
            end
            if isfield(opts, 'datacolumn')
                datacol = opts.datacolumn;
                if isempty(datacol)
                    datacol = {[]};
                elseif ~iscell(datacol)
                    datacol = num2cell(datacol);
                end
                opts = rmfield(opts, 'datacolumn');
            else
                datacol = {1};
            end
            if isfield(opts, 'corrcolumns')
                corrcol = opts.corrcolumns;
                if isempty(corrcol)
                    corrcol = {[]};
                elseif ~iscell(corrcol)
                    corrcol = num2cell(corrcol);
                end
                opts = rmfield(opts, 'corrcolumns');
            else
                corrcol = {2};
            end
            res = cell(size(names));
            funnames = cell(size(names));
            for i = 1:length(names)
                res{i} = indicator_fun(names{i}, opts);
                funnames{i} = res{i}.name;
                for k = 1:length(res{i}.parameters)
                    %if a parameter is a cell it can give a different value for each
                    %of the indicators
                    if iscell(res{i}.parameters{k})
                        if length(res{i}.parameters{k}) > 1
                            res{i}.parameters{k} = res{i}.parameters{k}{i};
                        else
                            res{i}.parameters{k} = res{i}.parameters{k}{1};
                        end
                    elseif isnumeric(res{i}.parameters{k}) && numel(res{i}.parameters{k}) == length(names)
                        if length(res{i}.parameters{k}) > 1
                            res{i}.parameters{k} = res{i}.parameters{k}(i);
                        else
                            res{i}.parameters{k} = res{i}.parameters{k}(1);
                        end
                    end
                end
                if length(corrcol) == 1
                    res{i} = res{i}.set('corrcolumns', corrcol{1});
                else
                    res{i} = res{i}.set('corrcolumns', corrcol{i});
                end
                if length(datacol) == 1
                    res{i} = res{i}.set('datacolumn', datacol{1});
                else
                    res{i} = res{i}.set('datacolumn', datacol{i});
                end
            end
            [~, ~, indices] = unique(funnames);
        end

        
    end
    methods
        function obj = indicator_fun(aname, varargin)
            %constructor for an indicator function, a name should be one of
            %the supported indicators or an unique name for a user-defined
            %indicator, varargin can be all properties/parameters to set
            if isstruct(aname)
                %user defined indicator function
                s = aname;
                if isfield(s, 'Name')
                    aname = s.Name;
                else
                    aname = s.name;
                end
                allinds = indicator_fun.allproperties;
                if any(strcmpi(allinds.Name, aname))
                    if isfield(s, 'funct')
                        error('indicator_fun:userfun', 'User function "%s" should have another name than the standard functions', s.name);
                    end
                    f = [fieldnames(s) struct2cell(s)];
                    f = f(~strcmpi(f(:, 1), 'name'), :)';
                    obj = indicator_fun(aname, f{:});
                    return;
                else
                    obj.dispname = aname;
                    obj.name = aname;
                    obj.funct = s.function;
                    obj.parameter_names = {'userfunct', 'props'};
                    defaultprops = obj.allproperties(true);
                    defaultprops.Name = {obj.dispname};
                    if isa(defaultprops, 'table')
                        f2 = defaultprops.Properties.VariableNames;
                    else
                        f2 = defaultprops.Properties.VarNames;
                    end
                    f = fieldnames(s);
                    for i = 1:length(f)
                        if any(strcmp(f{i}, f2))
                            defaultprops.(f{i}) = s.(f{i});
                        end
                    end
                    obj.parameters = {obj.funct, defaultprops};
                    %userfunction cannot have free parameters (use anonymous function handle to set fixed parameters)
                    %you can only change the props as table (see obj.props)
                    obj.outfunct = []; %you can also not define outfunct using max_ etc.
                    if nargin > 1
                        obj = obj.set(varargin{:});
                    else
                        obj = update_functs(obj);
                    end
                end
                return
            elseif isa(aname, 'function_handle')
                nam = regexprep(char(aname), '[@]?[(][^)]*[)]', '');
                obj = indicator_fun(struct('name', nam, 'function', aname), varargin{:});
                return;
            elseif isa(aname, 'indicator_fun')
                obj = aname;
                return;
            end
            obj.dispname = aname;
            names = regexp(aname, '_', 'split'); %extract outfunc e.g. min_AR becomes minimum of AR
            %note that if possible (e.g. mean, min and max) the functions are done on all dimensions
            if numel(names) == 2
                aname = names{2};
                if any(strcmp(names{1}, {'mean', 'sum', 'median'}))
                    obj.outfunct = str2func(sprintf('@(x)%s(x(:))', names{1}));
                elseif any(strcmp(names{1}, {'std', 'var'}))
                    obj.outfunct = str2func(sprintf('@(x)%s(x(:),0)', names{1}));
                elseif any(strcmp(names{1}, {'min', 'max'}))
                    if any(strcmpi(aname, {'crosscorr', 'abscorr'}))
                        %for corr we use each value once and neglect the
                        %diagonal
                        if matlabLessThan([8 5])
                            obj.outfunct = str2func(sprintf('@(x)%s(x(tril(true(size(x)),-1)))', names{1}));
                        else
                            obj.outfunct = str2func(sprintf('@(x)%s(x(tril(true(size(x)),-1)),[],''includenan'')', names{1}));
                        end
                        %                        obj.outfunct = str2func(sprintf('@(x)%s(x(tril(true(size(x)),-1)))', names{1}));
                    else
                        if matlabLessThan([8 5])
                            obj.outfunct = str2func(sprintf('@(x)%s(x(:))', names{1}));
                        else
                            obj.outfunct = str2func(sprintf('@(x)%s(x(:),[],''includenan'')', names{1}));
                        end
                        %
                    end
                else
                    obj.outfunct = str2func(names{1});
                end
            end
            obj.nodetrending = false;
            obj.name = aname;
            obj.cols = {{}};
            %set default parameters as indicated in props:
            prop = obj.props;
            obj.nodetrending = prop.nodetrending;
            if ~isempty(prop.default_pars{1})
                pars = regexp(prop.pars, ',', 'split');
                obj.parameter_names = [obj.parameter_names, pars{1}];
                defpar = prop.default_pars{1};
                for i = 1:length(defpar)
                    obj.parameters{end + 1} = defpar(i);
                end
            else
                obj.parameters = {};
                obj.parameter_names = {};
            end
            if prop.numcols == 2
                %switch lower(obj.name)
                %bb    case {'cov', 'crosscorr', 'abscorr'}
                obj.cols = {{}, {}};
            end
            if nargin > 1
                obj = obj.set(varargin{:});
            else
                obj = update_functs(obj);
            end
            if isempty(obj.funct)
                tab = obj.allproperties;
                s = sprintf('%s, ', tab.Name{:});
                error('indicator_fun:unknown', 'Indicator "%s" not supported.\nValid options are: %s\n<a href="matlab:indicator_fun.supported">more help</a>\n', obj.dispname, s);
            end
        end
 
        function res = props(obj, aname)
            %names of supported indices and some properties
            if nargin == 1
                if any(strcmpi(obj.dispname, {'mafeig', 'mafac', 'mafvar'}))
                    aname = obj.dispname;
                else
                    aname = obj.name;
                end
            end
            if iscell(aname)
                names = cell(size(aname));
                for i = 1:length(aname)
                    if ~ischar(aname{i})
                        names{i} = obj.props(aname{i}.name);
                    else
                        names{i} = obj.props(aname{i});
                    end
                end
                res = vertcat(names{:});
                return
            end
            res = indicator_fun.allproperties;
            ndx1 = strcmpi(res.Name, aname);
            if ~any(ndx1)
                %if ~strncmpi(aname, '-a', 2)
                %user defined functions should have props as parameter
                ndx2 = strcmp(obj.parameter_names, 'props');
                if ~any(ndx2) && ~strcmp(obj.name, obj.dispname)
                    %other derived function than in the list
                    res = obj.props(obj.name);
                    test = obj.outfunct(rand(1, 10));
                    res.dim_reduct = numel(test) == 1;
                    res.Name{1} = obj.dispname;
                    res.vectorized = false;
                elseif ~isempty(obj.parameters)
                    %user defined function
                    res = obj.parameters{ndx2};
                    if isstruct(res)
                        if ~isfield(res, 'description')
                            res.description = {'user defined function'};
                        end
                        if exist('struct2table', 'file')
                            res = struct2table(res, 'asArray', true);
                        end
                    end
                    if isa(res, 'table') && ~any(strcmp(res.Properties.VariableNames, 'dim_reduct'))
                        res.dim_reduct = false;
                    elseif isa(res, 'dataset') && ~any(strcmp(res.Properties.VarNames, 'dim_reduct'))
                        res.dim_reduct = false;
                    elseif ~isfield(res, 'dim_reduct')
                        res.dim_reduct = false;
                    end
                end
                % else
                %     error('dsf')
                % end
            else
                res = res(ndx1, :);
            end
        end
 
        function [obj, a_nanflag] = update_functs(obj, ananflag)
            %update the function handles
            %it is assumed these functions can handle matrices (each column
            %is calculated)
            %functions need to handle nans'
            persistent nanflag ;
            function x = getcols(x, cols, i)
                if nargin == 2
                    i = 1;
                end
                if ~isempty([cols{i}{:}])
                    x = x(:, [cols{i}{:}]);
                elseif i == 2
                    %if corrcolumns=[] then we use all columns except the
                    %datacolumn
                    if size(x, 2) < 2
                        x = nan(size(x));
                    else
                        x(:, [cols{1}{:}]) = [];
                    end
                end
            end
            if nargin > 1
                if ~strcmp(ananflag, 'omitnan')
                    nanflag = 'includenan';
                else
                    %run this once to set the nanflag
                    nanflag = ananflag; %use 'includenan' to return nan in case there is any nan value
                end
                if nargout == 0
                    %set the nanflag only
                    return;
                end
            elseif isempty(nanflag)
                nanflag = 'includenan';
            end
            oldmatlab = matlabLessThan([8 5]);
            
            if oldmatlab
                if strcmp(nanflag, 'omitnan')
                    nanflag = '';
                    error('ews:nanflag', 'In this MATLAB version the nanflag ''omitnan'' is not supported, update to newer MATLAB version');
                else
                    nanflag = '';
                end
            end
            %new indicators? --> add function here, nanflag should be
            %supported!
            switch lower(obj.name)
                case 'ar'
                    %if oldmatlab
                    %     myfunct = @(x)indicators.myautoregression_old(getcols(x,obj.cols),obj.parameters{1});
                    %     obj.funct = @(x)indicators.omitnan(myfunct,getcols(x,obj.cols),nanflag);
                    % else
                    obj.funct = @(x)indicators.myautoregression(getcols(x,obj.cols),obj.parameters{1},nanflag);
                    % end
                case 'acf'
                    obj.funct = @(x)indicators.acf(getcols(x,obj.cols),obj.parameters{1},nanflag);
                case 'slope'
                    obj.funct = @(x)indicators.TS_slope([],getcols(x,obj.cols),1,nanflag).*sign(indicators.TS_slope([],getcols(x,obj.cols),2,nanflag));
                case 'd2xdt2'
                    obj.funct = @(x)indicators.TS_slope([],getcols(x,obj.cols),2,nanflag);
                case 'accel'
                    obj.funct = @(x)indicators.TS_slope([],getcols(x,obj.cols),2,nanflag).*sign(indicators.TS_slope([],getcols(x,obj.cols),1,nanflag));
                case 'std'
                    if isempty(nanflag)
                        obj.funct = @(x)std(getcols(x,obj.cols),0,1);
                    else
                        obj.funct = @(x)std(getcols(x,obj.cols),0,1,nanflag);
                    end
                case 'var'
                    if isempty(nanflag)
                        obj.funct = @(x)var(getcols(x,obj.cols),0,1);
                    else
                        obj.funct = @(x)var(getcols(x,obj.cols),0,1,nanflag);
                    end
                case 'skewness'
                    obj.funct = @(x)indicators.myskewness(getcols(x,obj.cols),nanflag);
                case 'mean'
                    if isempty(nanflag)
                        obj.funct = @(x)mean(getcols(x,obj.cols),1);
                    else
                        obj.funct = @(x)mean(getcols(x,obj.cols),1,nanflag);
                    end
                case 'cov'
                    if isempty(nanflag)
                        obj.funct = @(x)indicators.mycov(getcols(x,obj.cols,1),getcols(x,obj.cols,2),'');
                    else
                        if strcmp(nanflag, 'omitnan')
                            obj.funct = @(x)indicators.mycov(getcols(x,obj.cols,1),getcols(x,obj.cols,2),'omitrows');
                        else
                            obj.funct = @(x)indicators.mycov(getcols(x,obj.cols,1),getcols(x,obj.cols,2),'includenan');
                        end
                    end
                case 'crosscorr'
                    if strcmp(nanflag, 'omitnan')
                        obj.funct = @(x)corr(getcols(x,obj.cols,1),getcols(x,obj.cols,2),'rows','complete');
                    elseif ~isempty(nanflag)
                        obj.funct = @(x)corr(getcols(x,obj.cols,1),getcols(x,obj.cols,2),'rows','all');
                    else
                        obj.funct = @(x)corr(getcols(x,obj.cols,1),getcols(x,obj.cols,2));
                    end
                case 'abscorr'
                    if strcmp(nanflag, 'omitnan')
                        obj.funct = @(x)abs(corr(getcols(x,obj.cols,1),getcols(x,obj.cols,2),'rows','complete'));
                    elseif ~isempty(nanflag)
                        obj.funct = @(x)abs(corr(getcols(x,obj.cols,1),getcols(x,obj.cols,2),'rows','all'));
                    else
                        obj.funct = @(x)abs(corr(getcols(x,obj.cols,1),getcols(x,obj.cols,2)));
                    end
                case 'cv'
                    if isempty(nanflag)
                        obj.funct = @(x)std(getcols(x,obj.cols),0,1)./mean(getcols(x,obj.cols),1);
                    else
                        obj.funct = @(x)std(getcols(x,obj.cols),0,1,nanflag)./mean(getcols(x,obj.cols),1,nanflag);
                    end
                case 'compress'
                    obj.funct = @(x)indicators.compressratio(getcols(x,obj.cols),obj.parameters{1},nanflag);
                case 'kbdm'
                    obj.funct = @(x)indicators.KBDM(getcols(x,obj.cols),obj.parameters{1},nanflag);
                case 'mse'
                    %set distamce measure to chebychev, and tau to mtau (no
                    %coarse graining
                    obj.funct = @(x)indicators.MSE(getcols(x,obj.cols),obj.parameters{1},...
                        obj.parameters{2},obj.parameters{3},'chebychev',nanflag);
                case 'sampen'
                    %set distamce measure to chebychev, and tau to 1 (no
                    %coarse graining
                    obj.funct = @(x)indicators.SampEn(getcols(x,obj.cols),obj.parameters{1},...
                        obj.parameters{2},1,'chebychev',nanflag);
                case 'klempelziv'
                    obj.funct = @(x)indicators.KLempelZiv(getcols(x,obj.cols),obj.parameters{1},nanflag);
                case 'dfa'
                    DFA = @(x)indicators.DFA_fun(x,obj.parameters{1},obj.parameters{2});
                    obj.funct = @(x)indicators.omitnan(DFA,getcols(x,obj.cols),nanflag);
                case 'degfinger'
                    obj.funct = @(x)indicators.degfinger(getcols(x,obj.cols),nanflag);
                case 'explvar'
                    obj.funct = @(x)indicators.explvar(getcols(x,obj.cols),nanflag);
                case 'pcavar'
                    obj.funct = @(x)indicators.pcavar(getcols(x,obj.cols),nanflag);
                case 'maf'
                    obj.funct = @(x)indicators.MAF(getcols(x,obj.cols),nanflag);
                case 'mafac'
                    obj.name = 'maf';
                    obj.funct = @(x)indicators.MAF(getcols(x,obj.cols),nanflag);
                    obj.outfunct = @(maf)indicators.mafac(maf,nanflag);
                case 'mafvar'
                    obj.name = 'maf';
                    obj.funct = @(x)indicators.MAF(getcols(x,obj.cols),nanflag);
                    obj.outfunct = @(maf)indicators.mafvar(maf,nanflag);
                case 'mafeig'
                    obj.name = 'maf';
                    obj.funct = @(x)indicators.MAF(getcols(x,obj.cols),nanflag);
                    obj.outfunct = @(maf)indicators.mafeig(maf);
                otherwise
                    if ~any(strcmp(obj.parameter_names, 'props')) %user defined functions?
                        tab = indicator_fun.allproperties;
                        if any(strcmpi(obj.name, tab.Name))
                            error('indicator_fun:notimplem', 'function "%s" not implemented,but should be', obj.name);
                        else
                            obj.funct = [];
                        end
                    else
                        user_funct = obj.parameters{strcmp(obj.parameter_names, 'userfunct')};
                        obj.funct = @(x)user_funct(getcols(x,obj.cols));
                    end
            end
            if nargout == 2
                a_nanflag = nanflag;
            end

        end
 
        function disp(obj)
            function s = col2str(c)
                if isempty(c) || isempty(c{1})
                    s = ':';
                elseif length(c{1}) == 1
                    s = int2str(c{1});
                else
                    s = mat2str([c{1}]);
                end
            end
            fprintf('class indicator_fun with properties:\n\n')
            fprintf('%15s:    ''%s''\n', 'name', obj.dispname)
            fprintf('%15s:    %d\n', 'nodetrending', obj.nodetrending)
            if isempty(obj.funct)
                fprintf('%15s:    %s\n', 'funct', 'ERROR: no function')
            else
                fprintf('%15s:    %s\n', 'funct', func2str(obj.funct))
            end
            if ~isempty(obj.outfunct)
                fprintf('%15s:    %s\n', 'outfunct', func2str(obj.outfunct))
            end
            if numel(obj.cols) == 1
                fprintf('%15s:    (%s)\n', 'cols', col2str(obj.cols{1}));
            elseif numel(obj.cols) == 2
                fprintf('%15s:    (%s, %s)\n', 'cols', col2str(obj.cols{1}), col2str(obj.cols{2}));
            end
            for i = 1:length(obj.parameter_names)
                if isnumeric(obj.parameters{i}) && length(obj.parameters{i}) == 1
                    fprintf('%15s:    %g\n', ['get(''', obj.parameter_names{i}, ''')'], obj.parameters{i})
                end
                if ischar(obj.parameters{i}) && length(obj.parameters{i}) == 1
                    fprintf('%15s:    ''%s''\n', ['get(''', obj.parameter_names{i}, ''')'], obj.parameters{i})
                end
            end
        end
        
        function res = get(obj, param)
            ndx = strcmp(param, obj.parameter_names);
            if any(ndx)
                res = obj.parameters{ndx};
            else
                s = sprintf('%s ', obj.parameter_names{:});
                error('parameter unknown, valid parameters: %s', s);
            end
        end
        
        function obj = set(obj, varargin)
            %any_error = false;
            if nargin == 2 %isstruct(varargin{1})
                opts = varargin{1};
                if isfield(opts, 'datacolumn')
                    if iscell(opts.datacolumn)
                        obj.cols{1} = opts.datacolumn;
                    else
                        obj.cols{1} = {opts.datacolumn};
                    end
                end
 
                if isfield(opts, 'corrcolumns')
                    if iscell(opts.corrcolumns)
                        obj.cols{2} = opts.corrcolumns;
                    else
                        obj.cols{2} = {opts.corrcolumns};
                    end
                end
                if isfield(opts, 'nanflag')
                    update_functs(obj, opts.nanflag);
                end

                %v = {};
                for i = 1:length(obj.parameter_names)
                    if isfield(opts, obj.parameter_names{i})
                        obj.parameters{i} = opts.(obj.parameter_names{i});
                    end
                end
 
            else
                for i = 1:2:length(varargin)
                    ndx = strcmpi(varargin{i}, obj.parameter_names);
                    if any(ndx)
                        if strcmp(varargin{i}, 'props')
                            %the props of user defined function can be set as
                            %struct/table, with only the non-default fields. 
                            default = indicator_fun.allproperties(true);
                            default.Name{1} = obj.dispname;
                            p = varargin{i + 1};
                            if isstruct(p)
                                p = struct2table(p, 'AsArray', true);
                            end
                            for j = 1:length(p.Properties.VariableNames)
                                default.(p.Properties.VariableNames{j}) = p.(p.Properties.VariableNames{j});
                            end
                            obj.parameters{ndx} = default;
                        else
                            obj.parameters{ndx} = varargin{i + 1};
                        end
                    elseif strcmp(varargin{i}, 'nanflag')
                        %this property is the same for all instances of
                        %indicator_fun
                        obj.update_functs(varargin{i + 1});
                    elseif strcmp(varargin{i}, 'funct')
                        obj.funct = varargin{i + 1};
                    elseif strcmp(varargin{i}, 'datacolumn')
                        if iscell(varargin{i + 1})
                            obj.cols{1} = varargin{i + 1};
                        else
                            obj.cols{1} = varargin(i + 1);
                        end
                    elseif strcmp(varargin{i}, 'corrcolumns')
                        if numel(obj.cols) == 2
                            if iscell(varargin{i + 1})
                                obj.cols{2} = varargin{i + 1};
                            else
                                obj.cols{2} = varargin(i + 1);
                            end
                        end
                    elseif strcmp(varargin{i}, 'cols')
                        %should be double cell, but is corrected here
                        %the correlation column is by default empty
                        cols1 = varargin{i + 1};
                        cols2 = {{}, {}};
                        for i2 = 1:numel(cols1)
                            if iscell(cols1)
                                if iscell(cols1{i2})
                                    %this is actually already correct
                                    cols2{i2} = cols1{i2};
                                else
                                    %double cell
                                    cols2{i2} = cols1(i2);
                                end
                            else
                                %no cell at all
                                cols2{i2} = {cols1(i2)};
                            end
                        end
                        obj.cols = cols2;
                    elseif strcmp(varargin{i}, 'name')
                        obj.name = varargin{i + 1};
                        if isempty(obj.dispname)
                            obj.dispname = varargin{i + 1};
                        end
                    elseif any(strcmpi(varargin{i}, {'dfawin','dfaorder','arlag'}))
                        ndx=strcmpi(varargin{i}, {'dfawin','dfaorder','arlag'});
                        newnames={'dfa_win','dfa_order','ar_lag'};
                        obj=obj.set(newnames{ndx},varargin{i+1});                              
                    elseif strcmp(varargin{i}, 'dispname')
                        obj.dispname = varargin{i + 1};
                    elseif strcmp(varargin{i}, 'parameter_names')
                        parnams = varargin{i + 1};
                        if ischar(parnams)
                            parnams = {parnams};
                        end
                        for j = 1:length(parnams)
                            if ~strcmp(parnams{j}, obj.parameter_names)
                                obj.parameter_names{end + 1} = parnams{j};
                            end
                        end
                    else
                        tab = indicator_fun.allproperties;
                        if isa(tab, 'table') && any(strcmp(varargin{i}, tab.Properties.VariableNames))
                            ndx = strcmpi('props', obj.parameter_names);
                            if any(ndx)
                                props = obj.parameters{ndx};
                                props.(varargin{i}) = varargin{i + 1};
                                obj.parameters{ndx} = props;
                                if props.numcols ~= length(obj.cols)
                                    if props.numcols == 2
                                        obj.cols{2} = {};
                                    else
                                        obj.cols(2) = [];
                                    end
                                end
                                if props.nodetrending ~= obj.nodetrending
                                    obj.nodetrending = props.nodetrending;
                                end
                            else
                                error('indicator_fun:set', 'Properties can only be changed in an user defined function');
                            end
                        elseif isa(tab, 'dataset') && any(strcmp(varargin{i}, tab.Properties.VarNames))
                            ndx = strcmpi('props', obj.parameter_names);
                            if any(ndx)
                                props = obj.parameters{ndx};
                                props.(varargin{i}) = varargin{i + 1};
                                obj.parameters{ndx} = props;
                                if props.numcols ~= length(obj.cols)
                                    if props.numcols == 2
                                        obj.cols{2} = {};
                                    else
                                        obj.cols(2) = [];
                                    end
                                end
                                if props.nodetrending ~= obj.nodetrending
                                    obj.nodetrending = props.nodetrending;
                                end
                            else
                                error('indicator_fun:set', 'Properties can only be changed in an user defined function');
                            end
                        end
                    end
                    
                end
            end
            obj = update_functs(obj);
            if nargout == 0
                error('indicator_fun:set', 'The set function needs to create a new object');
            end
            % if any_error
            %     error('some parameters were not known');
            % end
        end
        

        function res = calc(obj, varargin)
            %             if obj.nodetrending || isempty(detrended)
            %                 dats = rawdata;
            %             else
            %                 dats = detrended;
            %             end
            %             if nargin > 3 && ~isempty(rowndx)
            %                 dats = dats(rowndx, :);
            %             end
            %             if nargin > 4 && ~isempty(colndx)
            %                 dats = dats(:, colndx);
            %             end
            %             res = obj.funct(dats);

            res = obj.calcfunct(varargin{:});
            if ~isempty(obj.outfunct)
                res = obj.outfunct(res);
            end
            %             if any(~isreal(res))
            %             %To be used in debugging
            %                 warning('indicator_fun:maf', 'Complex indicator (%s) values encountered', obj.name);
            %                 res = nan(size(res));
            %             end
        end
        function res = calcfunct(obj, rawdata, detrended, rowndx, colndx)
            if obj.nodetrending || isempty(detrended)
                dats = rawdata;
            else
                dats = detrended;
            end
            if nargin > 3 && ~isempty(rowndx)
                dats = dats(rowndx, :);
            end
            if nargin > 4 && ~isempty(colndx)
                dats = dats(:, colndx);
            end
            res = obj.funct(dats);

            %             if any(~isreal(res))
            %             %To be used in debugging
            %                 warning('indicator_fun:maf', 'Complex indicator (%s) values encountered', obj.name);
            %                 res = nan(size(res));
            %             end
        end
    end
end





% 
% function [A, E] = GMAF(x, lag)
%     % [A,E]=GMAF(x,lag) performs generalized maximum autocorrelation factor 
%     %   analysis on the N-by-P data matrix x with time-lag in autocorrelation
%     %   specified in lag, and returns the maximum autocorrelation coefficients 
%     %   in A.  
%     %
%     %   Rows of X correspond to observations, and columns to variables.  
%     %   y=x*A gives one maximum aucororrelation factor in each column of y.
%     %   1-E/2 gives the autocorrelations of maximum aucororrelation factors. 
%     %
%     [n, ~] = size(x);
%     C = (x' * x) / n;
%     D = x(lag + 1:end, :) - x(1:end - lag, :);
%     V = (D' * D) / (n - lag);
%     [A, E] = eig(C \ V);
%     E = diag(E);
%     [E, I] = sort(E);
%     A = A(:, I);
% end



function [Y1, Y2] = remove_nanpairs(Ys, alag)
    Y1 = Ys(1:end - alag);
    Y2 = Ys(1 + alag:end);
    ndx = ~(isnan(Y1) | isnan(Y2));
    Y1 = Y1(ndx);
    Y2 = Y2(ndx);
end


