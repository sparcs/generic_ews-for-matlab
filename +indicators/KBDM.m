function res = KBDM(data, overlap,nanflag)
    %KBDM calculates the Kolmogorov complexity using the Block decomposition method 
    %of a data series
    %by default the overlap is zero
    %The file "bdmkey.mat" is needed!
    %
    %See Dakos, V. and F. Soler-Toscano. 2016. Measuring complexity to infer 
    %changes in the dynamics of ecological systems under stress. Ecological Complexity 32.
    %
    %Usage:
    %res=BDM(data) calculates Kbdm without overlap
    %res=BDM(data, overlap) (overlap between 0-7)
    %
    if nargin < 2
        %default overlap
        overlap = 0;
    end
    if nargin < 3
        %default nanflag
        nanflag = 'includenan';
    end
    if overlap > 7 || overlap < 0
        %give error when overlap is wrong
        error('The overlap should be between 0 and 7')
    end
    if strcmp(nanflag,'includenan')&&any(isnan(data(:)))
        res=nan;
        return
    end
    if numel(data) < 8
        res = NaN;
        %not enough data
        return;
    end
    %load the reference blocks and their ctm values
    load('indicators/bdmkey.mat','ctmvalues','ref_blocks');
    %
    if islogical(data)
        %if data is already logical do nothing
        ldata = data;
    else
        %Mean as threshold!
        %Logical data 1 if data>mean(data)
        ldata = data >= mean(data,'omitnan'); %always omitnan here otherwise everything is true!
        nanvalues = isnan(data);
    end
    if overlap == 0
        %this is more simple and efficient than overlap
        %calculate number of rows, rounded down to the nearest integer
        nrows = floor(length(ldata) / 8);
        %reshape the data to get non-overlapping segments of 8
        ldata = reshape(ldata(1:8 * nrows), 8, nrows)';
        nanvalues = reshape(nanvalues(1:8 * nrows), 8, nrows)';
        %
        %find the segments in the reference blocks, save indices ndx of
        %ref_blocks
        [~, ndx] = ismember(ldata, ref_blocks, 'rows');
        ndx(any(nanvalues,2),:)=[];
    else
        %fast despite the loop
        %start indices of blocks
        istart = 1:8 - overlap:length(ldata) - 8;
        %make logical variable with enough space
        ldata2 = false(length(istart), 8);
        nanvalues2 = ldata2;
        k = 1;
        for i = istart  %loop the start indices
            %fill the variable with segments
            ldata2(k, :) = ldata(i:i + 7);
            nanvalues2(k, :) = nanvalues2(i:i + 7);
            k = k + 1;
        end
        
        %find the segments in the reference blocks
        [~, ndx] = ismember(ldata2, ref_blocks, 'rows');
         ndx(any(nanvalues2,2),:)=[];
    end
 
    %find unique indices, orig_ndx is used for frequencies
    [unique_ndx, ~, orig_ndx] = unique(ndx);
 
    %frequency of the unique indices (if there are many repetitions the BDM
    %is much lower as log2(n)<<sum(CTM)
    freq = accumarray(orig_ndx, 1);
 
    %sum (log(frequency)+log(CTM)) log2(1)=0!
    res = sum(ctmvalues(unique_ndx)) + sum(log2(freq));

end

