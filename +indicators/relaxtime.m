function r = relaxtime(times, data, nbins, maxtime, nlags)
    if nargin == 1 && isstruct(times)
        r = times;
        plot(r.bincenter, r.corrs, 'o-');
        set(gca, 'yscale', 'log')
        xlabel('delta-t');
        ylabel('autocorrelation')
        hold on
        
        plot(r.bincenter, r.acf(r.bincenter), 'r-')
        hold off
        return
    end

    if nargin < 3
        nbins = 20;
    end
    if nargin < 4
        maxtime = mean(diff(times)) * 20;
    end
    if nargin < 5
        nlags = nbins + 1;
    end
    seg_times = makesegments(times, 1:nlags, false);
    delta_times = seg_times - seg_times(1, :);
    ndx = delta_times < maxtime & delta_times > 0;
    seg_data = makesegments(data, 1:nlags, false);
    seg_data0 = repmat(data, 1, nlags + 1)';
    times = delta_times(ndx);
    X1 = seg_data(ndx);
    X0 = seg_data0(ndx);
    [dtimes, binedges] = discretize(times, nbins);
    bincenter = binedges(1:end - 1) + (binedges(2) - binedges(1)) / 2;
    corrs = nan(nbins, 1);
    npoints = nan(nbins, 1);
    for i = 1:nbins
        ndx = dtimes == i;
        if sum(ndx) > 3
            corrs(i) = corr(X1(ndx), X0(ndx),'Rows','complete');
            npoints(i) = sum(ndx);
        end
    end
    %plot(bincenter,corrs,'o-');
    ndx1 = ~isnan(corrs);
    corrs = corrs(ndx1);
    bincenter = bincenter(ndx1);
    npoints = npoints(ndx1);
    ndx2=corrs>0.2;
    fndx2=find(~ndx2,1);
    if ~isempty(fndx2)
       ndx2(fndx2:end)=false;
    end

    lm = fitlm(bincenter(ndx2), log(corrs(ndx2)), 'y ~ x1 + 1', 'weights', npoints(ndx2));
    r.lambda = lm.Coefficients.Estimate(2);
    r.relaxtime = -1 / lm.Coefficients.Estimate(2);
    r.acf = @(x)exp(lm.feval(x));
    r.bincenter = bincenter;
    r.corrs = corrs;
    r.npoints = npoints;
    r.lm=lm;
 

end


function dat1 = makesegments(dat, taus, dofillnans)
    %simple function to make segments of the time series dat
    %we use this for bootstrapping
    %
    % example:
    % makesegments(1:10,1:5)
    % 
    % ans =
    % 
    %      1     2     3     4     5     6
    %      2     3     4     5     6     7
    %      3     4     5     6     7     8
    %      4     5     6     7     8     9
    %      5     6     7     8     9    10
    %      6     7     8     9    10   NaN
    %      7     8     9    10   NaN   NaN
    %      8     9    10   NaN   NaN   NaN
    %      9    10   NaN   NaN   NaN   NaN
    %     10   NaN   NaN   NaN   NaN   NaN
    if nargin < 3
        dofillnans = false;
    end
    N = length(dat);
    dat1 = nan(numel(taus) + 1, N);
    dat1(1, :) = dat(1:N);
    for i = 1:numel(taus)
        dat1(i + 1, 1:N - taus(i)) = dat(1 + taus(i):end);
    end
    if dofillnans
        dat1 = fillnans(dat1);
    end
end

function segmentdata = fillnans(segmentdata)
    for i = 1:size(segmentdata, 2)
        n = find(isnan(segmentdata(:, i)), 1);
        if ~isempty(n)
            segmentdata(n + 1:end, i) = NaN;
        end
    end
    ndx = ~all(isnan(segmentdata), 1);
    segmentdata = segmentdata(:, ndx);
end


