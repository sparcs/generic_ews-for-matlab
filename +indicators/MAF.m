function [Wmaf, K] = MAF(data, nanflag)
    % MAF Maximum Autocorrelation Factors
    % implemented algorithm from https://arxiv.org/pdf/1502.01073.pdf
    % Function that decomposes data into a new space where
    % the first column is the vector on which the data has the highest
    % autocorrelation.
    if nargin < 2
        nanflag = 'includenan';
    end
    if size(data, 1) <= size(data, 2)
        %In case numrows<numcols you often get complex MAF values
        error('indicator_fun:maf:too_few_rows', 'Identification problem: too short time series (%dx%d) to calculate the MAF factors (more rows than columns needed)', size(data, 1), size(data, 2));
    end
    if strcmp(nanflag, 'omitnan')
        m = indicators.omitnan(@mean, data, nanflag);
    else
        m = mean(data);
    end
    data = bsxfun(@minus, data, m); %for older matlab versions
    Sz = indicators.mycov(data, data, nanflag);
    %decompose cov matrix
    try
        [U, D] = eig(Sz);
        %transformation to make it spherical
        X = data * U * D^ -0.5 * U';
    catch err
        if ~strcmp(err.identifier, 'MATLAB:eig:matrixWithNaNInf')
            rethrow(err);
        else
            U = nan(size(Sz));
            D = U;
            X = U;
        end
    end
  
    %delta-x the increments of these factors
    dX = X(1:end - 1, :) - X(2:end, :);
    %decompose the increments
    Sd = indicators.mycov(dX, dX, nanflag);
    try
        %highest variance in the first differences
        [V, K] = eig(Sd);
        %transfer back to original scale
        % direction of highest variance
        Wmaf = U * D^ -0.5 * U' * V;
    catch err
        if ~strcmp(err.identifier, 'MATLAB:eig:matrixWithNaNInf')
            rethrow(err);
        else
            V = nan(size(Sd));
            K = V;
            Wmaf = V;
        end
    end
    %MAF factors 
    %scale factors
    %Wmaf = normc(Wmaf); %requires machine learning toolbox
    %is though easily done without toolbox
    for i = 1:size(Wmaf, 2)
        %standardize length of the column vectors to one
        Wmaf(:, i) = Wmaf(:, i) ./ norm(Wmaf(:, i));
    end

    if any(~isreal(Wmaf(:)))
        %Complex MAF factors seem to be due to identifaction problems
        %this should not happen if size of time series is long enough
        warning('indicator_fun:maf', 'Complex MAF factors encountered');
        Wmaf = nan(size(Wmaf));
        K = nan(size(K));
    end
end