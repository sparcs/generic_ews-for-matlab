function res = acf(data, alag, nanflag)
    if nargin < 2
        alag = 1;
    end
    
    %autocorrelation function like in R
    if strcmp(nanflag, 'omitnan')
        if size(data, 2) > 1
            res = zeros(1, size(data, 2));
            for i = 1:size(data, 2)
                res(i) = indicators.acf(data(:, i), alag, nanflag);
            end
            return
        else
            data = data(~isnan(data));
        end
    end
    n = size(data, 1);
    s = var(data);
    mu = mean(data);
    data = bsxfun(@minus, data, mu); %for old matlab versions
    Xt = data(1:end - alag, :);
    Xtk = data(1 + alag:end, :);
    res = 1 ./ (n - 1) ./ s .* sum(Xt .* Xtk);
end