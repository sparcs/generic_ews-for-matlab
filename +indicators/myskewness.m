function res = myskewness(data, nanflag)
%skewness with nanflag
    if (isempty(nanflag)||strcmp(nanflag, 'includenan')) && any(isnan(data(:)))
        res = nan(1, size(data, 2));
    else
        res = skewness(data, 1, 1);
    end
end
