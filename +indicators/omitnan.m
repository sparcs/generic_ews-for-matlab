function res = omitnan(func, data, nanflag)
    if nargin < 3
        nanflag = 'includenan';
    end
    if strcmp(nanflag, 'omitnan')
        nans = isnan(data);
        if any(nans(:))
            res = zeros(1, size(data, 2));
            for i = 1:size(data, 2)
                res(i) = func(data(~nans(:, i), i));
            end
        else
            res = func(data);
        end
    else
        res = func(data);
    end
end