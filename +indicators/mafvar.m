function res = mafvar(data, nanflag)
    %variance of the first MAF

    %maf directions
    Wmaf = indicators.MAF(data, nanflag);
    %projection on the first axis
    proj2 = data * Wmaf(:, 1);
    %variance
    if strcmp(nanflag, 'omitnan')
        res = var(proj2(~isnan(proj2)));
    else
        res = var(proj2);
    end
end