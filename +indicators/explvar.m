function [maxE, maxEndx, V, E] = explvar(data, nanflag)
    %explained variance
 
    if nargin < 2
        nanflag = 'includenan';
    end
    %covariance matrix
    C = indicators.mycov(data, data, nanflag);
    %eigenvalues of covariance matrix
    try
        [V, E] = eig(C);
    catch err
        if ~strcmp(err.identifier, 'MATLAB:eig:matrixWithNaNInf')
            rethrow(err);
        else
            V = nan(size(C));
            E = V;
        end
    end
 
    %scaled eigenvalues
    E = diag(real(E)) / sum(diag(real(E)));
    %max scaled eigenvalue
    [maxE, maxEndx] = max(E);
end