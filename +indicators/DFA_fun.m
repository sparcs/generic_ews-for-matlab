function [A, stats] = DFA_fun(data, pts, order)

    % -----------------------------------------------------
    % DESCRIPTION:
    % Function for the DFA analysis.

    % INPUTS: 
    % data: a one-dimensional data vector.
    % pts: sizes of the windows/bins at which to evaluate the fluctuation
    % order: (optional) order of the polynomial for the local trend correction.
    % if not specified, order == 1;

    % OUTPUTS: 
    % A: a 2x1 vector. A(1) is the scaling coefficient "alpha",
    % A(2) the intercept of the log-log regression, useful for plotting (see examples).
    % F: A vector of size Nx1 containing the fluctuations corresponding to the
    % windows specified in entries in pts.
    % -----------------------------------------------------

    % Checking the inputs
    
    if nargin < 2
        pts = round(numel(data) / 10);
        if pts < 50
            pts = 50;
        end
    end
    if numel(pts) == 1
        pts = unique(round(logspace(log10(5), log10(pts), 50)));
    end
    if numel(pts) == 2
        pts = unique(round(logspace(log10(pts(1)), log10(pts(2)), 50)));
    end
    if nargin < 3
        order = 1;
    end
    if size(data, 2) > 1
        A = zeros(1, size(data, 2));
        for i = 1:size(data, 2)
            [A(i), stats(i)] = indicators.DFA_fun(data(:, i), pts, order);
        end
        return
    end

    
    sz = size(data);
    if sz(1) < sz(2)
        data = data';
    end

    exit = 0;

    if min(pts) == order + 1
        disp(['WARNING: The smallest window size is ' num2str(min(pts)) '. DFA order is ' num2str(order) '.'])
        disp('This severly affects the estimate of the scaling coefficient')
        disp('(If order == [] (so 1), the corresponding fluctuation is zero.)')
    elseif min(pts) < (order + 1)
        disp(['ERROR: The smallest window size is ' num2str(min(pts)) '. DFA order is ' num2str(order) ':'])
        disp(['Aborting. The smallest window size should be of ' num2str(order + 1) ' points at least.'])
        exit = 1;
    end

    if exit == 1
        return
    end


    % DFA
    npts = numel(pts);

    F = zeros(npts, 1);
    N = length(data);


    for h = 1:npts
        
        w = pts(h);
        
        n = floor(N / w);
        Nfloor = n * pts(h);
        D = data(1:Nfloor);
        
        y = cumsum(D - mean(D));
        
        bin = 0:w:(Nfloor - 1);
        vec = 1:w;
        
        coeff = arrayfun(@(j) polyfit(vec',y(bin(j) + vec),order), 1:n, 'uni', 0);
        y_hat = cell2mat(cellfun(@(y) polyval(y,vec), coeff, 'uni', 0));
        F(h) = mean(bsxfun(@minus, y, y_hat').^2)^0.5;
        
    end
    Fnan = isnan(F);
    if ~all(Fnan)
        F = F(~Fnan);
        pts = pts(~Fnan);
    end
    y = log(F)';
    x = log(pts);
    if nargout == 0
        figure
        plot(x, y, 'o')
    end
    [A, stat] = polyfit(x, y, 1);
    if nargout == 2
        [A1, stat1] = polyfit(x(1:round(length(x) / 2)), y(1:round(length(x) / 2)), 1);
        stats.R2_half = 1 - (stat1.normr / norm(y(1:round(length(x) / 2)) - mean(y(1:round(length(x) / 2)))))^2;
        stats.R2 = 1 - (stat.normr / norm(y - mean(y)))^2;
        stats.F = F';
        stats.pts = pts;
        stats.fit = A;
        stats.fit_half = A1;
    end
    A = A(1);
end