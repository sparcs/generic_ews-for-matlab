
function [result, proj] = degfinger(data, nanflag)
    %degenerate fingerprinting
 
    if nargin < 2
        nanflag = 'includenan';
    end
    %PCA
    %eigenvectors cov matrix:
    [~, maxEndx, V] = indicators.explvar(data, nanflag);
    %projection on first PCA
    proj = data * (V(:, maxEndx));

    %autocorrelation on first axis
    if size(proj, 2) > 1
        result = NaN;
        return;
    end
    proj1 = proj(1:end - 1);
    proj2 = proj(2:end);
    if strcmp(nanflag, 'omitnan')
        ndx = ~isnan(proj1) & ~isnan(proj2);
        if sum(ndx) < 2
            result = NaN;
        else
            result = corr(proj1(ndx), proj2(ndx));
        end
    else
        result = corr(proj1, proj2);
    end
end
