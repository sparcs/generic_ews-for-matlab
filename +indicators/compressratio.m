function [rat, details] = compressratio(data, number_classes, nanflag)
    %compressratio - how well can a time series be compressed (as complexity measure)
    %This function is an adapted version of dzip (by Michael Kleder) which compresses based
    %on the lossless GZIP algorithm (using java.util.zip). GZIP (GNU gzip)
    %is an open source compression software that uses the DEFLATE algorithm.
    %
    %Before compressing the time series is discretized in by default 20 classes and 
    %stored as integer bytes. This makes it easier to find patterns in the
    %time series, especially to pick up regular patterns in noisy data.
    %
    %usage:
    %[ratio,details]=compressratio(data,number_classes)
    % set number_classes to inf for the original data
    % ratio = compression ratio: N_zip_discr/N_disc: the number of bytes zipped divided by 
    %       number of bytes of discretized data
    % details gives some more details as struct with the following fields: 
    %       'class': the class of the original data
    %       'size': the size of the original data
    %       'discretize_classes': the number of classes used to discretize
    %             the data 
    %       'nanflag': the used nanflag
    %       'zipped': compressed original data (uint8)
    %       'zipped_discr': compressed discretized data (uint8)
    %       'compressratio': N_zip/N_orig = number of bytes zipped divided by number of bytes in
    %          original data
    %       'compressratio_discr': N_zip_discr/N_disc = number of bytes zipped divided by number of bytes in
    %          discretized data 
 
    if nargin < 2
        number_classes = 20;
    end
    if ischar(data)
        data = data + 0;
    end
    if nargin < 3
        %default nanflag
        nanflag = 'includenan';
    end
    details=struct('class',class(data),'size',size(data),'discretize_classes',number_classes,'nanflag',nanflag,'zipped',[],'zipped_discr',[],'compressratio',nan,'compressratio_discr',nan);

    if strcmp(nanflag,'includenan')&&any(isnan(data(:)))
        rat=nan;
        return
    end
    if number_classes <= intmax('uint32')
        if any(isnan(data))
            data_discr = discretize(data, number_classes - 1);
            data_discr(isnan(data_discr)) = number_classes;
        else
            data_discr = discretize(data, number_classes);
        end
    else
        data_discr = data;
    end
    if number_classes <= 256
        %one byte integer
        data_discr = uint8(data_discr(:));
    else
        if number_classes <= intmax('uint16')
            %two byte integer
            data_discr = uint16(data_discr);
        elseif number_classes <= intmax('uint32')
            %four byte integer
            data_discr = uint32(data_discr);
        end
        %cast to vector of bytes
        data_discr = typecast(data_discr(:), 'uint8');
    end
    %zip the data only without storage of info about original data
    zipped_discr = dzip_withoutclassinfo(data_discr);
 
    %compress ratio
    rat = numel(zipped_discr) / numel(data_discr);
    if nargout == 2
        %some more detail
        try
            %this takes computation time
            details.zipped = dzip_withoutclassinfo(typecast(data(:), 'uint8'));
        catch err
            if strcmp(err.identifier, 'MATLAB:Java:GenericException')
                %heap overflow error that occurs with large data sets
                warning(err.message);
                details.zipped = [];
            else
                rethrow(err);
            end
        end
        details.zipped_discr = zipped_discr;
        if isempty(details.zipped)
            details.compressratio = NaN;
        else
            details.compressratio = numel(details.zipped) / numel(typecast(data(:), 'uint8'));
        end
        details.compressratio_discr = rat;
    end
end

function Z = dzip_withoutclassinfo(M)
    % DZIP_WITHOUTCLASSINFO - losslessly compress data into smaller memory space
    % based on DZIP. Just removed the storage of the class and size of the original
    % variable to get a more precise complexity measureh
    %
    % USAGE:
    % Z = dzip(M)
    %
    % VARIABLES:
    % M = variable to compress
    % Z = compressed output
    %
    % NOTES: (1) The input variable M can be a scalar, vector, matrix, or
    %            n-dimensional matrix
    %        (2) The input variable must be a non-complex and full (meaning
    %            matrices declared as type "sparse" are not allowed)
    %        (3) Permitted input types include: double, single, logical,
    %            char, int8, uint8, int16, uint16, int32, uint32, int64,
    %            and uint64.
    %        (4) In testing, DZIP compresses several megabytes of data per
    %            second.
    %        (5) In testing, random matrices of type double compress to about
    %            75% of their original size. Sparsely populated matrices or
    %            matrices with regular structure can compress to less than
    %            1% of their original size. The realized compression ratio
    %            is heavily dependent on the data.
    %        (6) Variables originally occupying very little memory (less than
    %            about half of one kilobyte) are handled correctly, but
    %            the compression requires some overhead and may actually
    %            increase the storage size of such small data sets.
    %            One exception to this rule is noted below.
    %        (7) LOGICAL variables are compressed to a small fraction of
    %            their original sizes.
    %        (8) The DUNZIP function decompresses the output of this function
    %            and restores the original data, including size and class type.
    %        (9) This function uses the public domain ZLIB Deflater algorithm.
    %       (10) Carefully tested, but no warranty; use at your own risk.
    %       (11) Michael Kleder, Nov 2005

    %     s = size(M);
    %     c = class(M);
    %     cn = find(strcmp(c, {'double', 'single', 'logical', 'char', 'int8', 'uint8',  ...
    %         'int16', 'uint16', 'int32', 'uint32', 'int64', 'uint64'}));
    %     if cn == 3 || cn == 4
    %         M = uint8(M);
    %     end
    %M = typecast(M(:), 'uint8');
    %    M = [uint8(cn); uint8(length(s)); typecast(s(:), 'uint8'); M(:)];
    f = java.io.ByteArrayOutputStream();
    g = java.util.zip.GZIPOutputStream(f); %gzip version
    %    g = java.util.zip.DeflaterOutputStream(f); %zip version
    g.write(M);
    g.close;
    Z = typecast(f.toByteArray, 'uint8');
    f.close;
end


