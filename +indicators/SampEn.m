%% Function to calculate multiscale sample entropy
%  data is 1D vector with input data
%  m is the embedding dimension
%  r is the distance
%  taus is a vector or scalar containing the course graining numbers
%  distancetype can be 'chebychev' or 'euclidean' (or anything for pdist() 
%  function in matlab)
%
%I think multi-scale refers to coarse graining (check needed)
function y = SampEn(data, m, r, taus, distancetype, nanflag)
    if nargin < 2 || isempty(m)
        m = 3;
    end
    if nargin < 3 || isempty(r)
        r = 0.1;
    end
    if nargin < 4 || isempty(taus)
        taus = 1;
    end
    if nargin < 5 || isempty(distancetype)
        %default nanflag
        distancetype = 'chebychev';
    end
    if nargin < 6
        %default nanflag
        nanflag = 'includenan';
    end
        if strcmp(nanflag,'includenan')&&any(isnan(data(:)))
            y=nan(length(taus),1);
            return
        end
    m1 = m;
    m2 = m + 1;
    nriters = length(taus);
    y = zeros(nriters, 1);
    L = length(data);
    %Course graining to create new time series
    for i = 1:nriters
        tau = taus(i);
        L2 = floor(L / tau);
        %coarse graining
        data1 = data(1:L2 * tau);
        data2 = mean(reshape(data1, [tau, L2]), 1);
    
        %Create X
        X = [];
        for j = 1:m1
            X = [X data2(j:j + L2 - m1 - 1)'];
        end
        distances = pdist(X, distancetype);
        distances = distances(~isnan(distances));
        A = sum(distances <= r) / length(distances);

        X = [];
        for j = 1:m2
            X = [X data2(j:j + L2 - m2)'];
        end
        distances = pdist(X, distancetype);
        distances = distances(~isnan(distances));
        B = sum(distances <= r) / length(distances);
        
        y(i) = -log(B / A);
    end
end


