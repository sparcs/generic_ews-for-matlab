function b = TS_slope(x, y, order, nanflag)
    %TS_slope - Theil–Sen estimator of the slope
    %res=TS_slope(x,y)
    %Calculates the Theil–Sen estimator is a method for robustly 
    %fitting a line to sample points in the plane (simple linear regression) by 
    %choosing the median of the slopes of all lines through pairs of points. 
    %
    %Based on function b = Theil_Sen_Regress(x,y)
    %https://nl.mathworks.com/matlabcentral/fileexchange/34308-theil-sen-estimator
    if nargin < 3 || isempty(order)
        order = 1;
    end
    if nargin < 4
        %default nanflag
        nanflag = 'includenan';
    end
    if strcmp(nanflag,'includenan')&&any(isnan(y(:)))
        b=nan;
        return
    end
    y=y(~isnan(y));
    b = zeros(size(y, 2), 1);
    for j = 1:size(y, 2)
        if isempty(x)
            x = (1:size(y, 1)).';
        end
        if order == 1
             b1=TS_slope1(x,y(:,j));
            %b1 = polyfit(x, y(:, j), 1);
            b(j) = b1(1);
        else
            
            %              case 'slope'
            %                     obj.funct = @(x)indicators.TS_slope([],getcols(x,obj.cols),1);
            %                 case 'd2xdt2'
            %                     obj.funct = @(x)indicators.TS_slope([],getcols(x,obj.cols),2);
            %                 case 'accel'
            %                     obj.funct = @(x)indicators.TS_slope([],getcols(x,obj.cols),2).*indicators.TS_slope([],getcols(x,obj.cols),1);
            
            
            %         b=polyfit(x,y,2);
            %         %diff(a*x^2+b*x+c,2)=b+2*a*x;
            %         %diff(a*x^2+b*x+c,2)=2*a;
            %         b=2*b(1);
            %split in half, check speeding up by cheking the slopes of the
            %first half and the second half
            n = floor(numel(x) / 2);
              b1 = indicators.TS_slope(x(1:n), y(1:n, j), 1);
             b2 = indicators.TS_slope(x(n:end), y(n:end, j), 1);
            b(j) = (b2 - b1) / (x(n) - x(1));
            % b1=polyfit(x,y,2);
            %diff(a*x^2+b*x+c,2)=b+2*a*x;
            %diff(a*x^2+b*x+c,2)=2*a;
            %    b(j)=2*b1(1);
        end
     

        %     if order == 1
        %             %if numel x is large we take a sample
        %     if numel(x)>200
        %         ndx=randperm(numel(x),200);
        %         x=x(ndx);
        %         y=y(ndx);
        %     end
        %         if size(x, 1) == 1
        %             b = NaN;
        %         elseif size(x, 1) == 2
        %             Comb = nchoosek(1:size(x, 1), 2);
        %             b = median(diff(y(Comb), 1, 1) ./ diff(x(Comb), 1, 1));
        %         else
        %             Comb = nchoosek(1:size(x, 1), 2);
        %             b = median(diff(y(Comb), 1, 2) ./ diff(x(Comb), 1, 2));
        %         end
        %     elseif order == 2
        %         n=floor(numel(x)/2);
        %         b1=indicators.TS_slope(x(1:n),y(1:n),1);
        %         b2=indicators.TS_slope(x(n:end),y(n:end),1);
        %         b=(b2-b1)/(x(n)-x(1));
        %             %if numel x is large we take a sample
        % %    if numel(x)>200
        % %         ndx=randperm(numel(x),200);
        % %         x=x(ndx);
        % %         y=y(ndx);
        % %     end
        % %         if size(x, 1) < 2
        % %             b = NaN;
        % %         else
        % %             Comb = nchoosek(1:size(x, 1), 3);
        % %             if size(x,1)==3
        % %                 Comb=Comb';
        % %             end
        % %             yy=y(Comb);
        % %             xx=x(Comb);
        % %             delta1=(xx(:,2)-xx(:,1));
        % %             delta2=(xx(:,3)-xx(:,2));
        % %             diff1=(yy(:,2)-yy(:,1))./delta1;
        % %             diff2=(yy(:,3)-yy(:,2))./delta2;
        % %             b = median((diff2-diff1)./(delta1+delta2)/2);
        % %         end
        %      else
        %          error('order can either be 1 or 2');
        %      end
    end
end

function b = TS_slope1(x, y)
    %TS_slope - Theil–Sen estimator of the slope
    %res=TS_slope(x,y)
    %Calculates the Theil–Sen estimator is a method for robustly 
    %fitting a line to sample points in the plane (simple linear regression) by 
    %choosing the median of the slopes of all lines through pairs of points. 
    %
    %Based on function b = Theil_Sen_Regress(x,y)
    %https://nl.mathworks.com/matlabcentral/fileexchange/34308-theil-sen-estimator
    if numel(x) > 100
        ndx = randperm(numel(x), 100);
        x = x(ndx);
        y = y(ndx);
    end
    %    Comb=randi(numel(x),2000,2);
    if size(x, 1) == 1
        b = NaN;
    elseif size(x, 1) == 2
        Comb = nchoosek(1:size(x, 1), 2);
        b = median(diff(y(Comb), 1, 1) ./ diff(x(Comb), 1, 1), 'omitnan');
    else
        Comb = nchoosek(1:size(x, 1), 2);
        b = median(diff(y(Comb), 1, 2) ./ diff(x(Comb), 1, 2), 'omitnan');
    end
end
