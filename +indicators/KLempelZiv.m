% FUNCTION: KLempelZiv.m
% DATE: 9th Feb 2005
% AUTHOR: Stephen Faul (stephenf@rennes.ucc.ie)
%
% Function for estimating the Kolmogorov Complexity as per:
% "Easily Calculable Measure for the Complexity of Spatiotemporal Patterns"
% by F Kaspar and HG Schuster, Physical Review A, vol 36, num 2 pg 842
%
% Input is a digital string, so conversion from signal to a digital stream
% must be carried out a priori

function complexity = KLempelZiv(data,number_classes,nanflag)
    if nargin < 2
        %default nanflag
        number_classes = 20;
    end
    if nargin < 3
        %default nanflag
        nanflag = 'includenan';
    end
    if strcmp(nanflag,'includenan')&&any(isnan(data(:)))
        complexity=nan;
        return
    end
    if ~isempty(number_classes)
       data= discretize(data, number_classes);
    end

    
    data=data(~isnan(data));
    n = numel(data);
    c = 1;
    l = 1;

    i = 0;
    k = 1;
    k_max = 1;
    stop = 0;

    while stop == 0
        if data(i + k) ~= data(l + k)
            if k > k_max
                k_max = k;
            end
            i = i + 1;
     
            if i == l
                c = c + 1;
                l = l + k_max;
                if l + 1 > n
                    stop = 1;
                else
                    i = 0;
                    k = 1;
                    k_max = 1;
                end
            else
                k = 1;
            end
        else
            k = k + 1;
            if l + k > n
                c = c + 1;
                stop = 1;
            end
        end
    end

    b = n / log2(n);

    % a la Lempel and Ziv (IEEE trans inf theory it-22, 75 (1976), 
    % h(n)=c(n)/b(n) where c(n) is the kolmogorov complexity
    % and h(n) is a normalised measure of complexity.
    complexity = c / b;
end
