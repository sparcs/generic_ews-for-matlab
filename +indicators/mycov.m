function res = mycov(x, y, nanflag)
    %this function is similar to corr but uses other nanflag
    if strcmp(nanflag, 'omitnan')
        ndx = ~(any(isnan(x), 2) | any(isnan(y), 2));
        x = x(ndx, :);
        y = y(ndx, :);
    end
    if size(x, 2) > 1 && size(y, 2) == size(x, 2)
        res = cov(x);
    else
        res = zeros(size(x, 2), size(y, 2));
        for i = 1:size(x, 2)
            for j = 1:size(y, 2)
                covmat = cov(x(:, i), y(:, j));
                res(i, j) = covmat(2, 1);
            end
        end
    end
end