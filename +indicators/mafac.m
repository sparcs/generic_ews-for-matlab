function res = mafac(data, nanflag)
    %MAF AC - maximum autocorrealation in system

    %maf directions
    Wmaf = indicators.MAF(data, nanflag);
    %projection on the first axis
    proj2 = data * Wmaf(:, 1);
    %autocorrelation
    if strcmp(nanflag, 'omitnan')
        res = corr(proj2(1:end - 1), proj2(2:end), 'rows', 'complete');
    else
        res = corr(proj2(1:end - 1), proj2(2:end));
    end
end