%% Function to calculate multiscale sample entropy
%  data is 1D vector with input data
%  m is the embedding dimension
%  r is the distance
%  taus is a vector containing the course graining numbers
%  distancetype can be 'chebychev' or 'euclidean' (or anything for pdist() 
%  function in matlab)
%
function mse = MSE(data, m, r, mtau, distancetype,nanflag)
   if nargin < 2 || isempty(m)
        m = 3;
    end
    if nargin < 3 || isempty(r)
        r = 0.1;
    end
    if nargin < 4 || isempty(mtau)
        mtau = 5;
    end
    if nargin < 5 || isempty(distancetype)
        %default nanflag
        distancetype = 'chebychev';
    end
    if nargin < 6
        %default nanflag
        nanflag = 'includenan';
    end
    taus = 1:mtau;
    %get sample entropy for a range of coarse graining numbers
    samens = indicators.SampEn(data, m, r, taus, distancetype,nanflag);
    %MSE  is area under the curve:
    mse = trapz(taus, samens);
end


