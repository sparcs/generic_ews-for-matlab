
function res = mafeig(data, nanflag)
    %min eigenvalue of MAF directions

    %MAF decomposition
    [~, K] = indicators.MAF(data, nanflag);
    %standardize eigenvalues
    K = diag(K) / sum(diag(K));

    %minus minumum eigenvalue
    res = -min(K);
end
