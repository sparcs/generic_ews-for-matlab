function [result] = pcavar(data, nanflag)
    %variance of the first PCA axis
    if nargin < 2
        nanflag = 'includenan';
    end
    %projection on first PCA
    [~, proj] = indicators.degfinger(data, nanflag);
    if strcmp(nanflag, 'omitnan')
        proj = proj(~isnan(proj));
    end
    %variance of the first PCA axis
    result = var(proj);
end