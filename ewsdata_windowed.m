classdef ewsdata_windowed < ewsdata
    %this is a handle object as there may be overhead in changing the
    %trends
 
    methods (Static = true)
        function result = defaults
            [~,pars]=indicator_fun.allproperties;
            result = struct('winsize', [],  ... %for expanding window 10% for sliding window 50%
                'detrending', 'gaussian',  ... %  = c("no", "gaussian", "linear", "poly", "first-diff", "movmean",
                'bandwidth', 10,  ...
                'removetails', true,  ... %remove tails for detrending
                'time', [],  ...
                'datacolumn', {{0}},  ... %per indicator or one for all 0 means default
                'corrcolumns', {{[]}},  .... %empty is all (except datacolumn)
                'silent', false,  ...
                'expanding_window', false,  ...
                'logtransform', false,  ...
                'indicators', {{'AR', 'acf', 'std', 'skewness'}},  ... %other options cv, abscorr, crosscorr, mean, DFA
                'figures', {{'original data', 'residuals'; 'indicator 1', 'indicator 2'; 'indicator 3', 'indicator 4'; 'indicator 5', 'indicator 6'}},  ...
                'cv', false,  ...
                'polydegree', 4,  ...
                'interpolate', false,  ...
                'AR_n', false,  ...
                'powerspectrum', false,  ...
                'bandwidth_unit', '%',  ...
                'winsize_unit', '%',  ...
                'ebisuzaki', 0,  ...  %number of nullmodel (Ebisuzaki) runs (0=none)
                'violinplot', true,  ...
                'nanflag', 'omitnan',  ...
                'title', '', pars{:});
        end
 
        function plot_results(res)
     
            EWSopt = res.EWSopt;
            if ~isempty(res.nullmodel.taus)
                if ~iscell(res.nullmodel.taus)
                    %this is only the case if you have more than one outputs for
                    %some indicators. (datacolumn = [] and indicators that do not
                    %have dimension reduction)

                    fprintf('Surrogate data analysis  %s\n', EWSopt.title);
                    fprintf('Number of surrogate data sets: %d\n', size(res.nullmodel.taus, 2));
                    for i = 1:length(res.pvalues)
                        fprintf('Tau %s = %g, pvalue = %g\n', EWSopt.indicators{i}, res.taus(i), res.pvalues(i));
                    end
                    if ~EWSopt.silent && (~isfield(EWSopt, 'violinplot') || EWSopt.violinplot)
                        oldfig = get(0, 'currentfigure');
                        figure;
                        try
                            violinplot(res.nullmodel.taus', (1:length(res.pvalues)), 'bandwidth', 0.1, 'categorynames', EWSopt.indicators);
                        catch
                        end
                        %i_plotdefaults;
                        hold on
                        plot((1:length(res.pvalues)), res.taus, 'ro');
                        ylabel('Kendall \tau')
                        title('Surrogate data')
                        if ~isempty(oldfig)
                            figure(oldfig);
                        end
                    end
                end
            end
            if ~EWSopt.silent && ~isempty(EWSopt.figures)
                xlims = [min(EWSopt.time), max(EWSopt.time)];
                if abs(xlims(2) - xlims(1)) < 1E-20
                    xlims(2) = xlims(1) + 1;
                end
                hasstats = exist('corr', 'file') ~= 0;
                figure;
                figs = EWSopt.figures'; %strange of MATLAB that this is different
                for i = 1:numel(EWSopt.figures)
                    h = subplot(size(EWSopt.figures, 1), size(EWSopt.figures, 2), i);
                    if strncmp(figs{i}, 'original data', 13)
                        col = str2double(regexp(figs{i}, '[0-9]*', 'match', 'once'));
                        if isnan(col)
                            col = ewsdata_windowed.getcolumn(EWSopt.datacolumn, 1);
                            if iscell(col) && ~isempty(col)
                                col = col{1};
                            end
                        end
                        if ~isempty(col) && col > size(res.timeseries, 2)
                            error('generic_ews_new:size', 'Cannot draw figure "%s", too few columns', figs{i});
                        end
                        if isempty(col)
                            col = true(1, size(res.timeseries, 2));
                        end
                        plot(EWSopt.time, res.timeseries(:, col), 'k-', 'Tag', figs{i});
                        xlim(xlims)
                        %adjustticklabels(gca, 'Y');
                        if ~isempty(res.trend)
                            hold on;
                            if isempty(col)
                                plot(EWSopt.time, res.trend, 'r-', 'Tag', 'trend');
                            else
                                plot(EWSopt.time, res.trend(:, col), 'r-', 'Tag', 'trend');
                            end
                            hold off;
                        end
                        if ~isempty(EWSopt.title)
                            text(0.1, 0.9, EWSopt.title, 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'toptext')
                            set(gcf, 'name', EWSopt.title)
                        end
                        xlabel('Time');
                        % ylabel('variable and trend');
                    elseif strncmp(figs{i}, 'residuals', 9)
                        col = str2double(regexp(figs{i}, '[0-9]*', 'match', 'once'));
                        if isnan(col)
                            col = ewsdata_windowed.getcolumn(EWSopt.datacolumn, 1);
                            if iscell(col) && ~isempty(col)
                                col = col{1};
                            end
                        end
                        if isempty(col)
                            col = true(1, size(res.timeseries, 2));
                        end
                        if ~isempty(res.trend)
                            h = stem(EWSopt.time, res.timeseries(:, col) - res.trend(:, col), 'k.', 'Tag', figs{i});
                            %adjustticklabels(gca, 'Y');
                            text(0.1, 0.9, 'residuals', 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'toptext')
                            set(h, 'markersize', 1);
                        else
                            plot(EWSopt.time, res.timeseries(:, col), 'k-', 'Tag', figs{i});
                            text(0.1, 0.9, 'No residuals - no detrending', 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'text1')
                        end
                        xlabel('Time');
                        xlim(xlims);
                    else
                        if ~isfield(res, 'indicator_funs')
                            res.indicator_funs = indicator_fun.cell2ind(EWSopt.indicators, EWSopt);
                        end
                        uinds = ewsdata.makeunique(res.indicator_funs);
                        iind = find(strcmp(figs{i}, uinds));
                        if isempty(iind)
                            iind = str2double(regexp(figs{i}, '[0-9]*', 'match', 'once'));
                        end
                        if iind <= length(EWSopt.indicators)
                            sindicator = uinds{iind};
                            if any(strcmpi(EWSopt.indicators{iind}, {'AR', 'acf'}))
                                sindicator = sprintf('%s(%d)', lower(sindicator), EWSopt.ar_lag);
                            elseif strcmp(EWSopt.indicators{iind}, 'std')
                                sindicator = 'standard deviation';
                            end
                            if isfield(EWSopt, 'expanding_window') && EWSopt.expanding_window && isfield(res.nullmodel, 'results') && ~isempty(res.nullmodel.results)
                                quants = quantile(res.nullmodel.results{iind}, [0.05 0.95], 2);
                                ndx = ~any(isnan(quants), 2);
                                fill([EWSopt.time(ndx); flipud(EWSopt.time(ndx))], [quants(ndx, 1); flipud(quants(ndx, 2))], [0.8, 0.8, 1], 'EdgeColor', 'none', 'tag', uinds{iind});
                                ylim([-2 inf])
                                hold on
                                %plot(EWSopt.time, quants(:,2), 'r--', 'tag', uinds{iind});
                            end
                            if iscell(res.indicators)
                                plot(EWSopt.time, res.indicators{iind}, 'k-', 'tag', uinds{iind});
                            else
                                plot(EWSopt.time, res.indicators(:, iind), 'k-', 'tag', uinds{iind});
                            end
                            hold off;
                            %adjustticklabels(gca, 'Y');
                            text(0.1, 0.9, sindicator, 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'toptext')
                            if hasstats && (~iscell(res.taus) || size(res.taus{iind}, 2) == 1)
                                %if we have more than one tau value per figure none
                                %is shown, also none of the pvalues (they are in
                                %the result structure).
                                if iscell(res.taus)
                                    taus1 = res.taus{iind};
                                elseif ~isempty(res.taus)
                                    taus1 = res.taus(iind);
                                else
                                    taus1 = nan;
                                end
                                if isempty(res.pvalues)
                                    pval1 = [];
                                elseif iscell(res.pvalues)
                                    pval1 = res.pvalues{iind};
                                else
                                    pval1 = res.pvalues(iind);
                                end
                                if ~isempty(pval1)
                                    text(0.1, 0.1, sprintf('{\\tau} = %5.3g (p =%5g)', taus1, pval1), 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'bottomtext');
                                elseif ~isnan(taus1)
                                    text(0.1, 0.1, sprintf('{\\tau} = %5.3g', taus1), 'units', 'normalized', 'fontsize', 11, 'fontname', 'Arial', 'tag', 'bottomtext');
                                end
                            end
                            xlabel('Time');
                            xlim(xlims);
                        else
                            delete(h);
                        end
                    end
                    movegui(gcf, 'center');
                end
                ch = findobj(gcf, 'type', 'axes');
                set(ch, 'TickDir', 'out');
                if numel(figs) > 1
                    adjustpos([0, 0.07]);
                    removeaxis('x');
                end
                xlim(xlims);
            end
        end
     
        function res = getcolumn(col, i, multcell)
            if nargin < 3
                multcell = false;
            end
            if multcell && ~iscell(col)
                col = {col};
            end
            if isempty(col)
                res = col;
            elseif iscell(col)
                if numel(col) == 1
                    res = col{1};
                else
                    res = col{i};
                end
            else
                if numel(col) == 1
                    res = col(1);
                else
                    res = col(i);
                end
            end
        end
    end
    
    methods
        function obj = ewsdata_windowed(arawdata, varargin)
            if nargin > 1 && isstruct(varargin{1})
                %the second argument can be the defaults (or all
                %settings)
                defaultopts = varargin{1};
                varargin = varargin(2:end);
            else
                defaultopts = ewsdata_windowed.defaults;
            end
            if ischar(arawdata) && strncmp(arawdata, '-', 1)
                error('ewsdata_windowed:unknownopt', 'unknown option, valid options are: ''-defaultopts'',''-f''');
            end
            obj = obj@ewsdata(arawdata, defaultopts, varargin{:});
        end

        
        function res = set(obj, varargin)
            res = set@ewsdata(obj, varargin{:});
            %validation of some options
            if ~any(strcmp('figures', res.changed_pars))
                if ischar(obj.options.figures)
                    obj.options.figures = {obj.options.figures};
                end
                if iscell(obj.options.figures)
                    for i = 1:length(obj.options.figures)
                        if ~(strncmp(obj.options.figures{i}, 'original data', 13) || strncmp(obj.options.figures{i}, 'residuals', 9) || ~isnan(str2double(regexp(obj.options.figures{i}, '[0-9]*', 'match', 'once'))))
                            error('ewsdata_windowed:set', 'Some ''figures'' not recognized, valid options: ''original data [No]'' ,''residuals [No]'', ''indicator No''\nWhere [No] is an optional column number and No is required number)');
                        end
                    end
                else
                    error('ewsdata_windowed:set', '''figures'' should be of type cell');
                end
            end
            %Logicals:
            logicals = {'silent', 'expanding_window', 'interpolate', 'violinplot'};
            for i = 1:length(logicals)
                obj.options.(logicals{i}) = obj.validate('logical', logicals{i}, obj.options.(logicals{i}));
            end
        end
 
        % function result = running_window_with_ebi(obj)
        %    result=obj.running_window;           
        %   result = obj.add_ebisuzaki(result);
        %  end
        

 
        function result = windowed_sensitivity(obj, opts, winsizes, bandwidths)
            taus = cell(size(winsizes));
            indicats = indicator_fun.cell2ind(opts.indicators, opts);
            if ~obj.data_updated
                obj.update_data;
            end
            if ~obj.updated
                obj.update_trend;
            end
            %             vectorized = size(obj.rawdata, 2) == 1;
            %             if vectorized
            %                 for i = 1:length(indicats)
            %                     if ~indicats{i}.props.vectorized
            %                         vectorized = false;
            %                         break;
            %                     end
            %                 end
            %             end
            %             if false && vectorized && size(winsizes, 2) > 1 && min(g_sens.winsizes, [], 2) == max(g_sens.winsizes, [], 2)
            %                 %here we can speed it up
            %                 
            %                 %this can be much faster as the ebisuzaki runs are
            %                 %vectorized
            %                 for i = 1:length(indicats)
            %                     indicats{i} = indicats{i}.set('datacolumn', []);
            %                 end
            %                 opts.datacolumn = [];
            %                 nulldata = ebisuzaki(obj.rawdata, nebisuzaki);
            %                 detrended_null = do_detrend(tim, nulldata, opts.detrending,  ...
            %                     opts.bandwidth, opts.bandwidth_unit, opts.polydegree);
            %                 [~, ebitaus] = do_running_window(indicats, tim, nulldata, detrended_null, opts);
            %                 ebitaus = vertcat(ebitaus{:});
            %             else
            %this is just running sequentially
            opts1 = opts;
            %you can use parfor here
            for i = 1:numel(bandwidths)
                detrended_ = eswdata.do_detrend(obj.times, obj.rawdata, opts1.detrending,  ...
                    bandwidths(i), opts1.bandwidth_unit, opts1.polydegree, opts1.removetails);
                opts1.winsize = winsizes(i);
                [~, taus{i}] = do_running_window(indicats, obj.times, obj.rawdata, detrended_, opts1);
            end
            %   end
            siz = size(taus);
            taus = vertcat(taus{:})';
            taus = permute(reshape(taus, numel(indicats), siz(1), siz(2)), [2 3 1]);
 
            if iscell(taus)
                taus = {taus};
            end
            result = struct('timeseries', obj.rawdata, 'EWSopt', opts, 'trend', obj.rawdata - obj.detrended,  ...
                'colnames', {opts.indicators}, 'indicators', [], 'taus', taus, 'pvalues', [], 'nullmodel', struct('taus', []), 'description',  ...
                'gerenerated with generic_ews_sens', 'winsizes', winsizes, 'bandwidths', bandwidths);
        end

        function result = add_ebisuzaki(obj, result, nebisuzaki)
            if nargin < 3
                if isfield(obj.options, 'ebisuzaki')
                    nebisuzaki = obj.options.ebisuzaki;
                else
                    nebisuzaki = 100;
                end
            end
            if isfield(result, 'bandwidths') && isfield(result, 'winsizes')
                %add pvalues to sensitivity results
                res1 = result;
                res1 = rmfield(res1, {'bandwidths', 'winsizes'});
                ebitaus = cell(size(result.winsizes));
                pvalues = ebitaus;
                for i = 1:size(result.winsizes, 1)
                    for j = 1:size(result.winsizes, 2)
                        res1.EWSopt.winsize = result.winsizes(i, j);
                        res1.EWSopt.bandwidth = result.bandwidth(i, j);
                        res2 = obj.add_ebisuzaki(res1, nebisuzaki);
                        ebitaus{i, j} = res2.nullmodel.taus;
                        pvalues{i, j} = res2.pvalues;
                    end
                end
                return;
            end
            if isempty(result.nullmodel.taus) && ~isempty(nebisuzaki) && nebisuzaki > 1
                if ~isfield(result, 'indicator_funs')
                    indicats = indicator_fun.cell2ind(result.EWSopt.indicators, result.EWSopt);
                else
                    indicats = result.indicator_funs;
                end
                vectorized = size(obj.rawdata, 2) == 1;
                if vectorized
                    for i = 1:length(indicats)
                        if ~indicats{i}.props.vectorized
                            vectorized = false;
                            break;
                        end
                    end
                end
                tim = obj.times;
                opts = result.EWSopt;
                opts.seed = rng;
                opts.ebisuzaki = 0;
                if vectorized
                    %this can be much faster as the ebisuzaki runs are
                    %vectorized
                    for i = 1:length(indicats)
                        indicats{i} = indicats{i}.set('datacolumn', []);
                    end
                    opts.datacolumn = [];
                    nulldata = ebisuzaki(obj.rawdata, nebisuzaki);
                    detrended_null = ewsdata.do_detrend(tim, nulldata, opts.detrending,  ...
                        opts.bandwidth, opts.bandwidth_unit, opts.polydegree, opts.nanflag, opts.removetails);
                    [res, ebitaus] = do_running_window(indicats, tim, nulldata, detrended_null, opts);
                    if ~isempty(ebitaus)
                        ebitaus = vertcat(ebitaus{:});
                    end
                else
                    %this is just ran sequentially
                    nulldata = zeros(size(obj.rawdata, 1), size(obj.rawdata, 2), nebisuzaki);
                    for i = 1:size(obj.rawdata, 2)
                        nulldata(:, i, :) = ebisuzaki(obj.rawdata(:, i), nebisuzaki);
                    end
                    ebitaus = cell(nebisuzaki, 1);
                    %you can use parfor here
                    for i = 1:nebisuzaki
                        detrended_null = ewsdata.do_detrend(tim, nulldata(:, :, i), opts.detrending,  ...
                            opts.bandwidth, opts.bandwidth_unit, opts.polydegree, opts.nanflag, opts.removetails);
                        [res, taus] = do_running_window(indicats, tim, nulldata(:, :, i), detrended_null, opts);
                        ebitaus{i} = taus;
                    end
                    if isempty(taus)
                        ebitaus = [];
                    else
                        ebitaus = vertcat(ebitaus{:})';
                    end
                end
                if isempty(ebitaus) %expanding window method
                    result.nullmodel.taus = [];
                    result.nullmodel.results = res;
                    result.nullmodel.seed = opts.seed;
                else
                    %translate ebizusaki results to p values:
                    result.nullmodel.taus = ebitaus;
                    result.nullmodel.results = res;
                    result.nullmodel.seed = opts.seed;
                    result.pvalues = get_pvalues(result.taus, result.nullmodel.taus);
                end
            end
        end
       
   
 
        function result = running_window(obj, opts) %, indicators, winsize, winsize_unit, indicat1)
            %Classic running window method
            %indicators should be a cell array with indicator_fun's (you
            %can make it with inds=indicator_fun.cell2ind({'ar1','sd',..})
            if ~obj.data_updated
                obj.update_data;
            end
            if ~obj.updated
                obj.update_trend;
            end
            if nargin < 2
                opts = obj.options;
            end
            %if nargin > 3 && ~isempty(winsize_unit)
            %    opts.winsize_unit = winsize_unit;
            %end
            %if nargin > 2 && ~isempty(winsize)
            %    opts.winsize = winsize;
            %end
            %if nargin > 1 && ~isempty(indicators)
            %    if ischar(indicators)
            %        opts.indicators = {indicators};
            %    else
            %        opts.indicators = indicators;
            %    end
            %end
            %if nargin < 5
            %    obj.indicat = indicator_fun.cell2ind(opts.indicators, opts);
            %else
            %    obj.indicat = indicat1;
            %end
            if isempty(opts.winsize)
                %the defaults are different for expanding window than for
                %sliding window
                if opts.expanding_window
                    opts.winsize = 10;
                else
                    opts.winsize = 50;
                end
            end
            %             if isequal(opts.datacolumn,{0})
            %                 %default datacolumn is different for multivariate
            %                 %indicators
            %                 dimred=false(size(obj.indicat));
            %                 for i=1:numel(dimred)
            %                     dimred(i)=obj.indicat{i}.props.dim_reduct;
            %                 end
            %                 if any(dimred)
            %                     %default for multivariate
            %                     opts.datacolumn={[]};
            %                 else
            %                     %default for univariate
            %                     opts.datacolumn={1};
            %                 end
            %                 obj.options.datacolumn=opts.datacolumn;
            %                 indicat1=obj.indicat;
            %                 for i=1:numel(obj.indicat)
            %                     indicat1{i}=indicat1{i}.set('datacolumn',obj.options.datacolumn);
            %                 end
            %                 obj.indicat=indicat1;
            %             end


            [res, taus] = do_running_window(obj.indicat, obj.times, obj.rawdata, obj.detrended, opts);
            
            trend = obj.rawdata - obj.rawdetrended;
            colnames = cell(size(obj.indicat));
            for i = 1:length(colnames)
                colnames{i} = obj.indicat{i}.dispname;
            end
            descr = sprintf('data generated on %s with generic_ews\nwinsize = %g, detrending = %s, bandwidth = %g, logtransform = %d, ar_lag = %d, interpolate = %d',  ... 
                datestr(now()), obj.options.winsize, obj.get('detrending', 1), obj.get('bandwidth', 1), obj.get('logtransform', 1), obj.get('ar_lag', 1), obj.get('interpolate', 1));
            opts.time = obj.times;
            colnames = obj.makeunique(colnames);
            if iscell(res)
                %needs to be double cell to get a cell in a struct
                res = {res};
                taus = {taus};
            end
            result = struct('timeseries', obj.rawdata,  ...
                'EWSopt', opts,  ...
                'trend', trend,  ...
                'colnames', {colnames},  ...
                'indicators', res,  ...
                'taus', taus,  ...
                'pvalues', [],  ...
                'nullmodel', struct('taus', []),  ...  
                'indicator_funs', {obj.indicat},  ...
                'description', descr);
            result = obj.add_ebisuzaki(result);
        end
    end
end


function [res, taus] = do_running_window(indicat, times, rawdata, detrended, opts)
    %the basal running window using opts
    % if ~isfield(opts,'expanding_window')
    %     opts.expanding_window=false;
    % end
    %     function s = col2str(c)
    %         if iscell(c{1})
    %             if numel(c)==1
    %               s=col2str(c{1});
    %             elseif numel(c) == 2
    %               s=sprintf('%s, %s\n', col2str(c{1}), col2str(c{2}));
    %             end
    %             return;
    %         end
    %         if isempty(c) || isempty(c{1})
    %             s = ':';
    %         elseif length(c{1}) == 1
    %             s = int2str(c{1});
    %         else
    %             s = mat2str([c{1}]);
    %         end
    %     end
    if isa(times,'datetime')
        times=days(times-times(1));
    end
    if strcmp(opts.winsize_unit, '%')
        if opts.winsize > 100
            opts.winsize = 100;
        end
        if opts.winsize < 0.1
            opts.winsize = 0.1;
        end
        abswinsize = opts.winsize / 100 * (max(times) - min(times));
    else
        abswinsize = opts.winsize;
    end
    res = cell(1, numel(indicat));
    for i1 = 1:length(indicat)
        res{i1} = [];
    end
    indices = unique_indicat(indicat);

    tstart = times - abswinsize;
    if opts.expanding_window
        %exclude the nan points of the trend
        no_nantrend = ~isnan(detrended) | isnan(rawdata);
    end
    f = find(tstart >= times(1), 1);
    if isempty(f)
        error('not enough data')
    end
    tstart = tstart - (tstart(f) - times(1)); %allign with time lags
    tstart(tstart < times(1)) = NaN;
    for i1 = 1:length(times)
        if ~isnan(tstart(i1))
            if opts.expanding_window
                ndx = times < times(i1) & all(no_nantrend, 2);
            else
                ndx = times >= tstart(i1) & times < times(i1);
            end
            res2 = indicator_fun.calcgroup(indicat, indices, rawdata, detrended, ndx);
            for j = 1:length(indicat)
                %   res1 = indicat{j}.calc(rawdata, detrended, ndx);
                res1 = res2{j};
                if isempty(res{j})
                    res{j} = nan(size(rawdata, 1), numel(res1));
                end
                res{j}(i1, :) = res1(:);
            end
        end
    end
    colnames = cell(size(indicat));
    maxwidth = 1;
    for i = 1:length(colnames)
        colnames{i} = indicat{i}.dispname;
        if size(res{i}, 2) > maxwidth
            maxwidth = size(res{i}, 2);
        end
    end
    if maxwidth == 1
        res = [res{:}];
    end
    if opts.expanding_window
        %normalize the data using w(t)=(w(t)-mean(1:t))/sd(1:t))
        if iscell(res)
            for j = 1:length(indicat)
                res1 = res{j};
                fstart = find(~all(isnan(res1), 2), 1);
                res{j} = nan(size(res{j}));
                for i = fstart + 1:size(res1, 1)
                    res{j}(i, :) = (res1(i, :) - mean(res1(fstart:i, :), 1)) ./ std(res1(fstart:i, :), 1);
                end
            end
            taus = [];
            %             taus = cell(size(res));
            %             for i = 1:length(taus)
            %                 taus{i} = quantile(res{j}, 0.95, 2)';
            %             end
        else
            fstart = find(~all(isnan(res), 2), 1);
            res1 = res;
            res = nan(size(res1));
            for i = fstart + 1:size(res, 1)
                res(i, :) = (res1(i, :) - mean(res1(fstart:i, :), 1)) ./ std(res1(fstart:i, :), 1);
            end
            taus = [];
        end
    else
  
        hasstats = exist('corr', 'file') ~= 0;
        if hasstats
            %we cannot use p values as the data are not independent
            %use add_ebisuzaki
            if iscell(res)
                taus = cell(size(res));
                for i = 1:length(taus)
                    taus{i} = corr(times, res{i}, 'type', 'Kendall', 'rows', 'complete');
                end
            else
                %taus = zeros(size(res));
                %for i = 1:length(taus)
                taus = corr(times, res, 'type', 'Kendall', 'rows', 'pairwise');
                %end
            end
        else
            taus = [];
        end
    end
end

function pvalues = get_pvalues(taus, ebitaus)
    %translage ebisuzaki results to p-values
    %can handle matrices for taus, and 
    oldsiz = size(ebitaus);
    if length(oldsiz) == 3
        %if taus is 2D reshape input
        taus = reshape(taus, [oldsiz(1) * oldsiz(2) 1]);
        ebitaus = reshape(ebitaus, [oldsiz(1) * oldsiz(2) oldsiz(3)]);
    else
        oldsiz = [];
    end
    nebisuzaki = size(ebitaus, 2);
    if iscell(ebitaus)
        %this is only the case if you have more than one outputs for
        %some indicators. (datacolumn = [] and indicators that do not
        %have dimension reduction)
        pvalues = cell(1, numel(taus));
        for j = 1:numel(pvalues)
            ebitaus_j = vertcat(ebitaus{j, :});
            ps = zeros(1, size(ebitaus_j, 2));
            for m = 1:length(ps)
                ps(m) = (sum(taus{j}(m) < ebitaus_j(m, :)) + 1) / nebisuzaki;
            end
            ps(isnan(taus{j})) = NaN;
            pvalues{j} = ps;
        end
    else
        pvalues = zeros(size(taus));
        % nebisuzaki = size(res.ebitaus, 2);
        for j = 1:numel(pvalues)
            pvalues(j) = (sum(taus(j) < ebitaus(j, :)) + 1) / nebisuzaki;
        end
        pvalues(isnan(taus)) = NaN;
    end
    if ~isempty(oldsiz)
        %reshape back if necessary
        pvalues = reshape(pvalues, [oldsiz(1), oldsiz(2)]);
    end

end

function adjustpos(spacing)
    set(gcf, 'PaperPositionMode', 'auto')
    set(gcf, 'units', 'normalized');
    figpos = get(gcf, 'position');
    vert = 0.5469; %normal size of figure
    hor = 0.41;
    hs = getsubplotgrid(gcf);
    if nargin == 0
        spacing = 0.02;
    end
    if length(spacing) == 1
        spacing = spacing + zeros(2, 1);
    end
    [rows, cols] = size(hs);
    if cols < 4 && rows <= 4
        set(gcf, 'position', [figpos(1:2) hor * cols / 4 vert * rows / 4]);
    else
        rat = rows / cols;
        if rat < 1
            set(gcf, 'position', [figpos(1:2) hor vert * rat]);
        else
            set(gcf, 'position', [figpos(1:2) hor / rat vert]);
        end
    end
    for i = 1:rows
        for j = 1:cols
            h = findobj(gcf, 'tag', sprintf('subplot(%d,%d)', i, j));
            if any(ishandle(h)) && any(h ~= 0)
                set(h, 'position', subplotpos(rows, cols, j, i, spacing));
            end
        end
    end
end

function hs = getsubplotgrid(h)
    ch = get(h, 'children');
    tags = get(ch, 'tag');
    ch = ch(~strcmp(tags, 'legend'));
    types = get(ch, 'type');
    poss = get(ch, 'position');
    if length(ch) == 1
        poss = {poss};
        types = {types};
    end
    ipos = zeros(length(ch), 4);
    for i = length(ch): -1:1
        if iscell(types) && strcmp(types{i}, 'axes')
            ipos(i, :) = poss{i};
        elseif ischar(types) && strcmp(types, 'axes')
            ipos(i, :) = poss{i};
        else
            ipos(i, :) = [];
        end
    end
    colpos = sort(unique(sort(ipos(:, 1))), 'ascend');
    rowpos = sort(unique(sort(ipos(:, 2))), 'descend');
    hs = zeros(length(rowpos), length(colpos));
    for i = 1:length(ch)
        if strcmp(types{i}, 'axes')
            arow = find(rowpos == ipos(i, 2));
            acol = find(colpos == ipos(i, 1));
            hs(arow, acol) = ch(i);
            set(ch(i), 'tag', sprintf('subplot(%d,%d)', arow, acol));
        end
    end
end

function pos = subplotpos(rows, cols, x1, y1, spacing, colwidth, height_sgtitle)
    %tight position for subplots, you can only set the spacing between the
    %figures (simplified from subaxis)
    %   s=subplot('position',subplotpos(rows,cols,x1,y1),'tag',sprintf('subplot(%d,%d)',x1,y1));
    %
    if nargin < 5
        spacing = [0.02 0.02];
    end
    if nargin < 6
        colwidth = 1;
    end
    if nargin < 7
        height_sgtitle = 0;
    end
    if length(spacing) == 1
        spacing = spacing + zeros(2, 1);
    end

    Args = struct('Holdaxis', 0,  ...
        'SpacingVertical', spacing(1), 'SpacingHorizontal', spacing(2),  ...
        'MarginLeft', .2, 'MarginRight', .1, 'MarginTop', 0.05 + height_sgtitle, 'MarginBottom', .1,  ...
        'rows', rows, 'cols', cols);


    cellwidth = ((1 - Args.MarginLeft - Args.MarginRight) - (Args.cols - 1) * Args.SpacingHorizontal) / Args.cols;
    if colwidth > 1
        cellwidth = cellwidth * colwidth + (colwidth - 1) * Args.SpacingHorizontal;
    end
    cellheight = ((1 - Args.MarginTop - Args.MarginBottom) - (Args.rows - 1) * Args.SpacingVertical) / Args.rows;
    xpos1 = Args.MarginLeft + cellwidth * (x1 - 1) + Args.SpacingHorizontal * (x1 - 1);
    xpos2 = Args.MarginLeft + cellwidth * x1 + Args.SpacingHorizontal * (x1 - 1);
    ypos1 = Args.MarginTop + cellheight * (y1 - 1) + Args.SpacingVertical * (y1 - 1);
    ypos2 = Args.MarginTop + cellheight * y1 + Args.SpacingVertical * (y1 - 1);

    pos = [xpos1 1 - ypos2 xpos2 - xpos1 ypos2 - ypos1];
end


function removeaxis(orientation)
    hs = getsubplotgrid(gcf);
    if nargin == 0
        orientation = 'b';
    end
    [rows, cols] = size(hs);
    if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'x', 1)
        for i = 1:rows - 1
            for j = 1:cols
                if ishandle(hs(i, j)) && (hs(i, j) ~= 0)
                    set(get(hs(i, j), 'xlabel'), 'string', '')
                    set(hs(i, j), 'XTickLabel', []);
                end
            end
        end
        for i = 2:rows
            for j = 1:cols
                if ishandle(hs(i, j)) && (hs(i, j) ~= 0)
                    set(get(hs(i, j), 'title'), 'string', '')
                end
            end
        end
    end
    if strncmpi(orientation, 'b', 1) || strncmpi(orientation, 'y', 1)
        for i = 1:rows
            for j = 2:cols
                if ishandle(hs(i, j)) && (hs(i, j) ~= 0)
                    set(get(hs(i, j), 'ylabel'), 'string', '')
                    set(hs(i, j), 'YTickLabel', []);
                end
            end
        end
    end
    set(hs(hs ~= 0), 'fontsize', 12)
    aa = get(get(gcf, 'children'), 'xlabel');
    if ~iscell(aa)
        aa = {aa};
    end
    for i = 1:length(aa)
        set(aa{i}, 'fontsize', 12);
    end
    aa = get(get(gcf, 'children'), 'ylabel');
    if ~iscell(aa)
        aa = {aa};
    end
    for i = 1:length(aa)
        set(aa{i}, 'fontsize', 12);
    end
end
