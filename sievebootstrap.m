function [bootsets, res] = sievebootstrap(data, nboot, nanflag)
    %sievebootstrap generate bootstrapped data sets.
    %using the sieve bootstrap (Bühlmann, 1997). First an AR(n) model is fitted
    %and the residuals are determined. By sampling from the scaled residuals we
    %generate nboot new data series using the fitted AR model.
    %We start with the data and first stabilize 1000 time steps before generating
    %the bootstrapped sets.
    %
    %Usage
    %sets=sievebootstrap(data) the bootstrapped data sets are generated,
    %    data should be a vector, the default nboot(=1000)
    %    res is a matrix with the same number of rows as the data. The
    %    columns are the different bootstrapped sets.
    %[sets,res]=sievebootstrap(data,nboot) with specified number of boot samples
    %    the structure res contains all information to recreate the bootstrapped data
    %    sets but saves a lot of disc space.
    %sets=sievebootstrap(res) - res is the saved structure. sets is recreated with the same random seed 
    %as before.
    %
    %reference: Bühlmann, P. (1997). Sieve bootstrap for time series. Bernoulli, 123-148.

    %res saves the random seed and used model and data so that it can remake exactly the
    %same bootstrapped data sets 

    if nargin < 2
        nboot = 1000;
    end
    if nargin < 3
        nanflag = 'omitnan';
    end
    oldmatlab = verLessThan('matlab', '9.2');
    %sieve bootstrap
    %(1) determine a full AR model (should test here for higher orders)
    %we take the model with the lowest AIC between lag order 1 and 10
    if ~isstruct(data)
        if ~isnumeric(data)
            try
                data = double(data);
            catch
                data = table2array(data);
            end
        end
        if ~exist('fitlm', 'file')
            fitlm = @(varargin)LinearModel.fit(varargin{:});
        else
            fitlm = eval('@fitlm');
        end
        res.nboot = nboot;
        res.data = data;
        meandata = mymean(data, nanflag, 1, oldmatlab);
        data = bsxfun(@minus, data, meandata); %for older matlab versions
        %        data = data - meandata;
        siz3 = size(data, 2);
        if siz3 > 1
            res.ARcoefs = cell(siz3, 1);
        end
        for i = 1:siz3
            tbl = lagtable(data(:, i), 10);
 
            ndx = true(size(tbl, 2), 1);
            aic = zeros(size(tbl, 2) - 1, 1);
            for order = length(aic): -1:1
                m = fitlm(tbl(:, ndx), 'intercept', false);
                aic(order) = m.ModelCriterion.AIC;
                ndx(order) = false;
            end
            order = find(min(aic) == aic);
            ndx = true(size(tbl, 2), 1);
            ndx(order + 1:end - 1) = false;

            model = fitlm(tbl(:, ndx), 'intercept', false);
            %get the AR coefficients, fliplr because we need the highest order first
            if siz3 > 1
                res.ARcoefs{i} = fliplr(model.Coefficients.Estimate');
            else
                res.ARcoefs = fliplr(model.Coefficients.Estimate');
            end
        end
        res.seed = rng;
    else
        if isfield(data, 'bootstrap')
            res = data.bootstrap;
        else
            res = data;
        end
        nboot = res.nboot;
        rng(res.seed);
        meandata = mymean(res.data, nanflag, 1, oldmatlab);
    end
 
    siz3 = size(res.data, 2);
 
    data = bsxfun(@minus, res.data, meandata); %for older matlab versions
    %res.data - meandata;
    ndata = size(res.data, 1);
    if siz3 > 1
        bootsets = zeros(ndata, siz3, nboot);
    else
        bootsets = zeros(ndata, nboot);
    end
    %(2)determine the residuals
    for i = 1:siz3
        if siz3 > 1
            order = numel(res.ARcoefs{i});
            ARcoefs = res.ARcoefs{i};
        else
            order = numel(res.ARcoefs);
            ARcoefs = res.ARcoefs;
        end
        residuals = nan(size(data, 1), 1);
        for j = order + 1:ndata
            %matrix multiplication with the ARcoefficients - data
            residuals(j) = ARcoefs * data(j - order:j - 1, i) - data(j, i);
        end
        %residuals = res.model.Residuals.Raw;
        %remove nan's at the end
        residuals = residuals(~isnan(residuals));
        if isempty(residuals)
            residuals = NaN;
        end

        %residuals=data(2:end)-rc*data(1:end-1);
        %(3)scale the errors to have a mean of zero
        residuals = mean(residuals) - residuals'; %scale the errors
        n = numel(residuals);
        %(4)start with the first data point in the time series
        %run AR1 model for the size of the original data +1000 steps for stabilizing
        %(vectorized 1000 replicates) by sampling from the residuals with replacement
        abootsets = zeros(ndata + 1000, nboot);
        abootsets(1:order, :) = zeros(order, nboot) + data(1, i);
        for j = order + 1:ndata + 1000
            %stabilize + run the AR1 model
            %matrix multiplication with the ARcoefficients, + draw independent residuals
            abootsets(j, :) = ARcoefs * abootsets(j - order:j - 1, :) + residuals(randi(n, 1, nboot));
        end
        %(5)remove stabilizing part from the result
        if siz3 > 1
            bootsets(:, i, :) = abootsets(1001:end, :) + meandata(i);
        else
            bootsets = abootsets(1001:end, :) + meandata(i);
        end
    end
end
function meandata = mymean(data, nanflag, dim, oldmatlab)
    if ~oldmatlab
        meandata = mean(data, dim, nanflag);
    else
        if strcmp(nanflag, 'omitnan')
            meandata = nanmean(data, dim);
        else
            meandata = mean(data, dim);
        end
    end
end
function tbl = lagtable(data, maxlags)
    res1 = cell(maxlags, 1);
    for lag = 1:maxlags
        res1{lag} = nan(size(data));
        res1{lag}(1 + lag:end) = data(1:end - lag);
    end
    try
        tbl = table(res1{:}, data);
    catch err
        if strcmp(err.identifier, 'MATLAB:UndefinedFunction')
            tbl = dataset(res1{:}, data); %#ok<DTSET>
        end
    end
end
