classdef ewsdata < handle
    %this is a handle object as there may be overhead in changing the
    %trends
    properties (Access = protected)
        rawdetrended = []; %do not use this property directly (update can be delayed)
        updated = false;
        data_updated = false;
        indicat = {};
        multivariate = [];
    end
    properties
        rawdata = [];
        groups = []; %grouping variable, categorical or other (see stats/grpstats)
        times = [];
        options = {};
    end
    properties (Dependent)
        indicator_props
    end
    
    methods (Static)
 
        function indic = makeunique(indic)
            if ~isempty(indic) && ~ischar(indic{1})
                indicfun = indic;
                for i = 1:numel(indicfun)
                    indic{i} = strrep(indicfun{i}.dispname, '_', ' ');
                end
            end
            uindic = unique(indic);
            if length(uindic) < length(indic)
                for i = 1:length(uindic)
                    f = find(strcmp(uindic{i}, indic));
                    for j = 2:length(f)
                        indic{f(j)} = sprintf('%s_%d', indic{f(j)}, j - 1);
                    end
                end
            end
        end

        function detrended = do_detrend(times, data, detrending, bandwidth, bandwidth_unit, polydegree, nanflag, removetails)
            function par = getpar(par, i)
                if numel(par) > 1 && iscell(par)
                    par = par{i};
                elseif iscell(par)
                    par = par{1};
                end
            end
            if matlabLessThan([8 5])
                nanflag='';
            end
            if numel(detrending) == 1 && strncmpi(detrending{1}, 'no', 2)
                detrended = [];
                return;
            end
            detrended = data;
            if any(times(2:end)<times(1:end-1))
                error('ewsdata:timemustincrease','Time must be monotonically increasing, now the next value is %d time(s) smaller than the previous'...
                ,sum((times(2:end)<times(1:end-1))))
            end
            for i = 1:size(data, 2)
                trendname = getpar(detrending, i);
                if strcmpi(trendname, 'first-diff')
                    detrended(:, i) = [nan; diff(data(:, i))];
                elseif strcmpi(trendname, 'linear')
                    if isa(times,'datetime')
                        times=days(times-times(1));
                    end
                    if strcmp(nanflag, 'omitnan')
                        ndx = ~isnan(data(:, i));
                    else
                        ndx = true(size(times));
                    end
                    p = polyfit(times(ndx), data(ndx, i), 1);
                    detrended(ndx, i) = data(ndx, i) - polyval(p, times(ndx));
                elseif strncmpi(trendname, 'poly', 4)
                    if isa(times,'datetime')
                        times=days(times-times(1));
                    end
                    if strcmp(nanflag, 'omitnan')
                        ndx = ~isnan(data(:, i));
                    else
                        ndx = true(size(times));
                    end
                    p = polyfit(times(ndx), data(ndx, i), getpar(polydegree, i));
                    detrended(ndx, i) = data(ndx, i) - polyval(p, times(ndx));
                elseif strcmpi(trendname, 'movmean')
                    if isa(times,'datetime')
                        times=days(times-times(1));
                    end
                    bw = getpar(bandwidth, i);
                    if strcmp(getpar(bandwidth_unit, i), '%')
                        if ~isempty(bw)
                            absbandwidth = bw ./ 100 .* (max(times) - min(times));
                        else
                            absbandwidth = [];
                        end
                    else
                        absbandwidth = bw;
                    end
                    if isempty(absbandwidth)
                        absbandwidth = [4 0];
                    else
                        %convert from units of time to index
                        absbandwidth = round(absbandwidth * numel(times) / (max(times) - min(times)));
                        if numel(absbandwidth) == 1
                            absbandwidth = [absbandwidth 0];
                        end
                    end
                    try
                        detrended(:, i) = data(:, i) - movmean(data(:, i), absbandwidth, nanflag);
                    catch
                        detrended(:, i) = data(:, i) - movmean1(data(:, i), absbandwidth, nanflag);
                    end
                elseif any(strcmpi(trendname, {'oldgaussian','gaussian'}))
                    %                     if strcmp(nanflag, 'omitnan')
                    %                         ndx = ~isnan(data(:,i));
                    %                     else
                    %                         ndx = true(size(times));
                    %                     end
                    %                     tim=times(ndx);
                    bw = getpar(bandwidth, i);
                    if strcmp(getpar(bandwidth_unit, i), '%')
                        if ~isempty(bw)
                            absbandwidth = bw / 100 * (max(times) - min(times));
                        else
                            absbandwidth = [];
                        end
                    else
                        absbandwidth = bw;
                    end
                    detrended(:, i) = data(:, i) - ksmooth(times, data(:, i), absbandwidth, nanflag, trendname, removetails);
                elseif ~strncmpi(trendname, 'no', 2)
                    error('ewsdata:detrending', 'Kind of detrending unknown "%s": \nValid options are: no, first-diff, linear, poly, movmean, gaussian', trendname);
                end
 
            end
        end

    end
    methods
        function props = get.indicator_props(obj)
            indics = obj.indicat;
            if isempty(indics)
                indics = indicator_fun.cell2ind(obj.get('indicators'), obj.options);
            end
            if isempty(indics)
                props = [];
                return;
            end
            props = indics{1}.props;
            for i = 2:numel(indics)
                props = [props; indics{i}.props]; %#ok<AGROW>
            end
        end
 
        function results = sensitivity(obj, func, sens_pars, sens_values)
            if ~obj.data_updated
                obj.update_data;
            end
            if ~obj.updated
                obj.update_trend;
            end
            opts1 = obj.options;
            mesh = false;
            if ~iscell(sens_values)
                sens_values1 = num2cell(sens_values);
            elseif numel(sens_values) == 2 && size(sens_values{1}, 2) > 1
                mesh = true;
                x = sens_values{1};
                y = sens_values{2};
                sens_values1 = num2cell([x(:) y(:)]);
            else
                sens_values1 = sens_values;
            end
            res = cell(size(sens_values, 1), 1);
            switch func
                case 'running_window'
                    parfor i = 1:size(sens_values1, 1)
                        c = [sens_pars; sens_values1(i, :)];
                        obj.set(c{:}); %#ok<PFBNS>
                        res{i} = obj.running_window();
                    end
                case 'run_bursts'
                    parfor i = 1:size(sens_values1, 1)
                        c = [sens_pars; sens_values1(i, :)];
                        obj.set(c{:}); %#ok<PFBNS>
                        res{i} = obj.run_bursts();
                    end
                case 'run_fixed'
                    parfor i = 1:size(sens_values1, 1)
                        c = [sens_pars; sens_values1(i, :)];
                        obj.set(c{:}); %#ok<PFBNS>
                        res{i} = obj.run_fixed();
                    end
                otherwise
                    error('unknown function');
            end
            obj.options = opts1;
            for i = length(res): -1:1
                if strcmp(func, 'running_window')
                    r(i) = struct('taus', res{i}.taus, 'pvalues', res{i}.pvalues);
                else
                    r(i) = struct('CL', res{i}.CL);
                end
            end
            if mesh
                if strcmp(func, 'running_window')
                    aa = vertcat(r.taus);
                    for i = size(aa, 2): -1:1
                        taus{i} = reshape(aa(:, i), size(sens_values{1}));
                    end
                    aa = vertcat(r.pvalues);
                    for i = size(aa, 2): -1:1
                        pvalues{i} = reshape(aa(:, i), size(sens_values{1}));
                    end
                    r = struct('taus', {taus}, 'pvalues', {pvalues});
                else
                    reshape(r, size(sens_values{1}));
                end
            elseif strcmp(func, 'running_window')
                aa = vertcat(r.taus);
                for i = size(aa, 2): -1:1
                    taus{i} = aa(:, i);
                end
                aa = vertcat(r.pvalues);
                for i = size(aa, 2): -1:1
                    pvalues{i} = aa(:, i);
                end
                r = struct('taus', {taus}, 'pvalues', {pvalues});
            end
            if iscell(sens_values)
                sens_values = {sens_values};
            end
            results = struct('timeseries', res{1}.timeseries, 'EWSopt', opts1, 'trend', res{1}.trend, 'colnames', {res{1}.colnames},  ...
                'indicator_funs', {res{1}.indicator_funs}, 'description', res{1}.description, 'sens_pars', {sens_pars}, 'sens_values', sens_values, 'sens_results', r);
        end
    
        function obj = ewsdata(arawdata, defaultopts, varargin)
            %Constructor
            if iscell(arawdata) && length(arawdata) == 2
                obj.times = arawdata{1};
                obj.rawdata = arawdata{2};
            elseif ~(isa(arawdata, 'table') || isa(arawdata, 'dataset') || isnumeric(arawdata))
                error('ewsdata:rawdata', 'The data set should be numeric or table/dataset');
            elseif size(arawdata, 1) == 1 && size(arawdata, 2) > 1
                obj.times = (1:size(arawdata, 2)).';
                obj.rawdata = arawdata.';
            else
                obj.times = (1:size(arawdata, 1)).';
                obj.rawdata = arawdata;

            end
            if nargin > 1 && ~isempty(defaultopts) && ~ischar(defaultopts)
                obj.options = defaultopts;
            else
                if nargin > 1 && ischar(defaultopts)
                    varargin = [{defaultopts}, varargin];
                end
                %essential options
                
                obj.options = struct('detrending', 'gaussian',  ... %  = c("no", "gaussian", "linear", "poly", "first-diff", "movmean",
                    'polydegree', 4,  ...
                    'bandwidth', 10,  ...         
                    'bandwidth_unit', '%',  ...
                    'removetails', true,...
                    'logtransform', false,  ...
                    'nanflag', 'omitnan');
                if matlabLessThan([8 5])
                    obj.options.nanflag='';
                end
            end
            if isfield(obj.options,'nanflag')&&~isempty(obj.options.nanflag)
               %test if there is an old matlab version that gives an error
                try
                    sum([1 1],obj.options.nanflag);
                catch err
                    if strcmp(err.identifier,'MATLAB:sum:unknownFlag')
                        obj.options.nanflag='';
                    end
                end
            end
            if nargin > 2
                res = obj.set(varargin{:});
                if res.any_error
                    validopts = [fieldnames(obj.options); 'rawdata'];
                    s = sprintf('%s | ', validopts{:});
                    s1 = sprintf('"%s" ', res.unknown{:});
                    error('ewsdata:properties', 'Some of the settings unknown: %s\nValid options: %s\n', s1, s);
                end
            end
        end

    
    
        function disp(obj)
            fprintf('class %s with properties:\n\n', class(obj))
            fprintf('%15s:    [%dx%d double]\n', 'rawdata', size(obj.rawdata, 1), size(obj.rawdata, 2))
            fprintf('%15s:    [%dx%d double]\n', 'times', size(obj.times, 1), size(obj.times, 2))
            essential_pars = {'detrending', 'bandwidth', 'bandwidth_unit', 'polydegree'};
            for i = 1:length(essential_pars)
                par = obj.get(essential_pars{i});
                if isnumeric(par) && length(par) == 1
                    fprintf('%15s:    %g\n', essential_pars{i}, par)
                end
                if ischar(par)
                    fprintf('%15s:    ''%s''\n', essential_pars{i}, par)
                end
                if iscell(par)
                    if ischar(par{1})
                        s = sprintf('''%s'',', par{:});
                    else
                        s = sprintf('%g,', par{:});
                    end
                    fprintf('%15s:    {%s}\n', essential_pars{i}, s(1:end - 1))
                end
            end
        end

        function hs = plot(obj, hax)
            %fast basic plot function
            if nargin == 1
                hax = gca;
            end
            holdstate = ishold(hax);
            h = {0, 0};
            h{1} = plot(hax, obj.times, obj.rawdata, 'k.', 'tag', 'data');
            hold(hax, 'on');
            h{2} = plot(hax, obj.times, obj.rawdata - obj.detrended, 'r-', 'tag', 'trend');
            if ~holdstate
                hold(hax, 'off');
            end
            if nargout > 0
                hs = h;
            end
        end

        function res = detrended(obj)
            if ~obj.data_updated
                %you cannot change these settings if the data is updated
                obj.update_data;
            end
            if ~obj.updated
                obj.update_trend;
            end
            if isempty(obj.rawdetrended)
                res = obj.rawdata;
            else
                res = obj.rawdetrended;
            end
        end

        function update_data(obj)
            function a = notempty(a)
                if isempty(a)
                    a = false;
                end
            end
            if isempty(obj.multivariate)
                %if there is any multivariate indicator, set multivariate =
                %true and adapt datacolumn if necessary
                props = obj.indicator_props;
                obj.multivariate = any(props.dim_reduct);
                datacol = obj.options.datacolumn;
                if isequal(datacol, {0})
                    %default for multivariate
                    if obj.multivariate
                        obj.options.datacolumn = {[]};
                    else
                        %default for univariate
                        obj.options.datacolumn = {1};
                    end
                    indicat1 = obj.indicat;
                    for k = 1:numel(obj.indicat)
                        indicat1{k} = indicat1{k}.set('datacolumn', obj.options.datacolumn);
                    end
                    obj.indicat = indicat1;
                end
            end
            obj.data_updated = true;
            timeseries = obj.rawdata;
            if isa(timeseries, 'table')
                %the time series may be a table , it can then have a column with
                %time, the name of that column should be t or Time or time
                ndx = (strcmp('t', timeseries.Properties.VariableNames) | strcmpi('time', timeseries.Properties.VariableNames));
                if any(ndx)
                    % the time is added to the options
                    obj.set('time', timeseries{:, ndx});
                    timeseries = timeseries{:, ~ndx};
                else
                    timeseries = timeseries{:, :};
                end
            elseif isa(timeseries, 'dataset')
                %the time series may be a dataset, it can then have a column with
                %time, the name of that column should be t or Time or time
                ndx = ~(strcmp('t', timeseries.Properties.VarNames) | strcmpi('time', timeseries.Properties.VarNames));
                if any(ndx)
                    % the time is added to the options
                    obj.set('time', timeseries.t);
                    timeseries = double(timeseries(:, ndx));
                else
                    timeseries = double(timeseries(:, :));
                end
            else
                timeseries = double(timeseries);
            end

            %if ~exist('i_use', 'file')
            %    addpath([grindpath, filesep, 'sys2']);
            %end
            if isempty(timeseries)
                error('grind:generic_ews:empty', 'Empty time series');
            end
            %if notempty(obj.get('powerspectrum')) || notempty(obj.get('AR_n'))
            %    error('Option not yet supported');
            %end

            if notempty(obj.get('interpolate'))
                ts = transpose(linspace(obj.times(1), obj.times(end), size(timeseries, 1)));
                timeseries = interp1(obj.times, timeseries, ts);
                obj.set('time', ts);
            end
            for i = 1:size(timeseries, 2)
                if notempty(obj.get('logtransform', i))
                    timeseries(:, i) = log(timeseries(:, i) + 1);
                end
            end

            obj.options.datalength = max(obj.times) - min(obj.times);
            if notempty(obj.get('cv'))
                %depreciated
                f = strcmp(obj.get('indicators'), 'std');
                obj.options.indicators(f) = {'cv'};
            end
            obj.rawdata = timeseries;
        end

        function value = validate(obj, akind, afield, avalue, varargin)
            value = avalue;
            switch akind
                case 'logical'
                    if ischar(avalue) && any(strcmp(avalue, {'true', 'on', 'yes'}))
                        value = true;
                    elseif ischar(avalue) && any(strcmp(avalue, {'false', 'off', 'no'}))
                        value = false;
                    elseif all(isnumeric(avalue)) || all(islogical(avalue)) && avalue == 1
                        value = true;
                    elseif isnumeric(avalue) && avalue == 0
                        value = false;
                    elseif ~isempty(avalue) && ~islogical(avalue)
                        error('ewsdata:validate:logical', '''%s'' should be of type logical', afield);
                    end
                case 'cell_of_logical'
                    if ~iscell(avalue) && isnumeric(avalue) || islogical(avalue)
                        avalue = num2cell(avalue);
                        value = avalue;
                    end
                    for i = 1:length(avalue)
                        value{i} = obj.validate('logical', afield, avalue{i});
                    end
                case 'column_no'
                    if ~isempty(avalue)
                        for i = 1:length(avalue)
                            if ~all(rem(avalue{i}, 1) == 0)
                                error('ewsdata:validate:colno', '''%s'' should be a whole number', afield);
                            end
                            if ~isempty(obj.rawdata) && any(avalue{i} < 0 | avalue{i} > size(obj.rawdata, 2))
                                error('ewsdata:validate:colno', '''%s'' should be >0 and at most the number of columns of the data (%d)', afield, size(obj.rawdata, 2) + 1);
                            end
                        end
                    end
                case 'enumerated_or_empty'
                    validopts = varargin{1};
                    if ~isempty(obj.options.(afield))
                        if ~ischar(obj.options.(afield)) || ~any(strcmp(obj.options.(afield), validopts))
                            s = sprintf('''%s'' or ', validopts{:});
                            error('ewsdata_windowed:set', '''%s'' should be: [] or %s', afield, s(1:end - 4));
                        end
                    end
                otherwise
                    error('ewsdata:validation', 'unknown type "%s" for validatation', akind)
            end
        end
    


        function res = set(obj, varargin)
            if nargin == 1
                disp(obj.options);
                return;
            end
            if nargin == 2 && isstruct(varargin{1})
                opts = varargin{1};
                v = [fieldnames(opts).'; struct2cell(opts).'];
                res = obj.set(v{:});
                return;
            end
            oldnames={'arlag','DFAorder','DFAwin'};
            newnames={'ar_lag','dfa_order','dfa_win'};
            res.any_error = false;
            res.unknown = {};
            parameter_names = fieldnames(obj.options);
            res.changed_pars = varargin(1:2:end);
            for i = 1:2:length(varargin)
                ndx=strcmpi(varargin{i},oldnames);
                if any(ndx)
                    varargin{i}=newnames{ndx};
                end
                ndx = strcmpi(varargin{i}, parameter_names);
                if any(ndx)
                    par = varargin{i + 1};
                    %force cell for some parameters that can act on a
                    %column
                    if ~iscell(par) && any(strcmpi(parameter_names{ndx}, {'detrending',  ...
                            'bandwidth', 'bandwidth_unit', 'polydegree', 'logtransform', 'datacolumn', 'corrcolumns'}))
                        %options that should be a cell
                        if ischar(par)
                            par = {par};
                        else
                            par = num2cell(par);
                        end
                    end
                    obj.options.(parameter_names{ndx}) = par;
                    if strcmp(varargin{i}, 'time')

                        obj.times = varargin{i + 1};
                        % elseif strcmp(varargin{i}, 'datacolumn') && isequal(par, {0})
                        %default datacolumn is different for multivariate
                        %indicators
  
                    elseif strcmp(varargin{i}, 'groups')
                        obj.groups = varargin{i + 1};
                        if iscell(obj.groups)
                            obj.groups = categorical(obj.groups);
                        end
                    end
                elseif strcmp(varargin{i}, 'grouping')
                    %for backward compatibility
                    obj.groups = varargin{i + 1};
                    obj.options.groups = obj.groups;
                elseif strcmp(varargin{i}, 'time')
                    obj.times = varargin{i + 1};
                elseif strcmp(varargin{i}, 'rawdata')
                    obj.rawdata = varargin{i + 1};
                else
                    res.any_error = true;
                    res.unknown(end + 1) = varargin(i);
                end
            end
            obj.updated = false;
            obj.options.nanflag = obj.validate('enumerated_or_empty', 'nanflag', obj.options.nanflag, {'includenan', 'omitnan'});
            obj.options.removetails = obj.validate('logical', 'removetails', obj.options.removetails);
            obj.options.logtransform = obj.validate('cell_of_logical', 'logtransform', obj.options.logtransform);
            obj.options.datacolumn = obj.validate('column_no', 'datacolumn', obj.options.datacolumn);
            obj.options.corrcolumns = obj.validate('column_no', 'corrcolumns', obj.options.corrcolumns);
        end

        function res = get(obj, aname, idx)
            if nargin == 1
                res = obj.options;
                return;
            end
            res = [];
            if nargin < 3
                idx = [];
            end
            parameter_names = fieldnames(obj.options);
            ndx = strcmpi(parameter_names, aname);
            if any(ndx)
                res = obj.options.(parameter_names{ndx});
            elseif strcmp(aname, 'time')
                res = obj.times;
            elseif strcmp(aname, 'rawdata')
                res = obj.rawdata;
            end
            if iscell(res)
                if length(res) == 1
                    res = res{1};
                elseif ~isempty(idx)
                    res = res{idx};
                end
            end
        end

        function update_trend(obj)
            obj.updated = true;
            inds = obj.get('indicators');
            if ~isempty(inds)
                obj.indicat = indicator_fun.cell2ind(obj.get('indicators'), obj.options);
            end
            tim = obj.get('time');
            if ~isempty(tim)
                obj.times = tim;
            else
                tim = obj.times;
            end
            obj.rawdetrended = ewsdata.do_detrend(tim, obj.rawdata, obj.get('detrending'),  ...
                obj.get('bandwidth'), obj.get('bandwidth_unit'), obj.get('polydegree'), obj.get('nanflag'),...
                obj.get('removetails'));
        end
    end

end




function res = movmean1(timeseries, bw, nanflag)
    %simple remake of movmean (for older MAT
    if length(bw) == 1
        if mod(bw, 2) == 0
            bw = [bw / 2 bw / 2 - 1];
        else
            bw = [(bw - 1) / 2 (bw - 1) / 2];
        end
    end
    starti = (1:length(timeseries)) - bw(1);
    endi = (1:length(timeseries)) + bw(2);
    starti(starti < 1) = 1;
    endi(endi > length(timeseries)) = length(timeseries);
    res = zeros(size(timeseries));
    if ~matlabLessThan([8 5])
        for i1 = 1:length(timeseries)
            res(i1) = mean(timeseries(starti(i1):endi(i1)), nanflag);
        end
    else
        if strcmp(nanflag, 'omitnan')
            for i1 = 1:length(timeseries)
                res(i1) = nanmean(timeseries(starti(i1):endi(i1)));
            end
        else
            for i1 = 1:length(timeseries)
                res(i1) = mean(timeseries(starti(i1):endi(i1)));
            end
        end
    end
end
