%function generic_ews
% generic_ews is used to estimate statistical moments within rolling
% windows along a timeserie (based on the R early warning signals toolbox,
% but simplified)
%
% Usage
%
% generic_ews(timeseries, option pairs)
% generic_ews(time,timeseries, option pairs)
%    for example:
%    generic_ews(timeseries, 'winsize', 50, 'detrending', 'no', ...
%       'bandwidth', [], 'logtransform', false, 'interpolate', false)
%
% Arguments
%
% timeseries   - a numeric vector of the observed univariate timeseries values or a numeric
%                matrix of observed timeseries values. If you use a tabel or dataset
%                you can add time as a column named t or time
%
% time         - a numeric vector of the time index (numeric)
%
% datacolumn   - column number(s) with the data []=all (default is 1). In case of
%                multivariate indicators you need to adapt this!
%
% winsize	   - is the size of the rolling window expressed as percentage of the timeseries length
%                (must be numeric between 0 and 100). Default is 50%.
%
% bandwidth	   - is the bandwidth used for the Gaussian kernel when gaussian filtering is applied.
%                It is expressed as percentage of the timeseries length (must be numeric between 0 and 100)
%                Alternatively it can be given by the optimal bandwidth suggested by Bowman and Azzalini (1997) Default).
%
% detrending   - the timeseries can be detrended/filtered prior to analysis (can be a cell for each data column).
%                There are six options: 'no'= no detrending, 'gaussian' =
%                gaussian filtering, 'movmean' = moving average (statistics
%                toolbox), 'poly' = polynomical (default degree=4, see
%                polydegree), 'linear' = linear detrending, or 'first-diff' = first-differencing. Default is 'gaussian' detrending.
%
% removetails  - remove half bandwidth at both tails for detrending. Default = true
%
% indicators   - cell of strings with the names of the indicators.
%                <a href="matlab:indicator_fun.supported">full list</a>
%                Examples:
%                'AR' - autocorrelation of residuals
%                'acf' - autocorrelation function of residuals
%                'std' - standard deviation of residuals
%                'DFA' - detrended fluctuation analysis
%                'skewness' - skewness of residuals
%                'cv' - coefficient of variation of original data
%                'abscorr' - the average of the absolute correlation with all other columns
%                default: {'AR', 'acf', 'std', 'skewness'}
%
%                You can also specify custom functions as a function handle
%                or a struct with the type of function (see register_EWS_functions in the code). 
%                The function handle should take 2 arguments: x and trend and return the value.
%                example: 
%                >> generic_ews(data, 'indicators',{'AR','std',@(x,trend)autoregression(x-trend,2)}
%
% logtransform - if TRUE data are logtransformed prior to analysis as log(X+1). Default is FALSE.
%
% interpolate  - If TRUE linear interpolation is applied to produce a timeseries of equal length as the original. Default is FALSE (assumes there are no gaps in the timeseries).
%
% polydegree   - degree of polynomial for detrending (default = 4)      
%
%
% ebisuzaki    - number of runs for a null model (using the power-spectrum method of Ebisuzaki), default = 0
%
% corrcolumns  - column number with the variable for cross correlation 
%                ([] is all other columns than the data column), default = []
% bandwidth_unit - unit of bandwidth (see units of winsize)
%
% winsize_unit - units of the  winsize - if different from '%' the absolute
%                units of the time series are assumed. If % the real size
%                is calculated from the length of the time series
% cv           - (depreciated) if true use cv instead of standard deviation default = false (use now indicators field)
% sens_pars    - sensitivity analysis of these parameters {'winsize','bandwidth'}
% sens_values  - values of the parameters for sensitivity analysis, all combinations: ews_combine(10:10:100,1:4:20)
%
% Specific parameters for different indicators:
% acf_lag       - The lag for acf indicator. Default = 1
% ar_lag        - The lag for autoregression. Default = 1
% compress_nclass - number of classes for digitizing data for compress indicator, default = 20
% dfa_order     - the order of the detrended fluctuation analysis. Default = 1
% dfa_win       - the maximum window size for detrended fluctuation analysis. Default = 100
% kbdm_overlap  - overlap in KBDM indicator
% klempelziv_nclass - number of classes for digitizing data for KLempelZiv indicator, default = 20
% mse_dim      - embedding dimension for multi-scale entropy. Default=3
% mse_mtau     - number of scales (tau) for multi-scale entropy. Default=5
% mse_r        - The radius for multi-scale sample entropy. Default = 0.1
% sampen_dim   - embedding dimension for sample entropy. Default=3
% sampen_r     - The radius for sample entropy. Default = 0.1
%
% Plotting options:
% figures      - cell of strings with figures of the combined figure {'original data', 'residuals'; 
%               'indicator 1'...'indicator 4'}  'original data 2' gives the
%               original data of column 2, 'residuals 2' the residuals of
%               col 2, no number = first datacolumn
%
% silent       - If silent=true then the figure is not drawn, default = false
%
% violinplot   - make violin plot of the taus of the null model, default = true
%
% title        - use text as title of the figure, default = ''
%
%
%not yet supported in the MATLAB version:
% AR_n	       - If TRUE the best fitted AR(n) model is fitted to the data. Default is FALSE.
%
% powerspectrum	-If TRUE the power spectrum within each rolling window is plotted. Default is FALSE.
%
% Options:
% s=generic_ews('-defaultopts') - create a struct s with the default
%               options (used internally)
% generic_ews('-f',res) determine the Fisher combined probability plots of 
%               the results res. res should be a struct array or cell array of earlier results
%
% generic_ews returns a structure with the following fields:
% timeseries   - original time series
% EWSopt       - structure with all used options (including the time index)
% trend        - trend in the time series (depending on the detrending option)
% colnames     - names of columns of the results (indicator names made unique, e.g. if 
%                two AR indicators are saved they are named AR and AR_1)
% indicators   - the resulting 
% taus         - Kendall tau values for each indicator
% pvalues      - p values of the tau values (only if an ebizuzsaki analysis was
%                done)
% nullmodel.taus  - the tau values for each of the ebizuzsaki iterations
%                done)
% description  - string with main options
%
%
% In addition, generic_ews returns a plots. The  plot contains the original data,
% the detrending/filtering applied and the residuals (if selected), and all the moment statistics.
% For each statistic trends are estimated by the nonparametric Kendall tau correlation (if statistics toolbox
% is present).
% Not supported: The second plot, if asked, quantifies resilience indicators fitting AR(n) selected by the
% Akaike Information Criterion. The third plot, if asked, is the power spectrum estimated
% by spec.ar for all frequencies within each rolling window.
%
% Author(s)
%
% Vasilis Dakos vasilis.dakos@gmail.com
% MATLAB version by Egbert van Nes
%
%
% References
%
% Ives, A. R. (1995). "Measuring resilience in stochastic systems." Ecological Monographs 65: 217-233
%
% Dakos, V., et al (2008). "Slowing down as an early warning signal for abrupt climate change." Proceedings of the National Academy of Sciences 105(38): 14308-14312
%
% Dakos, V., et al (2012)."Methods for Detecting Early Warnings of Critical Transitions in Time Series Illustrated Using Simulated Ecological Data." PLoS ONE 7(7): e41010. doi:10.1371/journal.pone.0041010
%
%
function result = generic_ews(timeseries, varargin)
    if nargin == 0
        error('grind:generic_ews:notenoughargs', 'Not enough input arguments, at least the timeseries is needed (see <a href="matlab:help(''generic_ews'')">help generic_ews</a>)');
    end
    if ischar(timeseries)
        if strcmp(timeseries, '-defaultopts')
            %the default options
            result = ewsdata_windowed.defaults;
 
            return;

        elseif strncmpi(timeseries, '-f', 2)
            %table with fisher combined probabilities
            res = varargin{1};
            if ~iscell(res)
                %if not a cell make a cell for convenience
                res1 = varargin{1};
                res = cell(size(res));
                for i = 1:numel(res1)
                    res{i} = res1(i);
                end
            end
            %ebitaus = zeros(res{1}.EWSopt.ebisuzaki, length(res));
            titles = cell(length(res), 1);
            colnames = res{1}.colnames;
            pvalues = zeros(length(res), length(res{1}.colnames));
            fisher(1, length(res{1}.colnames)) = struct('p', [], 'chi', [], 'df', []);
            %  fisher = cell(1, length(res{1}.colnames));
            taus = zeros(length(res), length(res{1}.colnames));
            silent = res{1}.EWSopt.silent | nargout > 0;
            for j = 1:length(res{1}.colnames)
                if ~silent
                    fprintf('\n=======  %s  ========\n', res{1}.colnames{j});
                    fprintf('Title\tKendall-tau %s\tp-value %s\n', res{1}.colnames{j}, res{1}.colnames{j});
                end
                for i = 1:numel(res)
                    titles{i} = res{i}.EWSopt.title;
                    if isempty(titles{i})
                        titles{i} = sprintf('data set %d', i);
                    end
                    taus(i, j) = res{i}.taus(j);
                    pvalues(i, j) = res{i}.pvalues(j); %(sum(taus(i) < ebitaus(:, i)) + 1) / size(ebitaus, 1);
                    if ~silent
                        fprintf('%s\t%.3g\t%.3g\n', titles{i}, taus(i, j), pvalues(i, j));
                    end
                end
                fisher1 = fishercombine(pvalues(:, j));
                if ~silent
                    regimes = 'Fisher combined'; %sprintf('%s, ', titles{:});
                    fprintf('%s\tX2(%d)=%.3g\t%.3g\n', regimes, fisher1.df, fisher1.chi, fisher1.p);
                end
                fisher(1, j) = fisher1;
            end
            if nargout > 0
                result.titles = titles;
                result.colnames = colnames;
                result.taus = taus;
                result.pvalues = pvalues;
                %   result.fisherp = fisherp;
                result.fisher = fisher;
            end
            return;
        end
    end
    if ~((isfield(timeseries, 'timeseries') || isfield(timeseries, 'inds')) && isfield(timeseries, 'taus')) % if these fields exist it is a previously saved run
        %***************************** the main part ******
 
        ewstimeseries = ewsdata_windowed(timeseries, varargin{:});
        res = ewstimeseries.running_window;
 
    else %plot previous data
        res = timeseries;
        if nargin > 1
            ewstimeseries = ewsdata_windowed(res.timeseries, res.EWSopt, varargin{:});
            plottingopts = {'figures', 'title', 'violinplot'};
            opts = reshape(varargin, 2, numel(varargin) / 2);
            if any(~ismember(lower(opts(1, :)), plottingopts))
                disp('new run as some settings are changed');
                res = ewstimeseries.running_window;
                %res = ewstimeseries.add_ebisuzaki(res);
            else
                res.EWSopt = ewstimeseries.options;
            end
        end
 
        res.EWSopt.silent = false; %otherwise this function may have no use
        if isfield(res,'ebitaus') %old version
            taus=res.ebitaus;
            if iscell(taus)
                taus={taus};
            end
            res.nullmodel=struct('taus',taus);
        end
        if ~isfield(res, 'inds')
            %             trend = res.trend;
            %             if isempty(trend)
            %                 trend = zeros(size(res.timeseries));
            %             end
            %             timeseries = res.timeseries;
            %             indicators = num2cell(res.indicators, 1);
        elseif isa(res.inds, 'table') %old version
            if ~any(strcmp(res.inds.Properties.VariableNames, 'trend'))
                res.trend = zeros(size(res.inds.Variable));
            else
                res.trend = res.inds.trend;
            end
            res.timeseries = res.inds.Variable;
            inds = table2array(res.inds(:, ewsdata.makeunique({EWSfunctions.name})));
            res.indicators = num2cell(inds, 1);
        end
    end
    ewsdata_windowed.plot_results(res);

    if nargout > 0
        
%         if isempty(ewsdata_windowed.getcolumn(res.EWSopt.bandwidth, 1))
%             % optimal bandwidth suggested by Bowman and Azzalini (1997) p.31
%             n = size(timeseries, 1);
%             hx = median(abs(res.EWSopt.time - median(res.EWSopt.time))) / 0.6745 * (4 / 3 / n)^0.2;
%             hy = median(abs(timeseries(:, getcolumn(res.EWSopt.datacolumn, 1)) - median(timeseries(:, ewsdata_windowed.getcolumn(res.EWSopt.datacolumn, 1))))) / 0.6745 * (4 / 3 / n)^0.2;
%             res.EWSopt.bandwidth = sqrt(hy * hx);
%         end
        if nargout == 1
            result = res;
        end
    end

    %end
end





% function adjustticklabels(hax, orient)
%     %     axis(hax, 'tight')
%     %     newtick = get(hax, [orient 'tick']);
%     %     tickdiff = (newtick(2) - newtick(1));
%     %     newlim = [newtick(1) - tickdiff newtick(end) + tickdiff];
%     %     axis(hax, 'manual')
%     %     set(hax, [orient 'lim'], newlim);
%     %     set(hax, [orient 'tick'], [newtick(1) - tickdiff newtick]);
% end





