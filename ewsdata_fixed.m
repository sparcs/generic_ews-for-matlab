classdef ewsdata_fixed < ewsdata
    %this is a handle object as there may be overhead in changing the
    %trends
    %     
    %  properties inherited from ewsdata:
    %     properties (Access = protected)
    %         rawdetrended = []; %do not use this property directly (update can be delayed)
    %         updated = false;
    %         data_updated = false;
    %         indicat = {};
    %     end
    %     properties
    %         rawdata = [];
    %         groups = []; %grouping variable, categorical or other (see stats/grpstats)
    %         times = [];
    %         options = {};
    %     end
 
    methods (Static = true)
        function result = defaults
            [~,pars]=indicator_fun.allproperties;
            result = struct( ... 
                'alpha', 0.05,  ...
                'nboot', 1000,  ... 
                'groups', [],  ...
                'slopekind', 'ts',  ...
                'detrending', 'no',  ...
                'removetails', true,...
                'bandwidth', 10,  ...
                'time', [],  ...
                'datacolumn', {{0}},  ... % default dependent on number of variables, per indicator or one for all
                'corrcolumns', {{[]}},  .... %empty is all (except datacolumn)
                'silent', false,  ...
                'logtransform', false,  ...
                'indicators', {{'AR', 'acf', 'std', 'skewness'}},  ... %other options cv, abscorr, crosscorr, mean, DFA
                'polydegree', 4,  ...
                'interpolate', false,  ...
                'AR_n', false,  ...
                'powerspectrum', false,  ...
                'bandwidth_unit', '%',  ...
                'nanflag', 'includenan',  ...
                'title', '', pars{:});
            %                           'figures', {{'original data', 'residuals'; 'indicator 1', 'indicator 2'; 'indicator 3', 'indicator 4'; 'indicator 5', 'indicator 6'}},  ...
 
        end
 

        function res = getcolumn(col, i, multcell)
            if nargin < 3
                multcell = false;
            end
            if multcell && ~iscell(col)
                col = {col};
            end
            if isempty(col)
                res = col;
            elseif iscell(col)
                if numel(col) == 1
                    res = col{1};
                else
                    res = col{i};
                end
            else
                if numel(col) == 1
                    res = col(1);
                else
                    res = col(i);
                end
            end
        end
    end
    
    methods
        function obj = ewsdata_fixed(arawdata, varargin)
            if nargin > 1 && isstruct(varargin{1})
                %the second argument can be the defaults (or all
                %settings)
                defaultopts = varargin{1};
                varargin = varargin(2:end);
            else
                defaultopts = ewsdata_fixed.defaults;
            end
           % defaultopts.sampen_r=0.2*std(arawdata);
            obj = obj@ewsdata(arawdata, defaultopts, varargin{:});

            %grouping using the time
            if isempty(obj.groups) && ~isempty(obj.times)
                tim = obj.times;
                dtim = diff(tim);
                minstep = min(dtim);
                groupborders = [0; find(dtim > minstep * 2); numel(tim)];
                if numel(groupborders) > 2
                    grp = zeros(size(tim));
                    for i = 2:length(groupborders)
                        ndx1 = groupborders(i - 1) + 1:groupborders(i);
                        %check that the groups are correct
                        assert(all(abs(diff(tim(ndx1)) - minstep) < minstep * 2), 'Internal error in grouping of times')
                        grp(ndx1) = mean(tim(ndx1));
                    end
                    obj.groups = grp;
                    fprintf('Found %d groups of burst times\n', numel(groupborders) - 1);
                else
                    fprintf('Found one group in the times\n');
                end
            end
 
        end
        
        
        function res = set(obj, varargin)
            res = set@ewsdata(obj, varargin{:});
            %validation of some options
            obj.options.nanflag = obj.validate('enumerated_or_empty', 'nanflag', obj.options.nanflag, {'includenan', 'omitnan'});
            obj.options.slopekind = obj.validate('enumerated_or_empty', 'slopekind', obj.options.slopekind, { 'all', 'linreg', 'kendall', 'ts'});
            %Logicals:
            logicals = {'silent', 'interpolate'};
            for i = 1:length(logicals)
                obj.options.(logicals{i}) = obj.validate('logical', logicals{i}, obj.options.(logicals{i}));
            end
        end

        
        function result = run_bursts(obj) %, indicators, alpha, nboot, indicat1)
            function res = getslopes(gs, gdata)
                res = zeros(size(gdata, 2), 3) + NaN;
                for m = 1:size(gdata, 2)
                    ft1 = polyfit(gs, gdata(:, m), 1);
                    res(m, 1) = ft1(1); %slope
                    res(m, 2) = corr(gs, gdata(:, m), 'type', 'Kendall');
                    res(m, 3) = TS_slope(gs, gdata(:, m));
                end
            end
            if ~obj.data_updated
                obj.update_data;
            end
            if ~obj.updated
                obj.update_trend;
            end
            opts = obj.options;
            %if nargin > 2 && ~isempty(alpha)
            %    opts.alpha = alpha;
            %end
            %if nargin > 3 && ~isempty(nboot)
            %    opts.nboot = nboot;
            %end
            %if nargin > 2 && ~isempty(indicators)
            %    if ischar(indicators)
            %        opts.indicators = {indicators};
            %    else
            %        opts.indicators = indicators;
            %    end
            %end
            %if nargin < 5 %shortcut
            %    obj.indicat = indicator_fun.cell2ind(opts.indicators, opts);
            %else
            %    obj.indicat = indicat1;
            %end
            if isempty(opts.slopekind)
                opts.slopekind = 'ts';
            end
            ugroups = unique(obj.groups);
            %test if the grouping variable is seqential
            assert(numel(ugroups) == sum(diff(obj.groups) ~= 0) + 1, 'Error the groups are not sequential');
            
            %run generic_ews_fixed for all groups
            for i = 1:length(ugroups)
                % fprintf('%s: %g\n', 'Group number', ugroups(i))
                res2 = obj.run_fixed(ugroups(i)); %, opts.indicators, opts.alpha, opts.nboot, obj.indicat);
                res2.groupvalue = ugroups(i);
                if i == 1
                    groups = res2;
                else
                    groups(i) = res2;
                end
            end
            if length(ugroups) == 1
                %if there is only one group we cannot calculate slopes etc.
                result = res2;
                return;
            end
 
            quantiles = [opts.alpha / 2, 1 - opts.alpha / 2];

            groups = rmfield(groups, {'EWSopt', 'timeseries', 'trend', 'colnames', 'description', 'indicator_funs'});
            res = rmfield(res2, {'groupvalue', 'indicators'});
            res.bootstrap = struct('bootstrapsets', struct('values', []));
            res.groups = groups;
            slope_names = res.colnames;
            for k = 1:length(slope_names)
                slope_names{k} = ['slope_' slope_names{k}];
            end
 
            %extract the slopes  directly estimated from the data 
            if iscell(res.CL.Estimated)
                %multivariate and different column sizes
                gdata = cell(length(ugroups), numel(res.colnames));
                for i = 1:length(ugroups)
                    gdata(i, :) = groups(i).CL.Estimated;
                end
                estimvals = cell(1, numel(res.colnames));
                for i = 1:numel(res.colnames)
                    estimvals{i} = getslopes(ugroups, horzcat(gdata{:, i})');
                end
                slopedata = cell(size(groups(1).bootstrap.bootstrapsets.values));
                %     pvalues(j) = (sum(taus(j) < ebitaus(j, :)) + 1) / nebisuzaki;

                CL = slopedata;
                for j = 1:numel(res.indicator_funs)
                    slopedat.linearreg.values = zeros(size(groups(1).bootstrap.bootstrapsets.values{j}));
                    slopedat.kendall.values = zeros(size(groups(1).bootstrap.bootstrapsets.values{j}));
                    slopedat.tsslope.values = zeros(size(groups(1).bootstrap.bootstrapsets.values{j}));
     
                    for i = 1:opts.nboot
                        sdata = zeros(numel(ugroups), size(slopedat.linearreg.values, 2));
                        for j1 = 1:length(groups)
                            sdata(j1, :) = groups(j1).bootstrap.bootstrapsets.values{j}(i, :);
                        end
                        estimbootvals = getslopes(ugroups, sdata);
                        slopedat.linearreg.values(i, :) = estimbootvals(:, 1);
                        slopedat.kendall.values(i, :) = estimbootvals(:, 2);
                        slopedat.tsslope.values(i, :) = estimbootvals(:, 3);
                    end
                    slopedata{j} = slopedat;
                    qlinearreg = quantile(slopedat.linearreg.values, quantiles);
                    qkendall = quantile(slopedat.kendall.values, quantiles);
                    qtsslope = quantile(slopedat.tsslope.values, quantiles);
                    if size(slopedat.linearreg.values, 2) > numel(res.colnames)
                        qlinearreg = qlinearreg';
                        qkendall = qkendall';
                        qtsslope = qtsslope';
                    end
                    c = num2cell(1:size(slopedat.linearreg.values, 2))';
                    if numel(c) == 1
                        c = slope_names(j);
                    else
                        s = sprintf('%s_%%d|', slope_names{j});
                        c = regexp(sprintf(s, c{:}), '\|', 'split');
                        c = c(1:end - 1);
                    end
                    for k = 1:size(slopedat.linearreg.values, 2)
                        qlinearreg(k, 3) = (sum(slopedat.linearreg.values(:, k) <= 0) + 1) / opts. nboot;
                        qkendall(k, 3) = (sum(slopedat.kendall.values(:, k) <= 0) + 1) / opts. nboot;
                        qtsslope(k, 3) = (sum(slopedat.tsslope.values(:, k) <= 0) + 1) / opts. nboot;
                    end
     
                    %end
                    try
                        tab1 = table(estimvals{j}(:, 1), qlinearreg(:, 1), qlinearreg(:, 2), qlinearreg(:, 3), 'RowNames', c, 'VariableNames', {'Estimated', 'Lower', 'Higher', 'p_value'});
                        tab2 = table(estimvals{j}(:, 2), qkendall(:, 1), qkendall(:, 2), qkendall(:, 3), 'RowNames', c, 'VariableNames', {'Estimated', 'Lower', 'Higher', 'p_value'});
                        tab3 = table(estimvals{j}(:, 3), qtsslope(:, 1), qtsslope(:, 2), qtsslope(:, 3), 'RowNames', c, 'VariableNames', {'Estimated', 'Lower', 'Higher', 'p_value'});
                    catch err
                        if strcmp(err.identifier, 'MATLAB:UndefinedFunction')
                            tab1 = dataset(estimvals{j}(:, 1), qlinearreg(:, 1), qlinearreg(:, 2), qlinearreg(:, 3), 'ObsNames', c, 'VarNames', {'Estimated', 'Lower', 'Higher', 'p_value'}); %#ok<DTSET>
                            tab2 = dataset(estimvals{j}(:, 2), qkendall(:, 1), qkendall(:, 2), qkendall(:, 3), 'ObsNames', c, 'VarNames', {'Estimated', 'Lower', 'Higher', 'p_value'}); %#ok<DTSET>
                            tab3 = dataset(estimvals{j}(:, 3), qtsslope(:, 1), qtsslope(:, 2), qtsslope(:, 3), 'ObsNames', c, 'VarNames', {'Estimated', 'Lower', 'Higher', 'p_value'}); %#ok<DTSET>
                        else
                            rethrow(err);
                        end
                    end
                    CL{j} = struct('linearreg', tab1, 'kendall', tab2, 'tsslope', tab3);
                end
                res.bootstrap.bootstrapsets = slopedata;
                CL1 = CL{1};
                for j = 2:length(CL)
                    CL1.linearreg = vertcat(CL1.linearreg, CL{j}.linearreg);
                    CL1.kendall = vertcat(CL1.kendall, CL{j}.kendall);
                    CL1.tsslope = vertcat(CL1.tsslope, CL{j}.tsslope);
                end
                res.CL = CL1;
                res.pvalue = 0;
            else
                %univariate same column sizes
                gdata = zeros(length(ugroups), numel(res.colnames));
                for i = 1:length(ugroups)
                    gdata(i, :) = groups(i).CL.Estimated;
                end
                estimvals = getslopes(ugroups, gdata);
         
                %extract the bootstrapped slopes
                slopedata.linearreg.values = zeros(size(groups(1).bootstrap.bootstrapsets.values));
                slopedata.kendall.values = zeros(size(groups(1).bootstrap.bootstrapsets.values));
                slopedata.tsslope.values = zeros(size(groups(1).bootstrap.bootstrapsets.values));
         

                for i = 1:opts.nboot
                    sdata = zeros(numel(ugroups), numel(res.indicator_funs));
                    %combining group data
                    for j1 = 1:length(groups)
                        sdata(j1, :) = groups(j1).bootstrap.bootstrapsets.values(i, :);
                    end
                    estimbootvals = getslopes(ugroups, sdata);
                    slopedata.linearreg.values(i, :) = estimbootvals(:, 1);
                    slopedata.kendall.values(i, :) = estimbootvals(:, 2);
                    slopedata.tsslope.values(i, :) = estimbootvals(:, 3);
                end
                %gather the results and determine quantiles of slopes
                %res = struct('groups', groups, 'bootstrapsets', slopedata);

                res.bootstrap.bootstrapsets = slopedata;
                CL.linearreg = quantile(res.bootstrap.bootstrapsets.linearreg.values, quantiles)';
                CL.kendall = quantile(res.bootstrap.bootstrapsets.kendall.values, quantiles)';
                CL.tsslope = quantile(res.bootstrap.bootstrapsets.tsslope.values, quantiles)';
                for k = 1:size(res.bootstrap.bootstrapsets.linearreg.values, 2)
                    CL.linearreg(k, 3) = (sum(res.bootstrap.bootstrapsets.linearreg.values(:, k) <= 0) + 1) / opts. nboot;
                    CL.kendall(k, 3) = (sum(res.bootstrap.bootstrapsets.kendall.values(:, k) <= 0) + 1) / opts. nboot;
                    CL.tsslope(k, 3) = (sum(res.bootstrap.bootstrapsets.tsslope.values(:, k) <= 0) + 1) / opts. nboot;
                end
                try
                    CL.linearreg = table(estimvals(:, 1), CL.linearreg(:, 1), CL.linearreg(:, 2), CL.linearreg(:, 3), 'VariableNames', {'Estimated', 'Lower', 'Higher', 'p_value'}, 'RowNames', slope_names);
                    CL.kendall = table(estimvals(:, 2), CL.kendall(:, 1), CL.kendall(:, 2), CL.kendall(:, 3), 'VariableNames', {'Estimated', 'Lower', 'Higher', 'p_value'}, 'RowNames', slope_names);
                    CL.tsslope = table(estimvals(:, 3), CL.tsslope(:, 1), CL.tsslope(:, 2), CL.tsslope(:, 3), 'VariableNames', {'Estimated', 'Lower', 'Higher', 'p_value'}, 'RowNames', slope_names);
                catch err
                    if strcmp(err.identifier, 'MATLAB:UndefinedFunction')
                        CL.linearreg = dataset(estimvals(:, 1), CL.linearreg(:, 1), CL.linearreg(:, 2), CL.linearreg(:, 3), 'VarNames', {'Estimated', 'Lower', 'Higher', 'p_value'}, 'ObsNames', slope_names); %#ok<DTSET>
                        CL.kendall = dataset(estimvals(:, 2), CL.kendall(:, 1), CL.kendall(:, 2), CL.kendall(:, 3), 'VarNames', {'Estimated', 'Lower', 'Higher', 'p_value'}, 'ObsNames', slope_names); %#ok<DTSET>
                        CL.tsslope = dataset(estimvals(:, 3), CL.tsslope(:, 1), CL.tsslope(:, 2), CL.tsslope(:, 3), 'VarNames', {'Estimated', 'Lower', 'Higher', 'p_value'}, 'ObsNames', slope_names); %#ok<DTSET>
                    else
                        rethrow(err);
                    end
                end
                res.CL = CL;

            end
            switch opts.slopekind
                case 'all'
                    fprintf('Slopes determined with Linear Regression\n');
                    disp(res.CL.linearreg);
                    fprintf('Slopes determined with Kendall tau\n');
                    disp(res.CL.kendall);
                    fprintf('Slopes determined with Theil–Sen estimator\n');
                    disp(res.CL.tsslope);
                case 'linreg'
                    fprintf('Slopes determined with Linear Regression\n');
                    disp(res.CL.linearreg);
                case 'kendall'
                    fprintf('Slopes determined with Kendall tau\n');
                    disp(res.CL.kendall);
                case 'ts'
                    fprintf('Slopes determined with Theil–Sen estimator\n');
                    disp(res.CL.tsslope);
            end


            if nargout > 0
                result = res;
            end
        end


        function result = run_fixed(obj, cat) %, indicators, alpha, nboot, indicat1)
            %run fixed
            if ~obj.data_updated
                obj.update_data;
            end
            if ~obj.updated
                obj.update_trend;
            end
            opts = obj.options;
            if nargin < 2
                cat = [];
            end
            %if nargin > 3 && ~isempty(alpha)
            %     opts.alpha = alpha;
            % end
            % if nargin > 4 && ~isempty(nboot)
            %     opts.nboot = nboot;
            % end
            % if nargin > 2 && ~isempty(indicators)
            %     if ischar(indicators)
            %         opts.indicators = {indicators};
            %     else
            %         opts.indicators = indicators;
            %     end
            % end
            % if nargin < 6 %shortcut
            %     obj.indicat = indicator_fun.cell2ind(opts.indicators, opts);
            % else
            %     obj.indicat = indicat1;
            % end
            if ~isempty(obj.groups) && ~isempty(cat)
                ndx = obj.groups == cat;
            else
                ndx = true(size(obj.rawdata, 1), 1);
            end
            detrended_data = obj.detrended;
            ndx = ndx & ~isnan(detrended_data(:, 1));
            indices = unique_indicat(obj.indicat);
            values = indicator_fun.calcgroup(obj.indicat, indices, obj.rawdata, detrended_data, ndx);
            estimvals = cell(1, numel(obj.indicat));

            for j = 1:length(obj.indicat)
                % vals = obj.indicat{j}.calc(obj.rawdata, detrended_data, ndx);
                estimvals{j} = values{j}(:);
            end
            [bootsets, boot] = sievebootstrap(obj.rawdata(ndx, :), obj.options.nboot, obj.options.nanflag);
            %if size(bootsets, 3) > 1
            %    bootsets = permute(bootsets, [1, 3, 2]);
            %end
            ncols = size(obj.rawdata, 2);
            tim = obj.times(ndx);
            values1 = cell(obj.options.nboot, numel(obj.indicat));
            %for i = 1:size(bootsets, 2)
            for j = 1:obj.options.nboot
                if ncols == 1
                    theset = bootsets(:, j);
                else
                    theset = bootsets(:, :, j);
                end
                detrended_bootset = ewsdata.do_detrend(tim, theset, obj.options.detrending,  ...
                    obj.options.bandwidth, obj.options.bandwidth_unit, obj.options.polydegree, obj.options.nanflag,...
                obj.options.removetails);
                vals1 = indicator_fun.calcgroup(obj.indicat, indices, theset, detrended_bootset);

                for k = 1:length(obj.indicat)
 
                    % vals1= obj.indicat{k}.calc(theset, detrended_bootset);
                    values1{j, k} = vals1{k}(:)';
                end
            end
            %end
            vals = cell(size(estimvals));
            maxwidth = 1;
            for k = 1:length(obj.indicat)
                vals{k} = vertcat(values1{:, k});
                if size(vals{k}, 2) > maxwidth
                    maxwidth = size(vals{k}, 2);
                end
            end
            if maxwidth == 1
                boot.bootstrapsets.values = [vals{:}];
                estimvals = [estimvals{:}];
            else
                boot.bootstrapsets.values = vals;
            end
            colnames = ewsdata.makeunique(obj.indicat)';
            % for i = 1:length(colnames)
            %     colnames{i} = obj.indicat{i}.dispname;
            %  end

            quantiles = [opts.alpha / 2 1 - opts.alpha / 2];
            if iscell(boot.bootstrapsets.values)
                Lower = cell(size(estimvals));
                Higher = Lower;
                for i = 1:length(boot.bootstrapsets.values)
                    Lower{i} = quantile(boot.bootstrapsets.values{i}, quantiles(1));
                    Higher{i} = quantile(boot.bootstrapsets.values{i}, quantiles(2));
                end
                CL = struct('ColNames', {colnames'}, 'Estimated', {estimvals}, 'Lower', {Lower}, 'Higher', {Higher});
            else
                CL = quantile(boot.bootstrapsets.values, quantiles,1)';
                pval = CL(:, 1);
                for k = 1:length(pval)
                    pval(k) = (sum(boot.bootstrapsets.values(:, k) < 0) + 1) / opts.nboot;
                end

                try
                    CL = table(estimvals', CL(:, 1), CL(:, 2), pval, 'VariableNames', {'Estimated', 'Lower', 'Higher', 'p_value'}, 'RowNames', colnames');
                catch err
                    if strcmp(err.identifier, 'MATLAB:UndefinedFunction')
                        CL = dataset(estimvals', CL(:, 1), CL(:, 2), pval, 'VarNames', {'Estimated', 'Lower', 'Higher', 'p_value'}, 'ObsNames', colnames'); %#ok<DTSET>
                    else
                        rethrow(err)
                    end
     
                end

            end
      
            descr = sprintf('data generated on %s with generic_ews_fixed\nalpha = %g, detrending = %s, bandwidth = %g, logtransform = %d, arlag = %d, interpolate = %d',  ... 
                datestr(now()), obj.options.alpha, obj.get('detrending', 1), obj.get('bandwidth', 1), obj.get('logtransform', 1), obj.get('arlag', 1), obj.get('interpolate', 1));
            opts.time = obj.times;
            colnames = obj.makeunique(colnames);
            if iscell(estimvals)
                %needs to be double cell to get a cell in a struct
                estimvals = {estimvals};
                %   taus = {taus};
            end
            result = struct('timeseries', obj.rawdata,  ...
                'EWSopt', opts,  ...
                'trend', obj.rawdata - obj.rawdetrended,  ...
                'bootstrap', boot,  ...
                'colnames', {colnames},  ...
                'indicators', estimvals,  ...
                'CL', CL,  ...
                'indicator_funs', {obj.indicat},  ...
                'description', descr);
        end
    end




end


